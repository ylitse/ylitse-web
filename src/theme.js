import { createMuiTheme } from '@material-ui/core/styles';

export const GREEN_PRIMARY = '#76b856';
export const GREEN_SECONDARY = '#5685ee';
export const ORANGE = '#f7931e';
export const ORANGE_DARK = '#ec7404';
export const BEIGE = '#fdeddd';
export const YELLOW = '#ffc952';
export const GREY_VERY_LIGHT = '#f4f5f5';
export const GREY_LIGHT = '#f0f1f1';
export const GREY_10 = '#e6e7e8';
export const GREY_30 = '#c0c0c0';
export const GREY_50 = '#888888';
export const GREY_DARK = '#505050';
export const WHITE = '#fff';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: ORANGE,
            contrastText: WHITE,
        },
        secondary: {
            main: GREEN_PRIMARY,
            dark: GREEN_SECONDARY,
            contrastText: WHITE,
        },
        tonalOffset: 0.1,
        contrastThreshold: 3,
        background: {
            default: GREY_VERY_LIGHT,
        },
        text: {
            primary: GREY_DARK,
            secondary: GREY_50,
            disabled: GREY_30,
            hint: GREY_30,
            divider: GREY_10,
        },
    },
    overrides: {
        MuiFormLabel: {
            root: {
                '&$focused': {
                    color: GREEN_SECONDARY,
                },
            },
        },
        MuiInput: {
            underline: {
                '&:after': {
                    backgroundColor: GREEN_SECONDARY,
                },
            },
            root: {
                '$:disabled': {
                    // Make disabled (placeholder) text readable
                    color: 'inherit',
                },
            },
        },
        MuiCircularProgress: {
            root: {
                // Remove dotted border on Firefox
                '&:focused': {
                    outline: 'none',
                },
            },
        },
        MuiTab: {
            fullWidth: {
                // https://github.com/mui-org/material-ui/issues/10284
                maxWidth: 'initial',
            },
        },
        MuiTableCell: {
            root: {
                paddingRight: 24,
            },
        },
    },
});

export default theme;
