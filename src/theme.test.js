import theme from './theme';

describe('theme', () => {
    test('has palette', () => {
        expect(theme).toMatchObject(expect.objectContaining({
            palette: expect.any(Object),
        }));
    });
});
