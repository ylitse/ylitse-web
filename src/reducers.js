import { combineReducers } from 'redux';

import { patchReportList, sortBy, updateWith } from './utils';
import {
    FeedbackActionTypes,
    DialogActionTypes,
    VersionActionTypes,
    UserActionTypes,
    LogoutActionTypes,
    SkillsActionTypes,
    AccountsActionTypes,
    StatsActionTypes,
    QuestionsActionTypes,
    ReportsActionTypes,
    LogsActionTypes,
} from './actions';

export function feedback(state = { messages: [], current: '' }, action) {
    switch (action.type) {
        case FeedbackActionTypes.ENQUEUE:
            // Show the first message right away
            if (!state.current) {
                return { messages: [], current: action.message };
            }

            // Don't show the current message again
            if (action.message === state.current) {
                return state;
            }

            return {
                messages: [...state.messages, action.message],
                current: state.current,
            };
        case FeedbackActionTypes.DEQUEUE:
            // No more messages to show
            if (state.messages.length === 0) {
                return { messages: [], current: '' };
            }

            return {
                messages: state.messages.slice(1),
                current: state.messages[0],
            };
        default:
            return state;
    }
}

const dialogInitialState = { type: '', props: {} };

export function dialog(state = dialogInitialState, action) {
    switch (action.type) {
        case DialogActionTypes.OPEN:
            return {
                type: action.dialogType,
                props: action.dialogProps,
            };
        case DialogActionTypes.CLOSE:
            return dialogInitialState;
        default:
            return state;
    }
}

export function version(state = { api: '', isFetching: false }, action) {
    switch (action.type) {
        case VersionActionTypes.REQUEST:
            return { ...state, isFetching: true };
        case VersionActionTypes.SUCCESS:
            return {
                ...state,
                api: action.version.api,
                isFetching: false,
            };
        case VersionActionTypes.FAILURE:
            return { ...state, isFetching: false };
        default:
            return state;
    }
}

const userInitialState = { role: '', username: '', isFetching: false };

export function user(state = userInitialState, action) {
    switch (action.type) {
        case UserActionTypes.REQUEST:
            return { ...state, isFetching: true };
        case UserActionTypes.SUCCESS:
            return {
                ...state,
                ...action.user,
                isFetching: false,
            };
        case UserActionTypes.FAILURE:
            return { ...state, isFetching: false };
        default:
            return state;
    }
}

export function logout(state = { inProgress: false }, action) {
    switch (action.type) {
        case LogoutActionTypes.REQUEST:
            return { inProgress: true };
        default:
            return state;
    }
}

const skillsInitialState = {
    items: [],
    isFetching: false,
    isCreating: false,
    isDeleting: false,
};

export function skills(state = skillsInitialState, action) {
    switch (action.type) {
        case SkillsActionTypes.FETCH_REQUEST:
            return { ...state, isFetching: true };
        case SkillsActionTypes.FETCH_SUCCESS:
            return {
                ...state,
                items: sortBy('name', action.skills),
                isFetching: false,
            };
        case SkillsActionTypes.FETCH_FAILURE:
            return { ...state, isFetching: false };
        case SkillsActionTypes.CREATE_REQUEST:
            return { ...state, isCreating: true };
        case SkillsActionTypes.CREATE_SUCCESS:
            return {
                ...state,
                items: sortBy('name', [action.skill, ...state.items]),
                isCreating: false,
            };
        case SkillsActionTypes.CREATE_FAILURE:
            return { ...state, isCreating: false };
        case SkillsActionTypes.DELETE_REQUEST:
            return { ...state, isDeleting: true };
        case SkillsActionTypes.DELETE_SUCCESS:
            return {
                ...state,
                items: state.items.filter(s => s.id !== action.id),
                isDeleting: false,
            };
        case SkillsActionTypes.DELETE_FAILURE:
            return { ...state, isDeleting: false };
        default:
            return state;
    }
}

const accountsInitialState = {
    items: [],
    isFetching: false,
    isCreating: false,
    isUpdating: false,
    isDeleting: false,
};

export function accounts(state = accountsInitialState, action) {
    switch (action.type) {
        case AccountsActionTypes.FETCH_REQUEST:
            return { ...state, isFetching: true };
        case AccountsActionTypes.FETCH_SUCCESS:
            return {
                ...state,
                items: sortBy('username', action.accounts),
                isFetching: false,
            };
        case AccountsActionTypes.FETCH_FAILURE:
            return { ...state, isFetching: false };
        case AccountsActionTypes.CREATE_REQUEST:
            return { ...state, isCreating: true };
        case AccountsActionTypes.CREATE_SUCCESS:
            return {
                ...state,
                items: sortBy('username', [action.account, ...state.items]),
                isCreating: false,
            };
        case AccountsActionTypes.CREATE_FAILURE:
            return { ...state, isCreating: false };
        case AccountsActionTypes.UPDATE_REQUEST:
            return { ...state, isUpdating: true };
        case AccountsActionTypes.UPDATE_SUCCESS:
            return {
                ...state,
                items: updateWith(action.account, state.items),
                isUpdating: false,
            };
        case AccountsActionTypes.UPDATE_FAILURE:
            return { ...state, isUpdating: false };
        case AccountsActionTypes.DELETE_REQUEST:
            return { ...state, isDeleting: true };
        case AccountsActionTypes.DELETE_SUCCESS:
            return {
                ...state,
                items: state.items.filter(s => s.id !== action.id),
                isDeleting: false,
            };
        case AccountsActionTypes.DELETE_FAILURE:
            return { ...state, isDeleting: false };
        default:
            return state;
    }
}

const statsInitialState = {
    stats: {},
    totals: {},
    isFetching: false,
};

export function stats(state = statsInitialState, action) {
    switch (action.type) {
        case StatsActionTypes.REQUEST:
            return { ...state, isFetching: true };
        case StatsActionTypes.SUCCESS:
            return {
                ...state,
                stats: action.stats.stats,
                totals: action.stats.totals,
                isFetching: false,
            };
        case StatsActionTypes.FAILURE:
            return { ...state, isFetching: false };
        default:
            return state;
    }
}

export function questions(
    state = {
        questions: [],
        isFetching: false,
        questionEdit: {},
        isCreating: false,
        isDeleting: false,
    },
    action,
) {
    switch (action.type) {
        case QuestionsActionTypes.REQUEST:
            return { ...state, isFetching: true };
        case QuestionsActionTypes.SUCCESS:
            return {
                ...state,
                questions: action.questions,
                isFetching: false,
                isCreating: false,
            };
        case QuestionsActionTypes.FAILURE:
            return { ...state, isFetching: false };
        case QuestionsActionTypes.SET_QUESTION:
            return { ...state, questionEdit: action.question };
        case QuestionsActionTypes.SET_QUESTION_RULES:
            return {
                ...state,
                questionEdit: {
                    ...state.questionEdit,
                    rules: {
                        ...state.questionEdit?.rules,
                        [action.key]: action.value,
                    },
                },
            };
        case QuestionsActionTypes.CREATE_REQUEST:
            return { ...state, isCreating: true };
        case QuestionsActionTypes.CREATE_FAILURE:
            return { ...state, isCreating: false };
        case QuestionsActionTypes.DELETE_REQUEST:
            return { ...state, isDeleting: true };
        case QuestionsActionTypes.DELETE_SUCCESS: {
            const updatedQuestions = state.questions.filter(
                question => question.id !== action.id,
            );
            return {
                ...state,
                isDeleting: false,
                questions: updatedQuestions,
            };
        }
        case QuestionsActionTypes.DELETE_FAILURE:
            return { ...state, isDeleting: false };
        default:
            return state;
    }
}

export const selectIsQuestionExisting = state => Boolean(state.questions.questionEdit?.id !== undefined);

export const selectSelectedQuestionRules = (state) => {
    const answerRules = state.questions.questionEdit?.rules?.answer ?? {};
    const titles = state.questions.questionEdit?.rules?.titles ?? {};
    const recipients = state.questions.questionEdit?.rules?.recipients ?? [];
    const isExisting = selectIsQuestionExisting(state);
    return { answerRules, titles, isExisting, recipients };
};

export const selectSelectedQuestionSchedule = (state) => {
    const schedule = state.questions.questionEdit?.rules?.schedule ?? {};
    const isExisting = selectIsQuestionExisting(state);
    return { schedule, isExisting };
};

export const selectQuestionId = state => state.questions.questionEdit?.id ?? null;

export function reports(
    state = {
        reports: [],
        isFetching: false,
        modalReport: {},
        isDeleting: false,
        isPatching: false,
    },
    action,
) {
    switch (action.type) {
        case ReportsActionTypes.REQUEST:
            return { ...state, isFetching: true };
        case ReportsActionTypes.SUCCESS:
            return {
                ...state,
                reports: action.reports,
                isFetching: false,
            };
        case ReportsActionTypes.FAILURE:
            return { ...state, isFetching: false };
        case ReportsActionTypes.SET_REPORT:
            return { ...state, modalReport: action.modalReport };
        case ReportsActionTypes.DELETE_REQUEST:
            return { ...state, isDeleting: true };
        case ReportsActionTypes.DELETE_SUCCESS: {
            const updatedReports = state.reports.filter(
                report => report.id !== action.reportId,
            );
            return {
                ...state,
                isDeleting: false,
                reports: updatedReports,
            };
        }
        case ReportsActionTypes.DELETE_FAILURE:
            return { ...state, isDeleting: false };
        case ReportsActionTypes.PATCH_REQUEST:
            return { ...state, isPatching: true };
        case ReportsActionTypes.PATCH_SUCCESS: {
            const updatedReports = patchReportList(state.reports, {
                reportId: action.reportId,
                ...action.updated,
            });
            const updatedModalReport = {
                ...state.modalReport,
                status: action.updated.status,
                comment: action.updated.comment,
            };

            return {
                ...state,
                isPatching: false,
                reports: updatedReports,
                modalReport: updatedModalReport,
            };
        }
        case ReportsActionTypes.PATCH_FAILURE:
            return { ...state, isPatching: false };
        default:
            return state;
    }
}

export const selectModalReport = (state) => {
    const report = state.reports.modalReport ?? {};
    return report;
};

export function logs(
    state = {
        logs: [],
        isFetching: false,
    },
    action,
) {
    switch (action.type) {
        case LogsActionTypes.REQUEST:
            return { ...state, isFetching: true };
        case LogsActionTypes.SUCCESS:
            return {
                ...state,
                logs: action.logs,
                isFetching: false,
            };
        case LogsActionTypes.FAILURE:
            return { ...state, isFetching: false };
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    feedback,
    dialog,
    version,
    user,
    logout,
    skills,
    accounts,
    stats,
    questions,
    reports,
    logs,
});

export default rootReducer;
