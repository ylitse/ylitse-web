import {
    updateKeys,
    camelify,
    snakify,
    combineToAccount,
    constructAccountList,
    extractAccount,
    extractUser,
    extractMentor,
    constructReportList,
    constructLogsList,
} from './utils';
import { DialogTypes } from './constants';

export const FeedbackActionTypes = {
    ENQUEUE: 'FEEDBACK_ENQUEUE',
    DEQUEUE: 'FEEDBACK_DEQUEUE',
};

export const VersionActionTypes = {
    REQUEST: 'VERSION_REQUEST',
    SUCCESS: 'VERSION_SUCCESS',
    FAILURE: 'VERSION_FAILURE',
};

export const QuestionsActionTypes = {
    REQUEST: 'QUESTIONS_REQUEST',
    SUCCESS: 'QUESTIONS_SUCCESS',
    FAILURE: 'QUESTIONS_FAILURE',
    SET_QUESTION_RULES: 'SET_QUESTION_RULES',
    SET_QUESTION: 'SET_QUESTION',
    CREATE_REQUEST: 'QUESTION_CREATE_REQUEST',
    CREATE_FAILURE: 'QUESTION_CREATE_FAILURE',
    DELETE_REQUEST: 'QUESTION_DELETE_REQUEST',
    DELETE_FAILURE: 'QUESTION_DELETE_FAILURE',
    DELETE_SUCCESS: 'QUESTION_DELETE_SUCCESS',
};

export const UserActionTypes = {
    REQUEST: 'USER_REQUEST',
    SUCCESS: 'USER_SUCCESS',
    FAILURE: 'USER_FAILURE',
};

export const LogoutActionTypes = {
    REQUEST: 'LOGOUT_REQUEST',
    SUCCESS: 'LOGOUT_SUCCESS',
    FAILURE: 'LOGOUT_FAILURE',
};

export const SkillsActionTypes = {
    FETCH_REQUEST: 'SKILLS_FETCH_REQUEST',
    FETCH_SUCCESS: 'SKILLS_FETCH_SUCCESS',
    FETCH_FAILURE: 'SKILLS_FETCH_FAILURE',
    CREATE_REQUEST: 'SKILLS_CREATE_REQUEST',
    CREATE_SUCCESS: 'SKILLS_CREATE_SUCCESS',
    CREATE_FAILURE: 'SKILLS_CREATE_FAILURE',
    DELETE_REQUEST: 'SKILLS_DELETE_REQUEST',
    DELETE_SUCCESS: 'SKILLS_DELETE_SUCCESS',
    DELETE_FAILURE: 'SKILLS_DELETE_FAILURE',
};

export const AccountsActionTypes = {
    FETCH_REQUEST: 'ACCOUNTS_FETCH_REQUEST',
    FETCH_SUCCESS: 'ACCOUNTS_FETCH_SUCCESS',
    FETCH_FAILURE: 'ACCOUNTS_FETCH_FAILURE',
    CREATE_REQUEST: 'ACCOUNTS_CREATE_REQUEST',
    CREATE_SUCCESS: 'ACCOUNTS_CREATE_SUCCESS',
    CREATE_FAILURE: 'ACCOUNTS_CREATE_FAILURE',
    UPDATE_REQUEST: 'ACCOUNTS_UPDATE_REQUEST',
    UPDATE_SUCCESS: 'ACCOUNTS_UPDATE_SUCCESS',
    UPDATE_FAILURE: 'ACCOUNTS_UPDATE_FAILURE',
    DELETE_REQUEST: 'ACCOUNTS_DELETE_REQUEST',
    DELETE_SUCCESS: 'ACCOUNTS_DELETE_SUCCESS',
    DELETE_FAILURE: 'ACCOUNTS_DELETE_FAILURE',
};

export const StatsActionTypes = {
    REQUEST: 'STATS_REQUEST',
    SUCCESS: 'STATS_SUCCESS',
    FAILURE: 'STATS_FAILURE',
};

export const DialogActionTypes = {
    OPEN: 'DIALOG_OPEN',
    CLOSE: 'DIALOG_CLOSE',
};

export const ReportsActionTypes = {
    REQUEST: 'REPORTS_REQUEST',
    SUCCESS: 'REPORTS_SUCCESS',
    FAILURE: 'REPORTS_FAILURE',
    SET_REPORT: 'SET_REPORT',
    DELETE_REQUEST: 'REPORTS_DELETE_REQUEST',
    DELETE_SUCCESS: 'REPORTS_DELETE_SUCCESS',
    DELETE_FAILURE: 'REPORTS_DELETE_FAILURE',
    PATCH_REQUEST: 'REPORTS_PATCH_REQUEST',
    PATCH_SUCCESS: 'REPORTS_PATCH_SUCCESS',
    PATCH_FAILURE: 'REPORTS_PATCH_FAILURE',
};

export const LogsActionTypes = {
    REQUEST: 'LOGS_REQUEST',
    SUCCESS: 'LOGS_SUCCESS',
    FAILURE: 'LOGS_FAILURE',
};

export function enqueueFeedback(message) {
    return {
        type: FeedbackActionTypes.ENQUEUE,
        message,
    };
}

export function dequeueFeedback() {
    return {
        type: FeedbackActionTypes.DEQUEUE,
    };
}

async function request(endpoint, method, headers, body) {
    const resp = await fetch(endpoint, {
        method,
        headers,
        body,
        redirect: 'follow',
        credentials: 'include',
    });
    const contentType = resp.headers.get('Content-Type');
    const isJson = contentType && contentType.includes('application/json');

    if (resp.redirected) {
        window.location.replace(resp.url);

        // Don't really show the message
        throw new Error('');
    }

    if (!resp.ok || resp.status >= 400) {
        if (isJson) {
            const json = await resp.json();

            throw new Error(json.message || resp.statusText);
        }

        throw new Error(resp.statusText);
    }

    return isJson ? resp.json() : null;
}

function GET(endpoint, requestAction, failureAction, receiveAction) {
    return async (dispatch) => {
        dispatch(requestAction());

        try {
            const data = await request(endpoint, 'GET');

            if (data && 'resources' in data) {
                dispatch(
                    receiveAction(data.resources.map(r => updateKeys(r, camelify))),
                );
            } else {
                dispatch(receiveAction(updateKeys(data, camelify)));
            }
        } catch (error) {
            await dispatch(failureAction());
            dispatch(enqueueFeedback(error.message));
        }
    };
}

function write(
    endpoint,
    object,
    requestAction,
    failureAction,
    receiveAction,
    receiveMessage,
    method,
) {
    return async (dispatch) => {
        dispatch(requestAction());

        try {
            const headers = { 'Content-Type': 'application/json' };
            const body = JSON.stringify(updateKeys(object, snakify));
            const data = await request(endpoint, method, headers, body);

            if (data) {
                await dispatch(receiveAction(updateKeys(data, camelify)));
            } else {
                await dispatch(receiveAction());
            }

            if (receiveMessage) {
                dispatch(enqueueFeedback(receiveMessage));
            }
        } catch (error) {
            await dispatch(failureAction());
            dispatch(enqueueFeedback(error.message));
        }
    };
}

function POST(...args) {
    return write(...args, 'POST');
}

function PUT(...args) {
    return write(...args, 'PUT');
}

function PATCH(...args) {
    return write(...args, 'PATCH');
}

function DELETE(
    endpoint,
    requestAction,
    failureAction,
    receiveAction,
    receiveMessage,
) {
    return async (dispatch) => {
        dispatch(requestAction());

        try {
            await request(endpoint, 'DELETE');
            await dispatch(receiveAction());
            dispatch(enqueueFeedback(receiveMessage));
        } catch (error) {
            await dispatch(failureAction());
            dispatch(enqueueFeedback(error.message));
        }
    };
}

export function fetchQuestions() {
    return (dispatch, getState) => {
        if (getState().questions.isFetching) {
            return null;
        }

        return dispatch(
            GET(
                '/api/feedback/questions',
                () => ({ type: QuestionsActionTypes.REQUEST }),
                () => ({ type: QuestionsActionTypes.FAILURE }),
                questions => ({ type: QuestionsActionTypes.SUCCESS, questions }),
            ),
        );
    };
}

export function createQuestion(question) {
    return (dispatch, getState) => {
        if (getState().questions.isCreating) {
            return null;
        }

        return dispatch(
            POST(
                '/api/feedback/questions',
                question,
                () => ({ type: QuestionsActionTypes.CREATE_REQUEST }),
                () => ({ type: QuestionsActionTypes.CREATE_FAILURE }),
                () => GET(
                    '/api/feedback/questions',
                    () => ({ type: QuestionsActionTypes.REQUEST }),
                    () => ({ type: QuestionsActionTypes.FAILURE }),
                    questions => ({ type: QuestionsActionTypes.SUCCESS, questions }),
                ),
                'Question created',
            ),
        );
    };
}

export function deleteQuestion(id) {
    return (dispatch, getState) => {
        if (getState().questions.isDeleting) {
            return null;
        }

        return dispatch(
            DELETE(
                `/api/feedback/questions/${id}`,
                () => ({ type: QuestionsActionTypes.DELETE_REQUEST }),
                () => ({ type: QuestionsActionTypes.DELETE_FAILURE }),
                () => ({ type: QuestionsActionTypes.DELETE_SUCCESS, id }),
                'Question deleted',
            ),
        );
    };
}

export function fetchVersion() {
    return (dispatch, getState) => {
        if (getState().version.api || getState().version.isFetching) {
            return null;
        }

        return dispatch(
            GET(
                '/api/version',
                () => ({ type: VersionActionTypes.REQUEST }),
                () => ({ type: VersionActionTypes.FAILURE }),
                version => ({ type: VersionActionTypes.SUCCESS, version }),
            ),
        );
    };
}

export function fetchUser() {
    return (dispatch, getState) => {
        if (getState().user.isFetching) {
            return null;
        }

        return dispatch(
            GET(
                '/api/myuser',
                () => ({ type: UserActionTypes.REQUEST }),
                () => ({ type: UserActionTypes.FAILURE }),
                (json) => {
                    const { account, user, mentor } = json;
                    let args = [account, user, mentor].filter(x => x !== undefined);
                    args = args.map(x => updateKeys(x, camelify));
                    const combined = combineToAccount(...args);
                    return {
                        type: UserActionTypes.SUCCESS,
                        user: combined,
                    };
                },
            ),
        );
    };
}

function reload() {
    window.location.replace('/login');

    return { type: '' };
}

export function logout() {
    return (dispatch, getState) => {
        if (getState().logout.inProgress) {
            return null;
        }

        return dispatch(
            GET(
                '/api/logout',
                () => ({ type: LogoutActionTypes.REQUEST }),
                () => ({ type: LogoutActionTypes.FAILURE }),
                reload,
            ),
        );
    };
}

export function fetchSkills() {
    return (dispatch, getState) => {
        if (getState().skills.isFetching) {
            return null;
        }

        return dispatch(
            GET(
                '/api/skills',
                () => ({ type: SkillsActionTypes.FETCH_REQUEST }),
                () => ({ type: SkillsActionTypes.FETCH_FAILURE }),
                skills => ({ type: SkillsActionTypes.FETCH_SUCCESS, skills }),
            ),
        );
    };
}

export function createSkill(skillName) {
    return (dispatch, getState) => {
        if (getState().skills.isCreating) {
            return null;
        }

        return dispatch(
            POST(
                '/api/skills',
                { name: skillName },
                () => ({ type: SkillsActionTypes.CREATE_REQUEST }),
                () => ({ type: SkillsActionTypes.CREATE_FAILURE }),
                skill => ({ type: SkillsActionTypes.CREATE_SUCCESS, skill }),
                `${skillName} skill created`,
            ),
        );
    };
}

export function deleteSkill(skill) {
    return (dispatch, getState) => {
        if (getState().skills.isDeleting) {
            return null;
        }

        const { id, name } = skill;

        return dispatch(
            DELETE(
                `/api/skills/${id}`,
                () => ({ type: SkillsActionTypes.DELETE_REQUEST }),
                () => ({ type: SkillsActionTypes.DELETE_FAILURE }),
                () => ({ type: SkillsActionTypes.DELETE_SUCCESS, id }),
                `${name} skill deleted`,
            ),
        );
    };
}

export function fetchAccounts() {
    return (dispatch, getState) => {
        if (getState().accounts.isFetching) {
            return null;
        }

        return dispatch(
            GET(
                '/api/accounts',
                () => ({ type: AccountsActionTypes.FETCH_REQUEST }),
                () => ({ type: AccountsActionTypes.FETCH_FAILURE }),
                accounts => GET(
                    '/api/users',
                    () => ({ type: AccountsActionTypes.FETCH_REQUEST }),
                    () => ({ type: AccountsActionTypes.FETCH_FAILURE }),
                    users => GET(
                        '/api/mentors',
                        () => ({ type: AccountsActionTypes.FETCH_REQUEST }),
                        () => ({ type: AccountsActionTypes.FETCH_FAILURE }),
                        mentors => ({
                            type: AccountsActionTypes.FETCH_SUCCESS,
                            accounts: constructAccountList(accounts, users, mentors),
                        }),
                    ),
                ),
            ),
        );
    };
}

export function createAccount(newAccount) {
    return (dispatch, getState) => {
        if (getState().accounts.isCreating) {
            return null;
        }

        return dispatch(
            POST(
                '/api/accounts',
                {
                    account: extractAccount(newAccount),
                    password: newAccount.password,
                },
                () => ({ type: AccountsActionTypes.CREATE_REQUEST }),
                () => ({ type: AccountsActionTypes.CREATE_FAILURE }),
                created => PUT(
                    `/api/users/${created.user.id}`,
                    {
                        ...created.user,
                        ...extractUser(newAccount),
                    },
                    () => ({ type: AccountsActionTypes.CREATE_REQUEST }),
                    () => ({ type: AccountsActionTypes.CREATE_FAILURE }),
                    (user) => {
                        const account = updateKeys(created.account, camelify);
                        if (user.role !== 'mentor') {
                            return {
                                type: AccountsActionTypes.CREATE_SUCCESS,
                                account: combineToAccount(account, user),
                            };
                        }
                        return PUT(
                            `/api/mentors/${created.mentor.id}`,
                            {
                                ...created.mentor,
                                ...extractMentor(newAccount),
                            },
                            () => ({ type: AccountsActionTypes.CREATE_REQUEST }),
                            () => ({ type: AccountsActionTypes.CREATE_FAILURE }),
                            mentor => ({
                                type: AccountsActionTypes.CREATE_SUCCESS,
                                account: combineToAccount(account, user, mentor),
                            }),
                            false,
                        );
                    },
                    false,
                ),
                `${newAccount.username}'s account created`,
            ),
        );
    };
}

export function updateAccount(formAccount) {
    return (dispatch, getState) => {
        if (getState().accounts.isUpdating) {
            return null;
        }
        if (formAccount.password) {
            return dispatch(
                PUT(
                    `/api/accounts/${formAccount.account.id}/password`,
                    {
                        password: formAccount.password,
                        login_name: formAccount.username,
                    },
                    () => ({ type: AccountsActionTypes.UPDATE_REQUEST }),
                    () => ({ type: AccountsActionTypes.UPDATE_FAILURE }),
                    () => {
                        const account = { ...formAccount };
                        delete account.password;
                        return {
                            type: AccountsActionTypes.UPDATE_SUCCESS,
                            account,
                        };
                    },
                    'Password updated',
                ),
            );
        }

        const account = {
            ...formAccount.account,
            ...extractAccount(formAccount),
        };
        const user = {
            ...formAccount.user,
            ...extractUser(formAccount),
        };
        const mentor = {
            ...formAccount.mentor,
            ...extractMentor(formAccount),
        };

        return dispatch(
            PUT(
                `/api/accounts/${account.id}`,
                account,
                () => ({ type: AccountsActionTypes.UPDATE_REQUEST }),
                () => ({ type: AccountsActionTypes.UPDATE_FAILURE }),
                updatedAccount => PUT(
                    `/api/users/${user.id}`,
                    user,
                    () => ({ type: AccountsActionTypes.UPDATE_REQUEST }),
                    () => ({ type: AccountsActionTypes.UPDATE_FAILURE }),
                    (updatedUser) => {
                        if (user.role !== 'mentor') {
                            return {
                                type: AccountsActionTypes.UPDATE_SUCCESS,
                                account: combineToAccount(updatedAccount, updatedUser),
                            };
                        }
                        return PUT(
                            `/api/mentors/${mentor.id}`,
                            mentor,
                            () => ({ type: AccountsActionTypes.UPDATE_REQUEST }),
                            () => ({ type: AccountsActionTypes.UPDATE_FAILURE }),
                            updatedMentor => ({
                                type: AccountsActionTypes.UPDATE_SUCCESS,
                                account: combineToAccount(
                                    updatedAccount,
                                    updatedUser,
                                    updatedMentor,
                                ),
                            }),
                            false,
                        );
                    },
                    false,
                ),
                `${formAccount.username}'s account updated`,
            ),
        );
    };
}

export function deleteAccount(account) {
    return (dispatch, getState) => {
        if (getState().accounts.isDeleting) {
            return null;
        }

        const { id, username } = account;

        return dispatch(
            DELETE(
                `/api/accounts/${id}`,
                () => ({ type: AccountsActionTypes.DELETE_REQUEST }),
                () => ({ type: AccountsActionTypes.DELETE_FAILURE }),
                () => ({ type: AccountsActionTypes.DELETE_SUCCESS, id }),
                `${username}'s account deleted`,
            ),
        );
    };
}

export function addAccount() {
    return {
        type: DialogActionTypes.OPEN,
        dialogType: DialogTypes.ADD_ACCOUNT,
        dialogProps: {},
    };
}

export function editAccount(account) {
    return {
        type: DialogActionTypes.OPEN,
        dialogType: DialogTypes.EDIT_ACCOUNT,
        dialogProps: { account },
    };
}

export function closeDialog() {
    return {
        type: DialogActionTypes.CLOSE,
    };
}

export function setPassword(account) {
    return {
        type: DialogActionTypes.OPEN,
        dialogType: DialogTypes.SET_PASSWORD,
        dialogProps: { account },
    };
}

export function fetchStats() {
    return (dispatch, getState) => {
        if (getState().stats.isFetching) {
            return null;
        }

        return dispatch(
            GET(
                '/api/stats',
                () => ({ type: StatsActionTypes.REQUEST }),
                () => ({ type: StatsActionTypes.FAILURE }),
                stats => ({ type: StatsActionTypes.SUCCESS, stats }),
            ),
        );
    };
}

export function setQuestionRules(key, value) {
    return { type: QuestionsActionTypes.SET_QUESTION_RULES, key, value };
}

export function setQuestion(question) {
    return { type: QuestionsActionTypes.SET_QUESTION, question };
}

export function setReport(modalReport) {
    return { type: ReportsActionTypes.SET_REPORT, modalReport };
}

export function fetchReports() {
    return (dispatch, getState) => {
        if (getState().reports.isFetching) {
            return null;
        }

        return dispatch(
            GET(
                '/api/reports',
                () => ({ type: ReportsActionTypes.REQUEST }),
                () => ({ type: ReportsActionTypes.FAILURE }),
                basicReports => GET(
                    '/api/users',
                    () => ({ type: ReportsActionTypes.REQUEST }),
                    () => ({ type: ReportsActionTypes.FAILURE }),
                    users => GET(
                        '/api/accounts',
                        () => ({ type: ReportsActionTypes.REQUEST }),
                        () => ({ type: ReportsActionTypes.FAILURE }),
                        accounts => ({
                            type: ReportsActionTypes.SUCCESS,
                            reports: constructReportList(basicReports, users, accounts),
                        }),
                    ),
                ),
            ),
        );
    };
}

export function deleteReport(reportId) {
    return (dispatch, getState) => {
        if (getState().reports.isDeleting) {
            return null;
        }

        return dispatch(
            DELETE(
                `/api/reports/${reportId}`,
                () => ({ type: ReportsActionTypes.DELETE_REQUEST }),
                () => ({ type: ReportsActionTypes.DELETE_FAILURE }),
                () => ({ type: ReportsActionTypes.DELETE_SUCCESS, reportId }),
                `${reportId} report deleted`,
            ),
        );
    };
}

export function patchReport({ reportId, ...updated }) {
    return (dispatch, getState) => {
        if (getState().reports.isPatching) {
            return null;
        }
        return dispatch(
            PATCH(
                `/api/reports/${reportId}`,
                updated,
                () => ({ type: ReportsActionTypes.PATCH_REQUEST }),
                () => ({ type: ReportsActionTypes.PATCH_FAILURE }),
                () => ({
                    type: ReportsActionTypes.PATCH_SUCCESS,
                    reportId,
                    updated,
                }),
                false,
            ),
        );
    };
}

export function fetchLogs() {
    return (dispatch, getState) => {
        if (getState().logs.isFetching) {
            return null;
        }

        return dispatch(
            GET(
                '/api/chat_review_logs',
                () => ({ type: LogsActionTypes.REQUEST }),
                () => ({ type: LogsActionTypes.FAILURE }),
                basicLogs => GET(
                    '/api/users',
                    () => ({ type: LogsActionTypes.REQUEST }),
                    () => ({ type: LogsActionTypes.FAILURE }),
                    users => GET(
                        '/api/accounts',
                        () => ({ type: LogsActionTypes.REQUEST }),
                        () => ({ type: LogsActionTypes.FAILURE }),
                        accounts => ({
                            type: LogsActionTypes.SUCCESS,
                            logs: constructLogsList(basicLogs, users, accounts),
                        }),
                    ),
                ),
            ),
        );
    };
}
