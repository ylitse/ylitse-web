import {
    capitalize, camelify, snakify, updateKeys, updateWith, sortBy,
    extractAccount, extractUser, extractMentor, combineToAccount, validateEmail,
} from './utils';

describe('capitalize', () => {
    test('works with lower case', () => {
        expect(capitalize('test')).toEqual('Test');
    });

    test('works with empty string', () => {
        expect(capitalize('')).toEqual('');
    });
});

describe('camelify', () => {
    test('works with snake case', () => {
        expect(camelify('test_snake_case')).toEqual('testSnakeCase');
    });

    test('works with empty string', () => {
        expect(camelify('')).toEqual('');
    });
});

describe('snakify', () => {
    test('works with camel case', () => {
        expect(snakify('testCamelCase')).toEqual('test_camel_case');
    });

    test('works with empty string', () => {
        expect(snakify('')).toEqual('');
    });
});

describe('updateKeys', () => {
    test('works with simple object', () => {
        const obj = { key1: 'value1', key2: 'value2' };
        expect(updateKeys(obj, k => k)).toEqual(obj);
    });

    test('works with empty object', () => {
        const obj = {};
        expect(updateKeys(obj, k => k)).toEqual(obj);
    });
});

describe('updateWith', () => {
    test('works with simple list', () => {
        const objs = [{ id: '0', name: 'A' }, { id: '1', name: 'B' }];
        expect(updateWith({ id: '1', name: 'C' }, objs)).toEqual([
            { id: '0', name: 'A' },
            { id: '1', name: 'C' },
        ]);
    });

    test('works with empty list', () => {
        const obj = { id: '0', name: 'A' };
        expect(updateWith(obj, [])).toEqual([obj]);
    });
});

describe('sortBy', () => {
    test('works with simple list', () => {
        const objs = [{ name: 'B' }, { name: 'A' }];
        expect(sortBy('name', objs)).toEqual([
            { name: 'A' },
            { name: 'B' },
        ]);
    });

    test('works with empty list', () => {
        const objs = [];
        expect(sortBy('name', objs)).toEqual(objs);
    });

    test('works with list of empty objects', () => {
        const objs = [{}, {}];
        expect(sortBy('name', objs)).toEqual(objs);
    });
});

describe('combineToAccount', () => {
    const { account, user, mentor } = {
        account: {
            role: 'mentor',
            loginName: 'janteri',
            email: 'janteri@mail.fi',
            phone: '1234567890',

            id: 'wtm0sF2m92L_PljrG71GM2RU4CDcOUxrlovVpqCnXf0',
            created: '2018-08-03T11:34:58.168290',
            updated: '2018-08-03T11:34:58.182284',
            active: true,
        },
        user: {
            displayName: 'maikkeli',
            role: 'mentor',
            accountId: 'wtm0sF2m92L_PljrG71GM2RU4CDcOUxrlovVpqCnXf0',
            id: 'msOx_d8XmUncJ6sC2szkehTNvdCDc5hV-dAnWqGjS0c',
            created: '2018-08-03T11:34:58.182681',
            updated: '2018-08-03T11:34:58.183154',
            active: true,
        },
        mentor: {
            birthYear: 1990,
            communicationChannels: ['chat'],
            displayName: 'maikkeli',
            gender: 'male',
            languages: ['fi', 'sv'],
            region: 'Helsinki',
            skills: ['vim', 'js'],
            story: 'Cool story.',
            accountId: 'wtm0sF2m92L_PljrG71GM2RU4CDcOUxrlovVpqCnXf0',
            userId: 'msOx_d8XmUncJ6sC2szkehTNvdCDc5hV-dAnWqGjS0c',
            id: 'cRyIdqEc7LMl4ceMZItdLOKwBNF1uFdjZ9r_At2nmg8',
            created: '2018-08-03T11:34:58.183535',
            updated: '2018-08-03T11:34:58.184336',
            active: true,
        },
    };

    const combined = {
        id: account.id,

        role: 'mentor',
        username: 'janteri',
        nickname: 'maikkeli',
        email: 'janteri@mail.fi',
        phone: '1234567890',
        gender: 'male',
        birthYear: 1990,
        area: 'Helsinki',
        languages: ['fi', 'sv'],
        skills: ['vim', 'js'],
        communicationChannels: ['chat'],
        story: 'Cool story.',

        account,
        user,
        mentor,
    };

    test('combine account', () => {
        expect(combineToAccount(account, user, mentor)).toEqual(combined);
    });
});

describe('AccountExtraction functions', () => {
    const accountForm = {
        role: 'mentor',
        username: 'janteri',
        password: 'asdf',
        nickname: 'maikkeli',
        email: 'janteri@email.fi',
        phone: '1234567890',
        gender: 'male',
        birthYear: 1990,
        area: 'Helsinki',
        languages: ['fi', 'sv'],
        skills: ['vim', 'js'],
        communicationChannels: ['chat'],
        story: 'Cool story.',
    };

    test('Extract Account', () => {
        const account = {
            login_name: 'janteri',
            role: 'mentor',
            email: 'janteri@email.fi',
            phone: '1234567890',
        };
        expect(extractAccount(accountForm)).toEqual(account);
    });
    test('Extract User', () => {
        const user = {
            display_name: 'maikkeli',
            role: 'mentor',
        };
        expect(extractUser(accountForm)).toEqual(user);
    });
    test('Extract Mentor', () => {
        const mentor = {
            birth_year: 1990,
            communication_channels: ['chat'],
            display_name: 'maikkeli',
            gender: 'male',
            languages: ['fi', 'sv'],
            region: 'Helsinki',
            skills: ['vim', 'js'],
            story: 'Cool story.',
        };
        expect(extractMentor(accountForm)).toEqual(mentor);
    });
});

describe('validateEmail', () => {
    ['plainaddress',
        '#@%^%#$@#$@#.com',
        '@example.com',
        'Joe Smith <email@example.com>',
        'email.example.com',
        'email@example@example.com',
        'あいうえお@example.com',
        'email@example.com (Joe Smith)',
        'email@example',
        'email@111.222.333.44444',
        '',
        2,
        null,
        undefined,
        'a',
        `${'x'.repeat(400)}@example.org`,
    ].forEach((invalidEmail) => {
        test(`returns false for invalid address ${invalidEmail}`, () => {
            expect(validateEmail(invalidEmail)).toEqual(false);
        });
    });

    ['email@example.com',
        'firstname.lastname@example.com',
        'email@subdomain.example.com',
        'firstname+lastname@example.com',
        '1234567890@example.com',
        'email@example-one.com',
        '_______@example.com',
        'email@example.name',
        'email@example.museum',
        'email@example.co.jp',
        'firstname-lastname@example.com',
        "!#$%&'*+-/=?^_`.{|}~@kebab.fi",
        `${'x'.repeat(63)}@${'e'.repeat(191)}.${'f'.repeat(62)}`,
    ]
        .forEach((validEmail) => {
            test(`returns true for valid address ${validEmail}`, () => {
                expect(validateEmail(validEmail)).toEqual(true);
            });
        });
});
