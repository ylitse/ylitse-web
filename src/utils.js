export function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

export function camelify(str) {
    return str.replace(/_(\w|$)/g, (_, ch) => ch.toUpperCase());
}

export function snakify(str) {
    return str.replace(/([a-z\d])([A-Z])/g, '$1_$2').toLowerCase();
}

export function updateKeys(obj, func) {
    return Object.keys(obj).reduce(
        (updated, key) => ({
            ...updated,
            [func(key)]: obj[key],
        }),
        {},
    );
}

export function updateWith(item, items) {
    const id = items.findIndex(a => a.id === item.id);

    return [...items.slice(0, id), item, ...items.slice(id + 1)];
}

export function sortBy(key, items) {
    return items.sort((a, b) => {
        const valueA = a[key] || '';
        const valueB = b[key] || '';

        return valueA.toLowerCase().localeCompare(valueB.toLowerCase());
    });
}

export function combineToAccount(account, user, mentor) {
    let combinedAccount = {
        id: account.id,

        email: account.email,
        nickname: user.displayName,
        phone: account.phone,
        role: account.role,
        username: account.loginName,

        account,
        user,
    };
    if (mentor) {
        combinedAccount = {
            ...combinedAccount,
            area: mentor.region,
            birthYear: mentor.birthYear,
            communicationChannels: mentor.communicationChannels,
            gender: mentor.gender,
            languages: mentor.languages,
            skills: mentor.skills,
            story: mentor.story,
            isVacationing: mentor.isVacationing,
            statusMessage: mentor.statusMessage,

            mentor,
        };
    }
    return combinedAccount;
}

export function constructAccountList(accountList, userList, mentorList) {
    const userObject = userList.reduce(
        (acc, user) => ({ ...acc, [user.accountId]: user }),
        {},
    );
    const mentorObject = mentorList.reduce(
        (acc, mentor) => ({ ...acc, [mentor.accountId]: mentor }),
        {},
    );
    return accountList.reduce(
        (acc, a) => [
            ...acc,
            combineToAccount(a, userObject[a.id], mentorObject[a.id]),
        ],
        [],
    );
}

export function extractAccount(accountForm) {
    return {
        login_name: accountForm.username,
        role: accountForm.role,
        email: accountForm.email,
        phone: accountForm.phone,
    };
}

export function extractUser(accountForm) {
    return {
        display_name: accountForm.nickname,
        role: accountForm.role,
    };
}

export function extractMentor(accountForm) {
    return {
        birth_year: accountForm.birthYear,
        communication_channels: accountForm.communicationChannels,
        display_name: accountForm.nickname,
        gender: accountForm.gender,
        languages: accountForm.languages,
        region: accountForm.area,
        skills: accountForm.skills,
        story: accountForm.story,
        isVacationing: accountForm.isVacationing,
        statusMessage: accountForm.statusMessage,
    };
}

export function validateEmail(value) {
    if (!value || value.length > 320) {
        return false;
    }
    return /^[a-zA-Z0-9!#$%&'*+\-/=?^_`.{|}~]{1,64}@[a-z0-9.-]+\.[a-z]{2,64}$/.test(
        value,
    );
}

const findUserName = (users, accounts, id) => {
    const user = users.find(u => u.id === id);
    if (!user) {
        return 'Deleted user';
    }
    const account = accounts.find(u => u.id === user.accountId);
    if (!account) {
        return 'Deleted user';
    }
    const displayName = user.displayName ? user.displayName : account.loginName;
    return displayName;
};

export function constructReportList(basicReports, users, accounts) {
    const returnableReports = [];
    basicReports.forEach((report) => {
        const newReport = {
            id: report.id,
            contactField: report.contactField,
            created: report.created,
            active: report.active,
            reportReason: report.reportReason,
            reportedUserId: report.reportedUserId,
            reporterUserId: report.reporterUserId,
            status: report.status,
            comment: report.comment,
            updated: report.updated,
            reportedUserName: findUserName(users, accounts, report.reportedUserId),
            reporterUserName: findUserName(users, accounts, report.reporterUserId),
        };
        returnableReports.push(newReport);
    });
    return returnableReports;
}

export function patchReportList(reports, { reportId, ...updated }) {
    return reports.map(report => (report.id === reportId ? { ...report, ...updated } : report));
}

export const requestData = async (endpoint, method, headers, body) => {
    const resp = await fetch(endpoint, {
        method,
        headers,
        body,
        redirect: 'follow',
        credentials: 'include',
    });
    const contentType = resp.headers.get('Content-Type');
    const isJson = contentType && contentType.includes('application/json');

    if (resp.redirected) {
        window.location.replace(resp.url);

        // Don't really show the message
        throw new Error('');
    }

    if (!resp.ok || resp.status >= 400) {
        if (isJson) {
            const json = await resp.json();

            throw new Error(json.message || resp.statusText);
        }

        throw new Error(resp.statusText);
    }

    return isJson ? resp.json() : null;
};

export function constructLogsList(basicLogs, users, accounts) {
    const returnableLogs = [];
    basicLogs.forEach((log) => {
        const newLog = {
            id: log.id,
            created: log.created,
            chatUserId1: log.chatUserId1,
            chatUserName1: findUserName(users, accounts, log.chatUserId1),
            chatUserId2: log.chatUserId2,
            chatUserName2: findUserName(users, accounts, log.chatUserId2),
            reviewerUserId: log.reviewerUserId,
            reviewerUserName: findUserName(users, accounts, log.reviewerUserId),
        };
        returnableLogs.push(newLog);
    });
    return returnableLogs;
}
