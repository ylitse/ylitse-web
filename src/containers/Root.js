import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom';
import PropTypes from 'prop-types';

import { UserRoles } from '../constants';
import { fetchUser as fetchUserAction } from '../actions';
import DialogWindow from './DialogWindow';
import Feedback from './Feedback';
// eslint-disable-next-line import/no-named-as-default
import Navigation from './Navigation';
// eslint-disable-next-line import/no-named-as-default
import AccountPage from './AccountPage';
// eslint-disable-next-line import/no-named-as-default
import SkillPage from './SkillPage';
// eslint-disable-next-line import/no-named-as-default
import StatsPage from './StatsPage';
// eslint-disable-next-line import/no-named-as-default
import ProfilePage from './ProfilePage';
import MissingPage from '../components/MissingPage';
// eslint-disable-next-line import/no-named-as-default
import QuestionsPage from './QuestionsPage';
// eslint-disable-next-line import/no-named-as-default
import AnswersPage from './AnswersPage';
// eslint-disable-next-line import/no-named-as-default
import ReportsPage from './ReportsPage';
import LogsPage from './LogsPage';

export class Root extends Component {
  static propTypes = {
      username: PropTypes.string.isRequired,
      role: PropTypes.string.isRequired,
      fetchUser: PropTypes.func.isRequired,
  };

  componentDidMount() {
      const { fetchUser } = this.props;

      fetchUser();
  }

  render() {
      const { role, username } = this.props;
      const rootPath = role === UserRoles.ADMIN ? '/accounts' : '/profile';

      if (!username) {
          return <Feedback critical />;
      }

      return (
          <BrowserRouter basename={process.env.BASE_PATH || '/'}>
              <main>
                  <Navigation />
                  <Switch>
                      <Route path="/" exact>
                          <Redirect to={rootPath} />
                      </Route>
                      {role === UserRoles.ADMIN && (
                          <Route path="/accounts" exact component={AccountPage} />
                      )}
                      {role === UserRoles.ADMIN && (
                          <Route path="/skills" exact component={SkillPage} />
                      )}
                      {role === UserRoles.ADMIN && (
                          <Route path="/stats" exact component={StatsPage} />
                      )}
                      {role === UserRoles.ADMIN && (
                          <Route path="/questions" exact component={QuestionsPage} />
                      )}
                      {role === UserRoles.ADMIN && (
                          <Route path="/answers" exact component={AnswersPage} />
                      )}
                      {role === UserRoles.ADMIN && (
                          <Route path="/reports" exact component={ReportsPage} />
                      )}
                      {role === UserRoles.ADMIN && (
                          <Route path="/logs" exact component={LogsPage} />
                      )}
                      <Route path="/profile" exact component={ProfilePage} />
                      <Route component={MissingPage} />
                  </Switch>
                  <DialogWindow />
                  <Feedback />
              </main>
          </BrowserRouter>
      );
  }
}

function mapStateToProps({ user }) {
    return {
        username: user.username,
        role: user.role,
    };
}

export default connect(mapStateToProps, { fetchUser: fetchUserAction })(Root);
