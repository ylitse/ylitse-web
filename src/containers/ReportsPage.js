import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
    fetchReports as fetchReportsAction,
    setReport as setReportsAction,
    deleteReport as deleteReportAction,
    patchReport as patchReportAction,
} from '../actions';
import Loading from '../components/Loading';
import SingleReport from '../components/SingleReport';
import Page from '../components/Page';
import ReportListDataGrid from '../components/ReportListDataGrid';

const ReportsPage = ({
    reports,
    isFetchingReports,
    fetchReports,
    setReport,
    deleteReport,
    isDeletingReports,
    isPatching,
    patchReport,
}) => {
    const [isReportOpen, setIsReportOpen] = useState(false);

    useEffect(() => {
        fetchReports();
    }, [fetchReports]);

    const openReport = (event) => {
        setReport(event.row);
        setIsReportOpen(true);
    };

    const closeReport = () => {
        setIsReportOpen(false);
    };

    return (
        <Page header="Reports">
            {isFetchingReports || isDeletingReports || isPatching ? (
                <Loading />
      ) : (
          <ReportListDataGrid reports={reports} openReport={openReport} />
      )}
            {isReportOpen && (
                <SingleReport
                    closeReport={closeReport}
                    deleteReport={deleteReport}
                    patchReport={patchReport}
                />
            )}
        </Page>
    );
};

ReportsPage.propTypes = {
    reports: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string,
            reported_user_id: PropTypes.string,
            report_reason: PropTypes.string,
        }),
    ).isRequired,
    isFetchingReports: PropTypes.bool.isRequired,
    fetchReports: PropTypes.func.isRequired,
    setReport: PropTypes.func.isRequired,
    deleteReport: PropTypes.func.isRequired,
    isDeletingReports: PropTypes.bool.isRequired,
    isPatching: PropTypes.bool.isRequired,
    patchReport: PropTypes.func.isRequired,
};

const mapStateToProps = ({ reports }) => ({
    reports: reports.reports,
    isFetchingReports: reports.isFetching,
    isDeletingReports: reports.isDeleting,
    isPatching: reports.isPatching,
});

export default connect(mapStateToProps, {
    fetchReports: fetchReportsAction,
    setReport: setReportsAction,
    deleteReport: deleteReportAction,
    patchReport: patchReportAction,
})(ReportsPage);
