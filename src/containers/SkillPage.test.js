import React from 'react';
import { render, screen, within } from '@testing-library/react';

import { SkillPage } from './SkillPage';

describe('SkillPage', () => {
    const props = {
        skills: [{ id: '0', name: 'Test skill' }],
        isFetching: false,
        fetchSkills: () => {},
        createSkill: () => {},
        deleteSkill: () => {},
    };

    test('renders heading', () => {
        render(<SkillPage {...props} />);

        expect(screen.getByText('Mentor skills')).toBeInTheDocument();
    });

    test('renders skilllist with item', () => {
        render(<SkillPage {...props} />);
        const list = screen.getByRole('list');
        const item = within(list).getByText(/Test/i);
        expect(item).toBeInTheDocument();
    });
});
