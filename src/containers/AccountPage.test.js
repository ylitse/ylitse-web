import React from 'react';
import { render, screen, within } from '@testing-library/react';

import { AccountPage } from './AccountPage';

// These are the minimum data that will make tests pass
// but there is _alot_ more inside accounts
const props = {
    accounts: [
        {
            id: 'testxyz',
            username: 'test',
            role: 'mentee',
            account: {
                id: 'testxyz',
                loginName: 'test',
                role: 'mentee',
                created: '2022-02-19T15:50:45.962507',
            },
        },
        {
            id: 'testabc',
            username: 'test_2',
            role: 'mentor',
            account: {
                id: 'testabc',
                loginName: 'test_2',
                role: 'mentor',
                created: '2022-02-20T15:50:45.962507',
            },
        },
    ],
    stats: {
        testxyz: { sent: 2, received: 1, banned: 0 },
        testabc: { sent: 1, received: 2, banned: 0 },
    },
    totals: {
        admins: { sent: 0, received: 0, banned: 0 },
        mentors: { sent: 1, received: 2, banned: 0 },
        mentees: { sent: 2, received: 1, banned: 0 },
    },
    isFetchingAccounts: false,
    isFetchingStats: false,
    fetchAccounts: () => {},
    fetchStats: () => {},
    addAccount: () => {},
    editAccount: () => {},
    deleteAccount: () => {},
    setPassword: () => {},
};

describe('AccountPage', () => {
    test('renders sent messages for user correctly', () => {
        render(<AccountPage {...props} />);
        const rowGroups = screen.getAllByRole('grid');
        const tableData = within(rowGroups[0]).getAllByRole('cell');
        expect(tableData[3]).toHaveTextContent(props.stats.testxyz.sent);
    });

    test('renders all totals correctly', () => {
        render(<AccountPage {...props} />);
        expect(screen.getByText(/Total messages, admins/i)).toHaveTextContent(/0/i);
        expect(screen.getByText(/Total messages, mentors/i)).toHaveTextContent(/1/i);
        expect(screen.getByText(/Total messages, mentees/i)).toHaveTextContent(/2/i);
        expect(screen.getByText(/Number of registered mentees/i)).toHaveTextContent(/1/i);
        expect(screen.getByText(/Number of registered mentors/i)).toHaveTextContent(/1/i);
    });
});
