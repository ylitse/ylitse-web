import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import { validateEmail } from '../utils';

import { UserRoles, UserGenders, Languages } from '../constants';
import {
    fetchUser as fetchUserAction, fetchSkills as fetchSkillsAction,
    createAccount as createAccountAction, updateAccount as updateAccountAction,
    closeDialog as closeDialogAction,
} from '../actions';
import Loading from '../components/Loading';
import AccountForm from '../components/AccountForm';
import MentorForm from '../components/MentorForm';

const emptyState = {
    selectedTab: 0,
    account: {
        role: UserRoles.MENTOR,
        username: '',
        password: '',
        nickname: '',
        email: '',
        phone: '',
        gender: UserGenders.MALE,
        birthYear: '',
        area: '',
        languages: [],
        skills: [],
        communicationChannels: [],
        story: '',
        isVacationing: false,
        statusMessage: '',
    },
    errors: {
        username: undefined,
        password: undefined,
    },
    formValid: false,
    inputSkills: '',
    inputLanguages: '',
};

const requiredFields = {
    mentor: ['username', 'password', 'nickname', 'email', 'birthYear'],
    mentee: ['username', 'password', 'nickname'],
    admin: ['username', 'password', 'nickname'],
};

export class AccountDialog extends Component {
    static propTypes = {
        open: PropTypes.bool.isRequired,
        fullScreen: PropTypes.bool.isRequired,
        // eslint-disable-next-line react/forbid-prop-types
        account: PropTypes.object,
        currentUserId: PropTypes.string.isRequired,
        availableSkills: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
        })).isRequired,
        isFetchingSkills: PropTypes.bool.isRequired,
        fetchUser: PropTypes.func.isRequired,
        fetchSkills: PropTypes.func.isRequired,
        createAccount: PropTypes.func.isRequired,
        updateAccount: PropTypes.func.isRequired,
        closeDialog: PropTypes.func.isRequired,
    }

    static defaultProps = {
        account: undefined,
    }

    constructor(props) {
        super(props);

        // Deep-clone empty state
        const defaultState = JSON.parse(JSON.stringify(emptyState));
        const account = props.account ? props.account : defaultState.account;
        const formValid = this.validateForm(account, defaultState.errors);
        this.state = props.account ? {
            ...defaultState,
            account,
            formValid,
        } : defaultState;
        this.label = props.account ? 'Edit account' : 'Create new user';
        this.okLabel = props.account ? 'Save' : 'Create';
        this.isNew = !props.account;
    }

    componentDidMount() {
        const { fetchSkills } = this.props;
        fetchSkills();
    }

    updateValue = ({ target }) => {
        const { name, value } = target;
        const { errors, account } = this.state;
        let valid;
        let formattedValue = value;

        switch (name) {
            case 'username':
                valid = value.length > 2;
                errors.username = valid ? '' : 'Username is too short';
                break;
            case 'password':
                valid = value.length > 6;
                errors.password = valid ? '' : 'Password is too short';
                break;
            case 'nickname':
                valid = value.length > 2;
                errors.nickname = valid ? '' : 'Screen name is too short';
                break;
            case 'phone':
                valid = /^\+?[0-9()-]+$/.test(value);
                errors.phone = valid ? '' : 'Invalid phone number';
                break;
            case 'email':
                valid = validateEmail(value);
                errors.email = valid ? '' : 'Invalid email address';
                break;
            case 'birthYear':
                formattedValue = parseInt(value, 10) || value;
                valid = value === ''
                     || /^\d{4}$/.test(value)
                     && formattedValue >= 1900
                     && formattedValue <= new Date().getFullYear();
                errors.birthYear = valid ? '' : 'Invalid birth year';
                break;
            default:
                break;
        }

        // Reset error for an emptied field
        if (value.length === 0) {
            errors[name] = '';
        }

        const updatedAccount = {
            ...account,
            [target.name]: formattedValue,
        };
        const formValid = this.validateForm(updatedAccount, errors);
        this.setState(prevState => ({
            ...prevState,
            account: updatedAccount,
            errors,
            formValid,
        }));
    }

    validateForm = (account, errors) => {
        const noErrors = Object.values(errors).every(e => e === '');
        const roleFields = requiredFields[account.role];
        const required = account.id ? roleFields.filter(r => r !== 'password') : roleFields;
        const emptyRequired = required.some(key => account[key].length === 0);
        return !emptyRequired && noErrors;
    }

    addSelectedSkills = (skill) => {
        this.setState(prevState => ({
            account: {
                ...prevState.account,
                skills: [...prevState.account.skills, skill],
            },
            inputSkills: '',
        }));
    }

    removeSelectedSkills = (skill) => {
        this.setState(prevState => ({
            inputSkills: '',
            account: {
                ...prevState.account,
                skills: prevState.account.skills.filter(
                    i => i !== skill,
                ),
            },
        }));
    }

    changeSkills = (selectedSkills) => {
        const { account } = this.state;

        if (account.skills.includes(selectedSkills)) {
            this.removeSelectedSkills(selectedSkills);
        } else {
            this.addSelectedSkills(selectedSkills);
        }
    }

    changeIsVacationing = (event) => {
        this.setState(prevState => ({
            account: {
                ...prevState.account,
                isVacationing: event.target.checked,
            },
        }));
    }

    changeSkillsInput = (inputVal) => {
        const { account } = this.state;
        const t = inputVal.split(',');

        if (JSON.stringify(t) !== JSON.stringify(account.skills)) {
            this.setState({ inputSkills: inputVal });
        }
    }

    addSelectedLanguages = (language) => {
        this.setState(prevState => ({
            account: {
                ...prevState.account,
                languages: [...prevState.account.languages, language],
            },
            inputLanguages: '',
        }));
    }

    removeSelectedLanguages = (language) => {
        this.setState(prevState => ({
            inputLanguages: '',
            account: {
                ...prevState.account,
                languages: prevState.account.languages.filter(
                    i => i !== language,
                ),
            },
        }));
    }

    changeLanguages = (selectedLanguages) => {
        const { account } = this.state;

        if (account.skills.includes(selectedLanguages)) {
            this.removeSelectedLanguages(selectedLanguages);
        } else {
            this.addSelectedLanguages(selectedLanguages);
        }
    }

    changeLanguagesInput = (inputVal) => {
        const { account } = this.state;
        const t = inputVal.split(',');

        if (JSON.stringify(t) !== JSON.stringify(account.languages)) {
            this.setState({ inputLanguages: inputVal });
        }
    }

    updateCheckboxes = checkboxes => ({ target }, checked) => {
        this.setState((prevState) => {
            let values = prevState.account[checkboxes];

            if (checked) {
                values = [...new Set([...values, target.value])];
            } else {
                values = values.filter(v => v !== target.value);
            }

            return {
                account: { ...prevState.account, [checkboxes]: values },
            };
        });
    }

    updateTabs = (event, selectedTab) => {
        this.setState({ selectedTab });
    };

    sendAccount = () => {
        const {
            currentUserId, account: accountProp, updateAccount, createAccount,
            fetchUser, closeDialog,
        } = this.props;
        const { account } = this.state;
        const errors = {};

        // Username is required
        if (!account.username) {
            errors.username = 'Username is required';
        }

        // Password is required for new accounts
        if (!account.password && !accountProp) {
            errors.password = 'Password is required';
        }

        if (Object.keys(errors).length > 0) {
            this.setState(prevState => ({
                errors: {
                    ...prevState.errors,
                    ...errors,
                },
            }));

            return;
        }

        if ('id' in account) {
            delete account.password;

            updateAccount(account);
        } else {
            createAccount(account);
        }

        if (account.id === currentUserId) {
            fetchUser();
        }

        closeDialog();
    }

    render() {
        const {
            open, account: accountProp, availableSkills, isFetchingSkills,
            fullScreen, closeDialog,
        } = this.props;
        const {
            selectedTab, account, inputSkills, inputLanguages, formValid,
            errors,
        } = this.state;
        const isMentor = account.role === UserRoles.MENTOR;

        if (isFetchingSkills) {
            return <Loading fullScreen />;
        }

        return (
            <Dialog
                open={open}
                fullScreen={fullScreen}
                fullwidth="true"
                onClose={closeDialog}
            >
                <DialogTitle>{this.label}</DialogTitle>
                <DialogContent>
                    <Tabs
                        value={selectedTab}
                        fullwidth="true"
                        onChange={this.updateTabs}
                    >
                        <Tab label="User account" />
                        <Tab label="Mentor profile" disabled={!isMentor} />
                    </Tabs>
                </DialogContent>
                <DialogContent>
                    {selectedTab === 0 && (
                        <AccountForm
                            account={account}
                            isNew={!accountProp}
                            errors={errors}
                            onValueChange={this.updateValue}
                        />
                    )}
                    {selectedTab === 1 && (
                        <MentorForm
                            account={account}
                            skillsInputValue={inputSkills}
                            languagesInputValue={inputLanguages}
                            selectedSkills={account.skills}
                            selectedLanguages={account.languages}
                            availableSkills={availableSkills}
                            availableLanguages={Languages}
                            errors={errors}
                            onValueChange={this.updateValue}
                            onCheckboxesChange={this.updateCheckboxes}
                            onSkillsChange={this.changeSkills}
                            onSkillsInputChange={this.changeSkillsInput}
                            onSkillsRemove={this.removeSelectedSkills}
                            onLanguagesChange={this.changeLanguages}
                            onLanguagesInputChange={this.changeLanguagesInput}
                            onLanguagesRemove={this.removeSelectedLanguages}
                            onChangeIsVacationing={this.changeIsVacationing}
                            isNew={this.isNew}

                        />
                    )}
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeDialog}>Cancel</Button>
                    <Button
                        disabled={!formValid}
                        color="secondary"
                        onClick={this.sendAccount}
                    >
                        {this.okLabel}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

function mapStateToProps({ user, skills }) {
    return {
        currentUserId: user.id,
        availableSkills: skills.items,
        isFetchingSkills: skills.isFetching,
    };
}

export default connect(mapStateToProps, {
    fetchUser: fetchUserAction,
    fetchSkills: fetchSkillsAction,
    createAccount: createAccountAction,
    updateAccount: updateAccountAction,
    closeDialog: closeDialogAction,
})(withMobileDialog()(AccountDialog));
