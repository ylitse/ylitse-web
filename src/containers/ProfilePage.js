import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';

import { capitalize } from '../utils';
import { UserRoles } from '../constants';
import {
    fetchUser as fetchUserAction, editAccount as editAccountAction,
    deleteAccount as deleteAccountAction, setPassword as setPasswordAction,
    logout as logoutAction,
} from '../actions';
import ConfirmationDialog from '../components/ConfirmationDialog';
import DangerButton from '../components/DangerButton';
import Loading from '../components/Loading';
import Page from '../components/Page';

export class ProfilePage extends Component {
    static propTypes = {
        user: PropTypes.shape({
            id: PropTypes.string,
            username: PropTypes.string,
        }).isRequired,
        isFetching: PropTypes.bool.isRequired,
        fetchUser: PropTypes.func.isRequired,
        editAccount: PropTypes.func.isRequired,
        deleteAccount: PropTypes.func.isRequired,
        setPassword: PropTypes.func.isRequired,
        logout: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props);

        this.state = {
            deletingUser: false,
        };
    }

    componentDidMount() {
        const { fetchUser } = this.props;

        fetchUser();
    }

    setPassword = () => {
        const { user, setPassword } = this.props;

        setPassword(user);
    }

    editUser = () => {
        const { user, editAccount } = this.props;

        editAccount(user);
    }

    openConfirmation = () => {
        this.setState({ deletingUser: true });
    }

    closeConfirmation = () => {
        this.setState({ deletingUser: false });
    }

    deleteUser = () => {
        const { user, deleteAccount, logout } = this.props;

        deleteAccount(user);
        logout();
        this.closeConfirmation();
    }

    render() {
        const { user, isFetching } = this.props;
        const { deletingUser } = this.state;
        const header = 'User profile';
        const isMentor = user.role === UserRoles.MENTOR;

        if (isFetching) {
            return <Page header={header}><Loading /></Page>;
        }

        return (
            <Page header={header}>
                <div style={{ marginTop: 20 }}>
                    <Grid
                        container
                        justify="center"
                        style={{ background: '#f4f5f5', width: '100%', margin: 0, marginBottom: 20 }}
                    >
                        <Grid item>
                            <Avatar
                                alt={user.username}
                                src="/images/profile.png"
                                style={{ width: 160, height: 160, marginTop: 20, marginBottom: 20, background: 'white', border: '10px solid white' }}
                            />
                        </Grid>
                    </Grid>
                    <Grid container style={{ width: '100%', margin: 0, marginBottom: 20 }} justify="center" alignItems="flex-start" alignContent="flex-start">
                        <Grid item>
                            <Typography variant="h6" style={{ marginLeft: 16 }}>
                                Account
                            </Typography>
                            <List>
                                <ListItem>
                                    <Avatar>U</Avatar>
                                    <ListItemText primary={user.username} secondary="Username" />
                                </ListItem>
                                {user.email && (
                                    <ListItem>
                                        <Avatar>S</Avatar>
                                        <ListItemText primary={user.nickname} secondary="Screen name" />
                                    </ListItem>
                                )}
                                <ListItem>
                                    <Avatar>R</Avatar>
                                    <ListItemText primary={capitalize(user.role)} secondary="Role" />
                                </ListItem>
                                {user.email && (
                                    <ListItem>
                                        <Avatar>E</Avatar>
                                        <ListItemText primary={user.email} secondary="Email" />
                                    </ListItem>
                                )}
                            </List>
                        </Grid>
                        {isMentor && (
                            <Grid item>
                                <Typography variant="h6" style={{ marginLeft: 16 }}>
                                    Mentor profile
                                </Typography>
                                <List>
                                    {user.phone && (
                                        <ListItem>
                                            <Avatar>P</Avatar>
                                            <ListItemText primary={user.phone} secondary="Phone" />
                                        </ListItem>
                                    )}
                                    {user.gender && (
                                        <ListItem>
                                            <Avatar>G</Avatar>
                                            <ListItemText primary={capitalize(user.gender)} secondary="Gender" />
                                        </ListItem>
                                    )}
                                    {user.birthYear && (
                                        <ListItem>
                                            <Avatar>B</Avatar>
                                            <ListItemText primary={user.birthYear} secondary="Birth year" />
                                        </ListItem>
                                    )}
                                    {user.area && (
                                        <ListItem>
                                            <Avatar>A</Avatar>
                                            <ListItemText primary={capitalize(user.area)} secondary="Area" />
                                        </ListItem>
                                    )}
                                    {user.languages.length > 0 && (
                                        <ListItem>
                                            <Avatar>L</Avatar>
                                            <ListItemText primary={user.languages.join(', ')} secondary="Languages" />
                                        </ListItem>
                                    )}
                                    {user.skills.length > 0 && (
                                        <ListItem>
                                            <Avatar>S</Avatar>
                                            <ListItemText primary={user.skills.join(', ')} secondary="Skills" />
                                        </ListItem>
                                    )}
                                    {user.communicationChannels.length > 0 && (
                                        <ListItem>
                                            <Avatar>C</Avatar>
                                            <ListItemText primary={capitalize(user.communicationChannels.join(', '))} secondary="Communication channels" />
                                        </ListItem>
                                    )}
                                    {user.story && (
                                        <ListItem dense>
                                            <Avatar>S</Avatar>
                                            <ListItemText primary={user.story} secondary="Story" />
                                        </ListItem>
                                    )}
                                </List>
                            </Grid>
                        )}
                    </Grid>
                    <Button
                        variant="contained"
                        color="secondary"
                        onClick={this.editUser}
                    >
                        Update
                    </Button>
                    <Button
                        variant="contained"
                        color="secondary"
                        style={{ marginLeft: 16 }}
                        onClick={this.setPassword}
                    >
                        Set password
                    </Button>
                    {!isMentor && (
                        <DangerButton
                            style={{ marginLeft: 16 }}
                            onClick={this.openConfirmation}
                        >
                            Delete
                        </DangerButton>
                    )}
                </div>
                {user && (
                    <ConfirmationDialog
                        open={deletingUser}
                        label="Really delete your account?"
                        okLabel="Delete"
                        onOkClick={this.deleteUser}
                        onClose={this.closeConfirmation}
                    />
                )}
            </Page>
        );
    }
}

function mapStateToProps({ user }) {
    return {
        user,
        isFetching: user.isFetching,
    };
}

export default connect(mapStateToProps, {
    fetchUser: fetchUserAction,
    editAccount: editAccountAction,
    deleteAccount: deleteAccountAction,
    setPassword: setPasswordAction,
    logout: logoutAction,
})(ProfilePage);
