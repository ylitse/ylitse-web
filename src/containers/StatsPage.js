import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import { format } from 'date-fns';

import 'react-datepicker/dist/react-datepicker.css';

import Page from '../components/Page';

export class StatsPage extends Component {
    constructor(props) {
        super(props);

        const todayLocal = new Date();
        const firstOfLastMonth = new Date(todayLocal.getFullYear(), todayLocal.getMonth() - 1, 1);
        const lastOfLastMonth = new Date(todayLocal.getFullYear(), todayLocal.getMonth(), 0);

        this.state = {
            statPeriodStart: firstOfLastMonth,
            statPeriodEnd: lastOfLastMonth,
        };
    }

    setStartDate = (date) => {
        this.setState({
            statPeriodStart: date,
        });
    }

    setEndDate = (date) => {
        this.setState({
            statPeriodEnd: date,
        });
    }

    render() {
        const {
            statPeriodStart, statPeriodEnd,
        } = this.state;

        const statPeriodStartISO = format(statPeriodStart, 'yyyy-MM-dd');
        const statPeriodEndISO = format(statPeriodEnd, 'yyyy-MM-dd');

        const statPeriodLink = `/api/stats/period/${statPeriodStartISO}/${statPeriodEndISO}`;
        const statDailyLink = `/api/stats/period/${statPeriodStartISO}/${statPeriodEndISO}/daily`;
        const statWeeklyLink = `/api/stats/period/${statPeriodStartISO}/${statPeriodEndISO}/weekly`;
        const statMonthlyLink = `/api/stats/period/${statPeriodStartISO}/${statPeriodEndISO}/monthly`;
        const statPeriodMentorLink = `/api/stats/mentor/period/${statPeriodStartISO}/${statPeriodEndISO}`;

        return (
            <Page header="Stats">
                <legend>Start</legend>
                <DatePicker name="statPeriodStart" dateFormat="yyyy-MM-dd" selected={statPeriodStart} onChange={date => this.setStartDate(date)} />
                <br />
                <legend>End</legend>
                <DatePicker name="statPeriodEnd" dateFormat="yyyy-MM-dd" selected={statPeriodEnd} onChange={date => this.setEndDate(date)} />
                <br />
                <a
                    href={statPeriodLink}
                    target="_blank"
                    rel="noopener noreferrer"
                >Period
                </a>
                <br />
                <br />
                <a
                    href={statDailyLink}
                    target="_blank"
                    rel="noopener noreferrer"
                >Daily
                </a>
                <br />
                <br />
                <a
                    href={statWeeklyLink}
                    target="_blank"
                    rel="noopener noreferrer"
                >Weekly
                </a>
                <br />
                <br />
                <a
                    href={statMonthlyLink}
                    target="_blank"
                    rel="noopener noreferrer"
                >Monthly
                </a>
                <br />
                <br />
                <a
                    href={statPeriodMentorLink}
                    target="_blank"
                    rel="noopener noreferrer"
                >Mentors
                </a>
                <br />
                <br />
            </Page>
        );
    }
}

export default StatsPage;
