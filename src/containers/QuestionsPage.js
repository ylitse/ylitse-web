import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Loading from '../components/Loading';
import {
    fetchQuestions as fetchQuestionsAction,
    setQuestion as setQuestionsAction,
    createQuestion as createQuestionAction,
    deleteQuestion as deleteQuestionAction,
} from '../actions';
import Page from '../components/Page';
import QuestionList from '../components/QuestionList';
import QuestionForm from '../components/QuestionForm';
import { question } from '../questionType';
import ConfirmationDialog from '../components/ConfirmationDialog';

export class QuestionsPage extends Component {
  static propTypes = {
      questions: PropTypes.arrayOf(question).isRequired,
      isFetchingQuestions: PropTypes.bool.isRequired,
      setQuestion: PropTypes.func.isRequired,
      fetchQuestions: PropTypes.func.isRequired,
      createQuestion: PropTypes.func.isRequired,
      deleteQuestion: PropTypes.func.isRequired,
      questionEdit: PropTypes.object.isRequired,
  };

  constructor(props) {
      super(props);

      this.state = {
          isQuestionFormOpen: false,
          deleteConfirmation: { isOpen: false },
      };
  }

  componentDidMount() {
      const { fetchQuestions } = this.props;

      fetchQuestions();
  }

  closeQuestionForm = () => {
      this.setState({ isQuestionFormOpen: false });
  };

  openQuestionForm = (q) => {
      const { setQuestion } = this.props;
      setQuestion(q);
      this.setState({ isQuestionFormOpen: true });
  };

  handleQuestionCreate = () => {
      const { createQuestion, questionEdit } = this.props;
      createQuestion(questionEdit);
      this.setState({ isQuestionFormOpen: false });
  };

  handleQuestionDelete = () => {
      const { deleteQuestion } = this.props;
      const {
          deleteConfirmation: { toDelete },
      } = this.state;

      deleteQuestion(toDelete);
      this.setState({
          isQuestionFormOpen: false,
          deleteConfirmation: { isOpen: false },
      });
  };

  handleDeleteConfirmationOpen = (id) => {
      this.setState({ deleteConfirmation: { isOpen: true, toDelete: id } });
  };

  handleDeleteCancel = () => {
      this.setState({ deleteConfirmation: { isOpen: false } });
  };

  render() {
      const { questions, isFetchingQuestions } = this.props;
      const { isQuestionFormOpen, deleteConfirmation } = this.state;

      return (
          <Page header="Questions">
              {isFetchingQuestions ? (
                  <Loading />
        ) : (
            <QuestionList
                questions={questions}
                onQuestionClick={this.openQuestionForm}
            />
        )}
              <QuestionForm
                  isOpen={isQuestionFormOpen}
                  onClose={this.closeQuestionForm}
                  onSave={this.handleQuestionCreate}
                  onDelete={this.handleDeleteConfirmationOpen}
              />
              <ConfirmationDialog
                  open={deleteConfirmation.isOpen}
                  label="Are you sure you want to delete the question?"
                  okLabel="Confirm delete"
                  onOkClick={this.handleQuestionDelete}
                  onClose={this.handleDeleteCancel}
              />
          </Page>
      );
  }
}

function mapStateToProps({ questions }) {
    return {
        questions: questions.questions,
        isFetchingQuestions: questions.isFetching,
        questionEdit: questions.questionEdit,
    };
}

export default connect(mapStateToProps, {
    fetchQuestions: fetchQuestionsAction,
    setQuestion: setQuestionsAction,
    createQuestion: createQuestionAction,
    deleteQuestion: deleteQuestionAction,
})(QuestionsPage);
