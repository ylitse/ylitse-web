import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
    fetchLogs as fetchLogsAction,
} from '../actions';
import Loading from '../components/Loading';
import Page from '../components/Page';
import LogsListDataGrid from '../components/LogsListDataGrid';

const LogsPage = ({
    logs,
    isFetchingLogs,
    fetchLogs,
}) => {
    useEffect(() => {
        fetchLogs();
    }, [fetchLogs]);

    return (
        <Page header="Logs">
            {isFetchingLogs ? (
                <Loading />
      ) : (
          <LogsListDataGrid logs={logs} />
      )}
        </Page>
    );
};

LogsPage.propTypes = {
    logs: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string,
            chat_user_id_1: PropTypes.string,
            chat_user_id_2: PropTypes.string,
            reviewer_user_id: PropTypes.string,
            created: PropTypes.datetime,
        }),
    ).isRequired,
    isFetchingLogs: PropTypes.bool.isRequired,
    fetchLogs: PropTypes.func.isRequired,
};

const mapStateToProps = ({ logs }) => ({
    logs: logs.logs,
    isFetchingLogs: logs.isFetching,
});

export default connect(mapStateToProps, {
    fetchLogs: fetchLogsAction,
})(LogsPage);
