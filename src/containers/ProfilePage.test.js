import React from 'react';
import { render, screen } from '@testing-library/react';

import { ProfilePage } from './ProfilePage';

describe('ProfilePage', () => {
    const props = {
        user: { id: '0', username: 'test', role: 'admin' },
        isFetching: false,
        fetchUser: () => {},
        editAccount: () => {},
        deleteAccount: () => {},
        setPassword: () => {},
        logout: () => {},
    };

    test('renders avatar with username', () => {
        render(<ProfilePage {...props} />);
        const avatar = screen.getByRole('img', { name: 'test' });
        expect(avatar).toBeInTheDocument();
    });

    test('renders heading user profile', () => {
        render(<ProfilePage {...props} />);
        const heading = screen.getByRole('heading', { name: /user profile/i });
        expect(heading).toBeInTheDocument();
    });

    test('renders 3 buttons', () => {
        render(<ProfilePage {...props} />);
        const buttons = screen.getAllByRole('button');
        expect(buttons.length).toBe(3);
    });
});
