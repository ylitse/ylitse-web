import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormGroup from '@material-ui/core/FormGroup';

import {
    updateAccount as updateAccountAction, closeDialog as closeDialogAction,
} from '../actions';
import PasswordField from '../components/PasswordField';

export class PasswordDialog extends Component {
    static propTypes = {
        open: PropTypes.bool.isRequired,
        account: PropTypes.shape({
            username: PropTypes.string,
        }).isRequired,
        updateAccount: PropTypes.func.isRequired,
        closeDialog: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props);

        const { account } = this.props;

        this.state = {
            account,
            errors: {
                password: undefined,
                password2: undefined,
            },
            formValid: false,
        };
    }

    updateValue = ({ target }) => {
        const { name, value } = target;
        const { errors } = this.state;

        if (name === 'password') {
            const valid = value.length > 6;
            errors.password = valid ? '' : 'Password is too short';
        }

        this.setState((prevState) => {
            const { password, password2 } = prevState.account;

            if (name === 'password' && password2 === value
                || name === 'password2' && password === value) {
                errors.password2 = '';
            } else {
                errors.password2 = 'Passwords don\'t match';
            }

            return {
                account: {
                    ...prevState.account,
                    [target.name]: value,
                },
                errors,
                formValid: Object.values(errors).every(e => e === ''),
            };
        });
    }

    sendPassword = () => {
        const { updateAccount, closeDialog } = this.props;
        const { account } = this.state;

        delete account.password2;

        updateAccount(account);
        closeDialog();
    }

    render() {
        const { open, closeDialog } = this.props;
        const { account, formValid, errors } = this.state;

        return (
            <Dialog open={open} onClose={closeDialog}>
                <DialogTitle>Set password for {account.username}</DialogTitle>
                <DialogContent>
                    <FormGroup>
                        <PasswordField
                            id="password"
                            name="password"
                            label="Password"
                            error={Boolean(errors.password)}
                            helperText={errors.password}
                            required
                            style={{ marginBottom: 8 }}
                            onChange={this.updateValue}
                        />
                        <PasswordField
                            id="password2"
                            name="password2"
                            label="Confirm password"
                            error={Boolean(errors.password2)}
                            helperText={errors.password2}
                            required
                            style={{ marginBottom: 8 }}
                            onChange={this.updateValue}
                        />
                    </FormGroup>
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeDialog}>Cancel</Button>
                    <Button
                        color="secondary"
                        disabled={!formValid}
                        onClick={this.sendPassword}
                    >
                        Set
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

export default connect(null, {
    updateAccount: updateAccountAction,
    closeDialog: closeDialogAction,
})(PasswordDialog);
