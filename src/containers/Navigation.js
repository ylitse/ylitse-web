import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { version as uiVersion } from '../../package.json';
import { UserRoles } from '../constants';
import {
    fetchVersion as fetchVersionAction,
    logout as logoutAction,
} from '../actions';
import TopBar from '../components/TopBar';

const adminToolbarLinks = [
    {
        label: 'Accounts',
        to: '/accounts',
    },
    {
        label: 'Skills',
        to: '/skills',
    },
    {
        label: 'Stats',
        to: '/stats',
    },
    {
        label: 'Questions',
        to: '/questions',
    },
    {
        label: 'Answers',
        to: '/answers',
    },
    {
        label: 'Reports',
        to: '/reports',
    },
    {
        label: 'Logs',
        to: '/logs',
    },
];

export class Navigation extends Component {
  static propTypes = {
      apiVersion: PropTypes.string.isRequired,
      username: PropTypes.string.isRequired,
      role: PropTypes.string.isRequired,
      fetchVersion: PropTypes.func.isRequired,
      logout: PropTypes.func.isRequired,
  };

  componentDidMount() {
      const { fetchVersion } = this.props;

      fetchVersion();
  }

  render() {
      const { apiVersion, username, role, logout } = this.props;
      const toolbarLinks = role === UserRoles.ADMIN ? adminToolbarLinks : [];
      let fullUiVersion = uiVersion;

      if (process.env.REV) {
          fullUiVersion = `${uiVersion}:${process.env.REV}`;
      }

      return (
          <TopBar
              toolbarLinks={toolbarLinks}
              username={username}
              version={{ ui: fullUiVersion, api: apiVersion || 'N/A' }}
              onLogoutClick={logout}
          />
      );
  }
}

function mapStateToProps({ version, user }) {
    return {
        apiVersion: version.api,
        role: user.role,
        username: user.username,
    };
}

export default connect(mapStateToProps, {
    fetchVersion: fetchVersionAction,
    logout: logoutAction,
})(Navigation);
