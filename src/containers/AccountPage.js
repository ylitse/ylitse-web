import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {
    fetchAccounts as fetchAccountsAction, addAccount as addAccountAction,
    editAccount as editAccountAction, deleteAccount as deleteAccountAction,
    setPassword as setPasswordAction, fetchStats as fetchStatsAction,
} from '../actions';
import Loading from '../components/Loading';
import Page from '../components/Page';
import AccountToolbar from '../components/AccountToolbar';
import AccountList from '../components/AccountList';
import ConfirmationDialog from '../components/ConfirmationDialog';
import MentorConversationDialog from '../components/MentorConversationDialog';

export class AccountPage extends Component {
    static propTypes = {
        accounts: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.string,
            username: PropTypes.string,
        })).isRequired,
        isFetchingAccounts: PropTypes.bool.isRequired,
        fetchAccounts: PropTypes.func.isRequired,
        addAccount: PropTypes.func.isRequired,
        editAccount: PropTypes.func.isRequired,
        deleteAccount: PropTypes.func.isRequired,
        setPassword: PropTypes.func.isRequired,
        stats: PropTypes.objectOf(
            PropTypes.shape({
                sent: PropTypes.number.isRequired,
                received: PropTypes.number.isRequired,
            }).isRequired,
        ).isRequired,
        totals: PropTypes.shape({
            admins: PropTypes.object,
            mentors: PropTypes.object,
            mentees: PropTypes.object,
            accounts: PropTypes.shape({
                mentees: PropTypes.number,
                mentors: PropTypes.number,
                isVacationing: PropTypes.number,
            }),
        }).isRequired,
        isFetchingStats: PropTypes.bool.isRequired,
        fetchStats: PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props);

        this.state = {
            selectedAccount: undefined,
            deletingAccount: false,
            showConversations: false,
            account: undefined,
        };
    }

    componentDidMount() {
        const { fetchAccounts, fetchStats } = this.props;

        fetchAccounts();
        fetchStats();
    }

    setPassword = account => () => {
        const { setPassword } = this.props;

        setPassword(account);
    }

    editAccount = account => () => {
        const { editAccount } = this.props;

        editAccount(account);
    }

    openConfirmation = selectedAccount => () => {
        this.setState({ selectedAccount,
            deletingAccount: true,
            showConversations: this.showConversations,
            account: this.account });
    }

    closeConfirmation = () => {
        this.setState({ selectedAccount: undefined,
            deletingAccount: false,
            showConversations: this.showConversations,
            account: this.account });
    }

    deleteAccount = account => () => {
        const { deleteAccount } = this.props;

        deleteAccount(account);
        this.closeConfirmation();
    }

    openConversation = account => () => {
        this.setState({ selectedAccount: this.selectedAccount,
            deletingAccount: this.deletingAccount,
            showConversations: true,
            account });
    }

 closeConversations = () => {
     this.setState({ selectedAccount: this.selectedAccount,
         deletingAccount: this.deletingAccount,
         showConversations: false,
         account: undefined });
 }

    onlyMentees = accounts => accounts.filter(account => account.role === 'mentee')

    onlyMentors = accounts => accounts.filter(account => account.role === 'mentor')

    onlyVacationingMentors = accounts => accounts.filter(account => account.role === 'mentor' && account.isVacationing)

    render() {
        const {
            accounts, isFetchingAccounts, addAccount, stats, totals,
            isFetchingStats,
        } = this.props;
        const { selectedAccount, deletingAccount, showConversations, account } = this.state;
        const accountName = selectedAccount
              && `${selectedAccount.username}'s account`;

        totals.accounts = {
            mentees: this.onlyMentees(accounts).length,
            mentors: this.onlyMentors(accounts).length,
            isVacationing: this.onlyVacationingMentors(accounts).length,
        };

        return (
            <Page header="User accounts">
                <AccountToolbar onAddClick={addAccount} />
                {isFetchingAccounts || isFetchingStats ? (
                    <Loading />
                ) : (
                    <AccountList
                        accounts={accounts}
                        stats={stats}
                        totals={totals}
                        onEdit={this.editAccount}
                        onDelete={this.openConfirmation}
                        onPassChange={this.setPassword}
                        openConversation={this.openConversation}
                    />
                )}
                {selectedAccount && (
                    <ConfirmationDialog
                        open={deletingAccount}
                        label={`Really delete ${accountName}?`}
                        okLabel="Delete"
                        onOkClick={this.deleteAccount(selectedAccount)}
                        onClose={this.closeConfirmation}
                    />
                )}
                {
                    showConversations && (
                        <MentorConversationDialog
                            account={account}
                            onClose={this.closeConversations}
                        />
                    )
                }
            </Page>
        );
    }
}

function mapStateToProps({ accounts, stats }) {
    return {
        accounts: accounts.items,
        isFetchingAccounts: accounts.isFetching,
        stats: stats.stats,
        totals: stats.totals,
        isFetchingStats: stats.isFetching,
    };
}

export default connect(mapStateToProps, {
    fetchAccounts: fetchAccountsAction,
    addAccount: addAccountAction,
    editAccount: editAccountAction,
    deleteAccount: deleteAccountAction,
    setPassword: setPasswordAction,
    fetchStats: fetchStatsAction,
})(AccountPage);
