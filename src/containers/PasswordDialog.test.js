import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { PasswordDialog } from './PasswordDialog';

describe('PasswordDialog', () => {
    const props = {
        open: true,
        account: {
            username: 'Test',
        },
        updateAccount: jest.fn(),
        closeDialog: jest.fn(),
    };
    test('renders heading correctly', () => {
        render(<PasswordDialog {...props} />);
        expect(screen.getByText(/set password for/i)).toHaveTextContent(/test/i);
    });

    test('renders two passwordfields', () => {
        render(<PasswordDialog {...props} />);
        const passwordFields = screen.getAllByLabelText(/password/i, { name: /password/i });
        expect(passwordFields.length).toBe(2);
    });

    test('confirm button is disabled if passwords dont match', () => {
        render(<PasswordDialog {...props} />);
        const passwordFields = screen.getAllByLabelText(/password/i, { name: /password/i });
        userEvent.type(passwordFields[0], 'newpassword');
        const button = screen.getByRole('button', { name: /set/i });
        expect(button).toBeDisabled();
    });

    test('confirm button is enabled if passwords match', () => {
        render(<PasswordDialog {...props} />);
        const passwordFields = screen.getAllByLabelText(/password/i, { name: /password/i });
        userEvent.type(passwordFields[0], 'newpassword');
        userEvent.type(passwordFields[1], 'newpassword');
        const button = screen.getByRole('button', { name: /set/i });
        expect(button).not.toBeDisabled();
    });

    test('cancel button callback works', () => {
        render(<PasswordDialog {...props} />);
        const button = screen.getByRole('button', { name: /cancel/i });
        userEvent.click(button);
        expect(props.closeDialog).toBeCalled();
    });

    test('confirm button closes dialog', () => {
        render(<PasswordDialog {...props} />);
        const button = screen.getByRole('button', { name: /set/i });
        userEvent.click(button);
        expect(props.closeDialog).toBeCalled();
    });
});
