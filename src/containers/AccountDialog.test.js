import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { AccountDialog } from './AccountDialog';

jest.useFakeTimers();

describe('AccountDialog', () => {
    const props = {
        open: true,
        fullScreen: false,
        currentUserId: '',
        availableSkills: [{ id: '0', name: 'Test skill' }],
        isFetchingSkills: false,
        fetchUser: () => {},
        fetchSkills: () => {},
        createAccount: jest.fn(),
        updateAccount: jest.fn(),
        closeDialog: () => {},
    };

    const account = {
        role: 'mentor',
        username: 'testmentor',
        password: 'testpassword',
        nickname: 'testnickname',
        id: 'testid',
        email: 'mentor@mentor.mentor',
        gender: 'male',
        birthYear: '1991',
        skills: [],
        languages: [],
    };

    test('renders loading when fetching skills', () => {
        render(<AccountDialog {...props} isFetchingSkills />);
        jest.runAllTimers();
        const loading = screen.getByRole('progressbar');
        expect(loading).toBeInTheDocument();
        jest.clearAllTimers();
    });

    test('renders empty form if not loading skills and no account', () => {
        render(<AccountDialog {...props} />);
        expect(screen.getByText(/Create new/i)).toBeInTheDocument();
        expect(screen.getAllByRole('button').length).toBe(3);
        expect(screen.getAllByRole('tab').length).toBe(2);
        expect(screen.getAllByRole('radio').length).toBe(3);
        expect(screen.getAllByRole('textbox').length).toBe(3);
    });

    test('typing to email-field updates email', () => {
        render(<AccountDialog {...props} />);
        const emailField = screen.getByRole('textbox', { name: /email/i });
        userEvent.type(emailField, 'mentor@mentor.fi');
        expect(emailField).toHaveValue('mentor@mentor.fi');
    });

    test('mentor is selected role by default', () => {
        render(<AccountDialog {...props} />);
        const mentorRadio = screen.getByRole('radio', { name: /mentor/i });
        expect(mentorRadio).toBeChecked();
    });

    test('switching role updates selected', () => {
        render(<AccountDialog {...props} />);
        const menteeRadio = screen.getByRole('radio', { name: /mentee/i });
        userEvent.click(menteeRadio);
        expect(menteeRadio).toBeChecked();
        const mentorRadio = screen.getByRole('radio', { name: /mentor/i });
        expect(mentorRadio).not.toBeChecked();
    });

    test('if role \'mentor\' is selected, pushing mentor tab opens mentor-profile form', () => {
        render(<AccountDialog {...props} />);
        const mentorFormTab = screen.getByRole('tab', { name: /mentor profile/i });
        userEvent.click(mentorFormTab);
        expect(screen.getAllByRole('textbox').length).toBe(6);
        expect(screen.getByRole('group', { name: /gender/i })).toBeInTheDocument();
        expect(screen.getByRole('group', { name: /communication channels/i })).toBeInTheDocument();
    });

    test('if role \'mentor\' NOT selected, mentor tab is disabled', () => {
        render(<AccountDialog {...props} />);
        const menteeRadio = screen.getByRole('radio', { name: /mentee/i });
        userEvent.click(menteeRadio);
        const mentorFormTab = screen.getByRole('tab', { name: /mentor profile/i });
        expect(mentorFormTab).toBeDisabled();
    });

    test('when creating mentor, create-button is disabled when data is missing', () => {
        render(<AccountDialog {...props} />);
        const createButton = screen.getByRole('button', { name: /create/i });
        expect(createButton).toBeDisabled();
    });

    test('when creating mentor, create-button is disabled if birthYear missing', () => {
        render(<AccountDialog {...props} />);
        userEvent.type(screen.getByRole('textbox', { name: /username/i }), 'mentee');
        userEvent.type(screen.getByRole('textbox', { name: /email/i }), 'mentor@mentor.fi');
        userEvent.type(screen.getByLabelText(/password/i, { name: /password/i }), 'secretpassword');
        userEvent.type(screen.getByRole('textbox', { name: /screen name/i }), 'lil M');
        const createButton = screen.getByRole('button', { name: /create/i });
        expect(createButton).toBeDisabled();
    });

    test('when creating mentor, create-button enabled and called when pushed', () => {
        render(<AccountDialog {...props} />);
        userEvent.type(screen.getByRole('textbox', { name: /username/i }), 'mentee');
        userEvent.type(screen.getByRole('textbox', { name: /email/i }), 'mentor@mentor.fi');
        userEvent.type(screen.getByLabelText(/password/i, { name: /password/i }), 'secretpassword');
        userEvent.type(screen.getByRole('textbox', { name: /screen name/i }), 'lil M');
        userEvent.click(screen.getByRole('tab', { name: /mentor profile/i }));
        userEvent.type(screen.getByRole('textbox', { name: /birth year/i }), '1991');
        const createButton = screen.getByRole('button', { name: /create/i });
        expect(createButton).not.toBeDisabled();
        userEvent.click(createButton);
        expect(props.createAccount).toBeCalled();
    });

    test('passing account in props populates fields in form', () => {
        render(<AccountDialog {...props} account={account} availableItems={[]} availableLanguages={[]} />);
        expect(screen.getByRole('textbox', { name: /username/i })).toHaveValue(account.username);
        expect(screen.getByRole('textbox', { name: /email/i })).toHaveValue(account.email);
        expect(screen.getByRole('textbox', { name: /screen name/i })).toHaveValue(account.nickname);
        userEvent.click(screen.getByRole('tab', { name: /mentor profile/i }));
        expect(screen.getByRole('textbox', { name: /birth year/i })).toHaveValue(account.birthYear);
    });

    test('when updating mentor, save-button is enabled when all required data is filled', () => {
        render(<AccountDialog {...props} account={account} />);
        const createButton = screen.getByRole('button', { name: /save/i });
        expect(createButton).not.toBeDisabled();
    });

    test('when updating mentor, save-button is disabled when birthYear is missing', () => {
        const updatedAccount = {
            ...account,
            birthYear: '',
        };
        render(<AccountDialog {...props} account={updatedAccount} />);
        const createButton = screen.getByRole('button', { name: /save/i });
        expect(createButton).toBeDisabled();
    });

    test('when updating mentor, save-button enabled and called when pushed', () => {
        render(<AccountDialog {...props} account={account} />);
        const saveButton = screen.getByRole('button', { name: /save/i });
        expect(saveButton).not.toBeDisabled();
        userEvent.click(saveButton);
        expect(props.updateAccount).toBeCalled();
    });
});

