import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import { Button } from '@material-ui/core';
import { format } from 'date-fns';
import Page from '../components/Page';

const getTommorrow = date => date.setDate(date.getDate() + 1);

const Answers = () => {
    // Default start-date => Date 1970-01-01T00:00:00.000Z
    // Default end-date => Tommorrow
    const [dates, setDates] = useState({
        start: new Date(0),
        end: getTommorrow(new Date()),
    });

    const handleDateChange = (date, key) => {
        setDates({ ...dates, [key]: date });
    };

    const formatDate = date => format(date, 'yyyy-MM-dd');

    return (
        <Page header="Answers">
            Start
            <DatePicker
                name="statPeriodStart"
                dateFormat="yyyy-MM-dd"
                selected={dates.start}
                onChange={date => handleDateChange(date, 'start')}
            />
            End
            <DatePicker
                name="statPeriodEnd"
                dateFormat="yyyy-MM-dd"
                selected={dates.end}
                onChange={date => handleDateChange(date, 'end')}
            />
            <Button color="secondary">
                <a
                    href={`/api/feedback/answers?start=${formatDate(
                        dates.start,
                    )}&end=${formatDate(dates.end)}`}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Answers
                </a>
            </Button>
        </Page>
    );
};

export default Answers;
