import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Snackbar from '@material-ui/core/Snackbar';

import { dequeueFeedback as dequeueFeedbackAction } from '../actions';

class Feedback extends Component {
    static propTypes = {
        critical: PropTypes.bool,
        message: PropTypes.string.isRequired,
        multiple: PropTypes.bool.isRequired,
        dequeueFeedback: PropTypes.func.isRequired,
    }

    static defaultProps = {
        critical: false,
    }

    constructor(props) {
        super(props);

        this.message = '';
    }

    render() {
        const { critical, message, multiple, dequeueFeedback } = this.props;

        if (critical) {
            return (
                <Dialog open={Boolean(message)}>
                    <DialogTitle>Error</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {message}
                        </DialogContentText>
                    </DialogContent>
                </Dialog>
            );
        }

        if (message) {
            // Copy message to be shown during closing animation
            // when original message is cleared
            this.message = message;
        }

        return (
            <Snackbar
                key={message}
                open={Boolean(message)}
                message={this.message}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                autoHideDuration={2000}
                action={(
                    <Button
                        color="secondary"
                        size="small"
                        onClick={dequeueFeedback}
                    >
                        {multiple ? 'Next' : 'Close'}
                    </Button>
                )}
                onClose={dequeueFeedback}
            />
        );
    }
}

function mapStateToProps({ feedback }) {
    return {
        message: feedback.current,
        multiple: feedback.messages.length > 0,
    };
}

export default connect(mapStateToProps, {
    dequeueFeedback: dequeueFeedbackAction,
})(Feedback);
