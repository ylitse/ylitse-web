import React from 'react';
import { render, screen } from '@testing-library/react';

import { MemoryRouter } from 'react-router-dom';
import { Navigation } from './Navigation';

describe('Navigation', () => {
    const props = {
        apiVersion: '0.1',
        username: 'test',
        role: 'test',
        fetchVersion: () => {},
        logout: () => {},
    };

    test('renders topbar', () => {
        render(<MemoryRouter><Navigation {...props} /></MemoryRouter>);
        const topBar = screen.getByRole('banner');
        expect(topBar).toBeInTheDocument();
    });
});
