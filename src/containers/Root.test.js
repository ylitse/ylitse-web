import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';

import { UserRoles } from '../constants';
import { Root } from './Root';

jest.mock('react-router-dom/BrowserRouter', () => (
    jest.fn(({ children }) => <div>{children}</div>)
));

describe('Root', () => {
    const mockStore = configureStore([thunk]);
    const state = {
        user: { role: UserRoles.ADMIN, username: 'test' },
        accounts: { items: [], isFetching: false },
        stats: { stats: {}, totals: {}, isFetching: false },
        skills: { items: [], isFetching: false },
        feedback: { current: '', messages: [] },
        dialog: { type: '', props: {} },
        version: { api: '' },
    };
    const props = { ...state.user, fetchUser: () => {} };

    test('renders default route correctly (accountpage)', () => {
        render(
            <Provider store={mockStore(state)}>
                <MemoryRouter initialEntries={['/']}>
                    <Root {...props} />
                </MemoryRouter>
            </Provider>,
        );
        expect(screen.getByText(/no user accounts/i)).toBeInTheDocument();
    });

    test('renders route to skillspage', () => {
        render(
            <Provider store={mockStore(state)}>
                <MemoryRouter
                    initialEntries={['/', '/skills']}
                    initialIndex={1}
                >
                    <Root {...props} />
                </MemoryRouter>
            </Provider>,
        );
        expect(screen.getByText('Mentor skills')).toBeInTheDocument();
    });

    test('renders route missing page', () => {
        render(
            <Provider store={mockStore(state)}>
                <MemoryRouter
                    initialEntries={['/', '/skills', '/invalid']}
                    initialIndex={2}
                >
                    <Root {...props} />
                </MemoryRouter>
            </Provider>,
        );
        expect(screen.getByText(/could not find/i)).toBeInTheDocument();
    });

    test('renders nothing if user is missing', () => {
        const noUser = {
            username: '',
            role: '',
            fetchUser: () => {},
        };
        const { container } = render(
            <Provider store={mockStore(state)}>
                <MemoryRouter initialEntries={['/']}>
                    <Root {...noUser} />
                </MemoryRouter>
            </Provider>,
        );
        expect(container.firstChild).toBe(null);
    });
});
