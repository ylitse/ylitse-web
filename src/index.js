import React from 'react';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { render } from 'react-dom';
import { MuiThemeProvider } from '@material-ui/core/styles';
import 'whatwg-fetch';
import 'typeface-roboto';

import theme from './theme';
import rootReducer from './reducers';
import RootContainer from './containers/Root';

const middleware = [thunk];

if (process.env.NODE_ENV !== 'production') {
    middleware.push(createLogger());
}

/* eslint-disable no-underscore-dangle */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
/* eslint-enable */

const store = createStore(
    rootReducer,
    composeEnhancers(
        applyMiddleware(...middleware),
    ),
);

if (module.hot) {
    module.hot.accept('./reducers', () => {
        store.replaceReducer(rootReducer);
    });
}

const root = (
    <Provider store={store}>
        <MuiThemeProvider theme={theme}>
            <RootContainer />
        </MuiThemeProvider>
    </Provider>
);

render(root, document.getElementById('root'));
