import React from 'react';
import { render, screen } from '@testing-library/react';

import AccountForm from './AccountForm';

describe('AccountForm', () => {
    const props = {
        account: {
            role: 'mentee',
            password: '123',
        },
        errors: {
            password: 'Password is too short',
        },
        classes: {},
        isNew: true,
        onValueChange: () => {},
    };

    test('renders all role-options', () => {
        render(<AccountForm {...props} />);

        const radioGroupControl = screen.getAllByRole('radio');
        expect(radioGroupControl.length).toEqual(3);
    });

    test('renders *password too short error*', () => {
        render(<AccountForm {...props} />);

        const passwordError = screen.getByText('Password is too short');
        expect(passwordError).toBeInTheDocument();
    });

    test('does not render role-options if user is old', () => {
        render(<AccountForm {...props} isNew={false} />);

        const radioGroupControl = screen.queryAllByRole('radio');
        expect(radioGroupControl).toEqual([]);
    });

    test('username (login-name) textfield is NOT disabled when new account', () => {
        render(<AccountForm {...props} />);

        const usernameInput = screen.getByRole('textbox', { name: /username/i });
        expect(usernameInput).not.toBeDisabled();
    });

    test('username (login-name) textfield is disabled when account exists', () => {
        render(<AccountForm {...props} isNew={false} />);

        const usernameInput = screen.getByRole('textbox', { name: /username/i });
        expect(usernameInput).toBeDisabled();
    });
});
