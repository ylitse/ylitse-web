import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from '@material-ui/core/Modal';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
    inline: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '5%',
        marginBottom: '5%',
    },
    fullscreen: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50px, -50px)',
        outline: '0',
    },
});

class Loading extends Component {
    static propTypes = {
        fullscreen: PropTypes.bool,
        classes: PropTypes.shape({
            outer: PropTypes.string,
            progress: PropTypes.string,
        }).isRequired,
    }

    static defaultProps = {
        fullscreen: false,
    }

    constructor(props) {
        super(props);

        this.state = {
            visible: false,
        };
    }

    componentDidMount() {
        this.timer = setTimeout(() => {
            this.setState({ visible: true });
        }, 200);
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
    }

    render() {
        const { fullscreen, classes } = this.props;
        const { visible } = this.state;
        const spinnerProps = {
            color: 'secondary',
            size: fullscreen ? 100 : 50,
            thickness: fullscreen ? 4 : 3.6,
        };

        if (!visible) {
            return null;
        }

        if (fullscreen) {
            return (
                <Modal open>
                    <div className={classes.fullscreen}>
                        <CircularProgress {...spinnerProps} />
                    </div>
                </Modal>
            );
        }

        return (
            <div className={classes.inline}>
                <CircularProgress {...spinnerProps} />
            </div>
        );
    }
}

export default withStyles(styles)(Loading);
