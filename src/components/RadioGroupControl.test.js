import React from 'react';
import { render, screen } from '@testing-library/react';

import RadioGroupControl from './RadioGroupControl';

describe('RadioGroupControl', () => {
    test('both options are mapped', () => {
        const props = {
            name: 'test',
            label: 'Test label',
            value: 'One',
            options: ['One', 'Two'],
            onChange: () => {},
        };
        render(<RadioGroupControl {...props} />);
        const options = screen.getAllByRole('radio');
        expect(options.length).toBe(2);
    });
});
