import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import DangerButton from './DangerButton';

const ConfirmationDialog = ({
    label, okLabel, onOkClick, onClose, ...props
}) => (
    <Dialog onClose={onClose} {...props}>
        <DialogTitle>Confirm</DialogTitle>
        <DialogContent>
            <DialogContentText>
                {label}
            </DialogContentText>
        </DialogContent>
        <DialogActions>
            <Button onClick={onClose}>
                Cancel
            </Button>
            <DangerButton onClick={onOkClick}>
                {okLabel}
            </DangerButton>
        </DialogActions>
    </Dialog>
);

ConfirmationDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    label: PropTypes.string.isRequired,
    okLabel: PropTypes.string,
    onOkClick: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
};

ConfirmationDialog.defaultProps = {
    okLabel: 'Ok',
};

export default ConfirmationDialog;
