import React from 'react';
import PropTypes from 'prop-types';
import FormGroup from '@material-ui/core/FormGroup';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';

import { UserRoles } from '../constants';
import PasswordField from './PasswordField';
import RadioGroupControl from './RadioGroupControl';

const styles = theme => ({
    row: {
        marginBottom: theme.spacing(1),
    },
    radioRow: {
        marginTop: theme.spacing(2),
        marginBottom: 0,
    },
    checkboxRow: {
        marginTop: theme.spacing(2),
        marginBottom: 0,
    },
    formControl: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    button: {
        marginTop: theme.spacing(2),
    },
    buttonIcon: {
        marginLeft: theme.spacing(1),
    },
});

const renderTextField = (name, label, required, account, other, disabled) => (
    <TextField
        name={name}
        label={label}
        id={name}
        value={account[name]}
        error={Boolean(other.errors[name])}
        helperText={other.errors[name]}
        required={required}
        className={other.classes.row}
        onChange={other.onValueChange}
        disabled={disabled}
    />
);

const AccountForm = ({ account, isNew, ...other }) => (
    <form autoComplete="off">
        <FormGroup>
            {isNew && (
                <RadioGroupControl
                    name="role"
                    label="Account type"
                    value={account.role}
                    options={Object.values(UserRoles)}
                    className={other.classes.radioRow}
                    onChange={other.onValueChange}
                />
            )}
            {renderTextField('username', 'Username', true, account, other, !isNew)}
            {isNew && (
                <PasswordField
                    name="password"
                    label="Password"
                    value={account.password}
                    error={Boolean(other.errors.password)}
                    helperText={other.errors.password}
                    required
                    className={other.classes.row}
                    onChange={other.onValueChange}
                />
            )}
            {renderTextField('nickname', 'Screen name', false, account, other)}
            {renderTextField('email', 'Email', false, account, other)}
        </FormGroup>
    </form>
);

AccountForm.propTypes = {
    account: PropTypes.shape({
        role: PropTypes.string,
        password: PropTypes.string,
    }).isRequired,
    isNew: PropTypes.bool,
    errors: PropTypes.shape({
        password: PropTypes.string,
    }).isRequired,
    onValueChange: PropTypes.func.isRequired,
    classes: PropTypes.shape({
        row: PropTypes.string,
        radioRow: PropTypes.string,
        checkboxRow: PropTypes.string,
        formControl: PropTypes.string,
        button: PropTypes.string,
        buttonIcon: PropTypes.string,
    }).isRequired,
};

AccountForm.defaultProps = {
    isNew: false,
};

export default withStyles(styles)(AccountForm);
