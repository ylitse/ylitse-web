import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import ConfirmationDialog from './ConfirmationDialog';

describe('ConfirmationDialog', () => {
    const onOkClick = jest.fn();
    const onClose = jest.fn();
    const props = {
        onClose,
        open: true,
        label: 'ConfirmationDialog',
        onOkClick,
        okLabel: 'Delete',
    };

    test('if not opened, not found', () => {
        const closed = {
            ...props,
            open: false,
        };
        render(<ConfirmationDialog {...closed} />);
        const dialog = screen.queryByText('ConfirmationDialog');
        expect(dialog).not.toBeInTheDocument();
    });

    test('renders correctly', () => {
        render(<ConfirmationDialog {...props} />);
        const dialog = screen.getByText('ConfirmationDialog');
        expect(dialog).toBeInTheDocument();
    });

    test('cancel button callback works', () => {
        render(<ConfirmationDialog {...props} />);
        const button = screen.getByRole('button', { name: /cancel/i });
        userEvent.click(button);
        expect(props.onClose).toBeCalled();
    });

    test('delete button callback works', () => {
        render(<ConfirmationDialog {...props} />);
        const button = screen.getByRole('button', { name: /delete/i });
        userEvent.click(button);
        expect(props.onOkClick).toBeCalled();
    });
});
