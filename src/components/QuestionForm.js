import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import { useSelector } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import QuestionRules from './QuestionRules';
import QuestionSchedule from './QuestionSchedule';
import { selectQuestionId } from '../reducers';
import DangerButton from './DangerButton';

const QuestionForm = ({ isOpen, onClose, onSave, onDelete, classes }) => {
    const [selectedTab, setSelectedTab] = useState(0);

    const questionId = useSelector(selectQuestionId);
    const isExisting = Boolean(questionId);

    return (
        <Dialog open={isOpen} fullwidth="true" onClose={onClose}>
            <DialogTitle>Edit question</DialogTitle>
            <DialogContent>
                <Tabs
                    value={selectedTab}
                    fullwidth="true"
                    onChange={(_, tab) => setSelectedTab(tab)}
                >
                    <Tab label="Rules" />
                    <Tab label="Schedule" />
                </Tabs>
            </DialogContent>
            <DialogContent>
                {selectedTab === 0 && <QuestionRules classes={classes} />}
                {selectedTab === 1 && <QuestionSchedule classes={classes} />}
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose}>Cancel</Button>
                {!isExisting && (
                    <Button color="secondary" onClick={onSave}>
                        Save
                    </Button>
                )}
                {isExisting && (
                    <DangerButton onClick={() => onDelete(questionId)}>
                        Delete
                    </DangerButton>
                )}
            </DialogActions>
        </Dialog>
    );
};

const styles = theme => ({
    formControl: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
});

export default withStyles(styles)(QuestionForm);
