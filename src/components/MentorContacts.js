import React, { useState, useEffect } from 'react';
import { Typography, Button } from '@material-ui/core';
import { requestData } from '../utils';

const ContactList = ({ contacts, showIdentities, showMessages }) => (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-start' }}>
        {contacts && contacts[0] && contacts.map((contact, index) => (
            <Button
                variant="text"
                key={contact.id}
                onClick={() => showMessages(contact)}
            >
                {showIdentities ? contact.user.display_name : `User ${index}`}
            </Button>
        ))}
    </div>
);

const MentorContacts = ({ account, showIdentities, showMessages }) => {
    const [contacts, setContacts] = useState([]);

    useEffect(() => {
        const fetchContacts = async () => {
            try {
                const response = await requestData(`/api/users/${account.user.id}/contacts_for_admin`,
                    'GET');
                setContacts(response.resources);
            } catch (error) {
                console.error(error);
            }
        };
        fetchContacts();
    }, []);

    if (!contacts) {
        return <></>;
    }

    return (
        <div style={{ display: 'flex', flexDirection: 'column', height: '80vh' }}>
            <Typography variant="h6" gutterBottom>
                Users
            </Typography>
            <ContactList contacts={contacts} showIdentities={showIdentities} showMessages={showMessages} />
        </div>
    );
};

export default (MentorContacts);
