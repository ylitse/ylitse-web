import React from 'react';
import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

import userEvent from '@testing-library/user-event';
import TopBar from './TopBar';

const props = {
    username: 'test',
    toolbarLinks: [{ label: 'Test', to: '/test' }],
    version: { ui: '0.1', api: '0.1' },
    onLogoutClick: jest.fn(),
};

describe('TopBar', () => {
    test('renders 3 buttons', () => {
        render(<MemoryRouter><TopBar {...props} /></MemoryRouter>);
        const buttons = screen.getAllByRole('button');
        expect(buttons.length).toBe(3);
    });

    test('button-menu opens menu-options', () => {
        render(<MemoryRouter><TopBar {...props} /></MemoryRouter>);
        expect(screen.queryByText(/about/i)).not.toBeInTheDocument();
        const menuButton = screen.getByRole('button', { name: /menu/i });
        userEvent.click(menuButton);
        const about = screen.getByText(/about/i);
        expect(about).toBeInTheDocument();
    });

    test('menuitem-about opens about-dialog', () => {
        render(<MemoryRouter><TopBar {...props} /></MemoryRouter>);
        userEvent.click(screen.getByRole('button', { name: /menu/i }));
        const aboutButton = screen.getByRole('menuitem', { name: /about/i });
        userEvent.click(aboutButton);
        const about = screen.getAllByText(/about/i);
        expect(about.length).toBe(2);
    });

    test('logout-button callback works', () => {
        render(<MemoryRouter><TopBar {...props} /></MemoryRouter>);
        userEvent.click(screen.getByRole('button', { name: /menu/i }));
        const logoutButton = screen.getByRole('menuitem', { name: /logout/i });
        userEvent.click(logoutButton);
        expect(props.onLogoutClick).toBeCalled();
    });
});
