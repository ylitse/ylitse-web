import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Button } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import { withStyles } from '@material-ui/core/styles';
import { selectModalReport } from '../reducers';
import ReportDeleteConfimation from './ReportDeleteConfimation';
import ReportInfo from './ReportInfo';
import ReportConversation from './ReportConversation';
import ConfirmReport from './ConfirmReport';
import DangerButton from './DangerButton';

const styles = () => ({
    dialogcontent: {
        minWidth: '80vw',
    },
});

const statuses = {
    received: { nextStatus: 'handled', buttonText: 'Resolve' },
    handled: { nextStatus: 'received', buttonText: 'Restore' },
};

const SingleReport = ({ classes, closeReport, deleteReport, patchReport }) => {
    const [showIdentities, setShowIdentities] = useState(false);
    const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
    const [reportResolve, setReportResolve] = useState(null);
    const [selectedTab, setSelectedTab] = useState(0);

    const report = useSelector(selectModalReport);

    const toggleShowIdentites = () => {
        setShowIdentities(!showIdentities);
    };

    const toggleShowDeleteConfirmation = () => {
        setShowDeleteConfirmation(!showDeleteConfirmation);
    };

    const changeSelectedTab = (_event, newValue) => {
        setSelectedTab(newValue);
    };

    const openResolveDialog = () => {
        const { nextStatus } = statuses[report.status];
        setReportResolve({ status: nextStatus, id: report.id });
    };

    const closeResolveDialog = () => setReportResolve(null);

    const deleteReportHandler = () => {
        toggleShowDeleteConfirmation();
        closeReport();
        deleteReport(report.id);
    };

    if (!report) {
        return null;
    }

    return (
        <>
            <Dialog
                open
                onClose={closeReport}
                maxWidth="lg"
                className={classes.dialog}
            >
                <DialogTitle>Mentor report</DialogTitle>
                <DialogContent className={classes.dialogcontent}>
                    <Tabs
                        value={selectedTab}
                        fullwidth="true"
                        onChange={changeSelectedTab}
                    >
                        <Tab label="Report" />
                        <Tab label="Conversation" />
                    </Tabs>
                </DialogContent>
                <DialogContent>
                    {selectedTab === 0 && (
                        <ReportInfo
                            showIdentities={showIdentities}
                            report={report}
                            patchReport={patchReport}
                        />
                    )}
                    {selectedTab === 1 && (
                        <ReportConversation
                            showIdentities={showIdentities}
                            report={report}
                        />
                    )}
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeReport}>Close</Button>
                    <Button onClick={toggleShowIdentites}>
                        {showIdentities ? 'Hide identities' : 'Show identities'}
                    </Button>
                    <Button onClick={openResolveDialog}>
                        {statuses[report.status].buttonText}
                    </Button>
                    <DangerButton onClick={toggleShowDeleteConfirmation}>
                        Delete
                    </DangerButton>
                </DialogActions>
            </Dialog>
            {reportResolve && (
                <ConfirmReport
                    nextStatus={reportResolve.status}
                    reportId={report.id}
                    reportComment={report.comment}
                    patchReport={patchReport}
                    onClose={closeResolveDialog}
                />
            )}
            <ReportDeleteConfimation
                toggleshowDeleteConfirmation={toggleShowDeleteConfirmation}
                deleteReport={deleteReportHandler}
                showDeleteConfirmation={showDeleteConfirmation}
                reportId={report.id}
            />
        </>
    );
};

export default withStyles(styles)(SingleReport);
