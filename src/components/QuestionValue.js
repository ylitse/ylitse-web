import React from 'react';
import TextField from '@material-ui/core/TextField';
import { FormGroup, Typography } from '@material-ui/core';
import Localizable from './Localizable';

const QuestionValue = ({
    name,
    label,
    values,
    onChange,
    onLabelsChange,
    isDisabled,
    classes,
}) => {
    const numberValue = values?.value ?? '';
    const localizations = values?.labels ?? {};

    return (
        <FormGroup className={classes.formControl}>
            <Typography variant="h5" gutterBottom>
                {label}
            </Typography>
            <TextField
                name={name}
                label={label}
                id={name}
                value={numberValue}
                onChange={onChange}
                type="number"
                disabled={isDisabled}
            />
            <Localizable
                name={name}
                values={localizations}
                onLabelsChange={onLabelsChange}
                isDisabled={isDisabled}
            />
        </FormGroup>
    );
};

export default QuestionValue;
