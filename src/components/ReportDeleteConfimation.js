import React from 'react';
import { Modal, Paper, Button, Container } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import DangerButton from './DangerButton';

const styles = () => ({
    paper: {
        margin: 'auto',
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: '60vw',
        padding: '3rem',
    },
    container: {},
});

const ReportDeleteConfirmation = ({
    classes,
    toggleshowDeleteConfirmation,
    deleteReport,
    showDeleteConfirmation,
    reportId,
}) => {
    const handleDeleteReport = (reportIdToDelete) => {
        deleteReport(reportIdToDelete);
    };

    return showDeleteConfirmation ? (
        <Modal open>
            <Paper className={classes.paper}>
                Are you sure you want to delete report?
                <Container>
                    <Button onClick={toggleshowDeleteConfirmation}>Cancel</Button>
                    <DangerButton onClick={() => handleDeleteReport(reportId)}>
                        Delete
                    </DangerButton>
                </Container>
            </Paper>
        </Modal>
  ) : (
      <></>
  );
};

export default withStyles(styles)(ReportDeleteConfirmation);
