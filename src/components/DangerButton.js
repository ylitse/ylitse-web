import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import deepOrange from '@material-ui/core/colors/deepOrange';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: deepOrange,
    },
});

const DangerButton = ({ ButtonComponent, ...props }) => (
    <MuiThemeProvider theme={theme}>
        <ButtonComponent {...props} color="primary" />
    </MuiThemeProvider>
);

DangerButton.propTypes = {
    ButtonComponent: PropTypes.elementType,
};

DangerButton.defaultProps = {
    ButtonComponent: Button,
};

export default DangerButton;
