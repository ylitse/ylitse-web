import React from 'react';
import PropTypes from 'prop-types';

import { Button, List, ListItem, Typography } from '@material-ui/core';
import { question as questionPropType, defaultQuestion } from '../questionType';

const humanReadableDate = date => new Date(date).toLocaleDateString(window.navigator?.language ?? 'fi', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
});

const QuestionList = ({ questions, onQuestionClick }) => (
    <div style={{ padding: '16px' }}>
        <Button color="secondary" onClick={() => onQuestionClick(defaultQuestion)}>
            create new
        </Button>
        <List>
            {questions.map((question) => {
                const type = question.rules.answer.type === 'range'
            ? 'Range-question'
            : 'Yesno-question';

                const label = `${type}, created at: ${humanReadableDate(
                    question.created,
                )}`;

                return (
                    <ListItem
                        button
                        key={question.id}
                        onClick={() => onQuestionClick(question)}
                    >
                        <Typography variant="subtitle1">{label}</Typography>
                    </ListItem>
                );
            })}
        </List>
    </div>
);

QuestionList.propTypes = {
    questions: PropTypes.arrayOf(questionPropType).isRequired,
    onQuestionClick: PropTypes.func.isRequired,
};

export default QuestionList;
