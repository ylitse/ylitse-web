import React from 'react';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';
import { UserGenders, AvailableChannels } from '../constants';
import CheckboxGroupControl from './CheckboxGroupControl';
import ChipSelect from './ChipSelect';
import RadioGroupControl from './RadioGroupControl';

const styles = theme => ({
    row: {
        marginBottom: theme.spacing(1),
    },
    radioRow: {
        marginTop: theme.spacing(2),
        marginBottom: 0,
    },
    checkboxRow: {
        marginTop: theme.spacing(2),
        marginBottom: 0,
    },
    formControl: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    legend: {
        marginBottom: theme.spacing(1),
    },
    button: {
        marginTop: theme.spacing(2),
    },
    buttonIcon: {
        marginLeft: theme.spacing(1),
    },
});

const renderTextField = (name, label, required, account, other) => (
    <TextField
        name={name}
        label={label}
        id={name}
        value={account[name]}
        error={Boolean(other.errors[name])}
        helperText={other.errors[name]}
        required={required}
        className={other.classes.row}
        onChange={other.onValueChange}
    />
);

const showVacationInputsOnlyWhenUpdating = (isNew, account, onChangeIsVacationing, other) => {
    if (!isNew) {
        return (
            <>
                <FormLabel component="legend">
                    Status
                </FormLabel>
                <FormControlLabel
                    name="vacation"
                    label="Vacation"
                    checked={account.isVacationing}
                    control={<Checkbox onChange={onChangeIsVacationing} />}
                />
                <TextField
                    name="statusMessage"
                    label="Status message"
                    color="secondary"
                    value={account.statusMessage}
                    helperText="Write your status message"
                    multiline
                    rows={1}
                    rowsMax={8}
                    className={other.classes.row}
                    onChange={other.onValueChange}
                />
            </>
        );
    }

    return '';
};

const MentorForm = ({
    account, availableSkills, selectedSkills, skillsInputValue,
    languagesInputValue, availableLanguages, selectedLanguages, onSkillsChange,
    onSkillsRemove, onSkillsInputChange, onLanguagesChange, onLanguagesRemove,
    onLanguagesInputChange, onCheckboxesChange, onChangeIsVacationing, isNew, ...other
}) => (
    <form autoComplete="off">
        <FormGroup>
            {renderTextField('phone', 'Phone number', false, account, other)}
            <RadioGroupControl
                name="gender"
                label="Gender"
                value={account.gender}
                options={Object.values(UserGenders)}
                className={other.classes.radioRow}
                onChange={other.onValueChange}
            />

            {renderTextField('birthYear', 'Birth year', false, account, other)}
            {renderTextField('area', 'Area', false, account, other)}
            <FormControl
                disabled={availableSkills.length === 0}
                className={other.classes.formControl}
            >
                <FormLabel component="legend" className={other.classes.legend}>
                    Languages
                </FormLabel>
                <ChipSelect
                    itemName="language"
                    inputValue={languagesInputValue}
                    selectedItem={selectedLanguages}
                    availableItems={availableLanguages}
                    onChange={onLanguagesChange}
                    onRemoveItem={onLanguagesRemove}
                    onInputValueChange={onLanguagesInputChange}
                />

            </FormControl>
            <FormControl
                disabled={availableSkills.length === 0}
                className={other.classes.formControl}
            >
                <FormLabel component="legend" className={other.classes.legend}>
                    Skills
                </FormLabel>
                <ChipSelect
                    itemName="skill"
                    inputValue={skillsInputValue}
                    selectedItem={selectedSkills}
                    availableItems={availableSkills}
                    onChange={onSkillsChange}
                    onRemoveItem={onSkillsRemove}
                    onInputValueChange={onSkillsInputChange}
                />
            </FormControl>
            <CheckboxGroupControl
                label="Communication channels"
                options={AvailableChannels}
                checkedKeys={account.communicationChannels}
                className={other.classes.checkboxRow}
                onChange={onCheckboxesChange('communicationChannels')}
            />
            {showVacationInputsOnlyWhenUpdating(isNew, account, onChangeIsVacationing, other)}
            <TextField
                name="story"
                label="Story"
                color="secondary"
                value={account.story}
                helperText="Tell something about yourself"
                multiline
                rows={1}
                rowsMax={8}
                className={other.classes.row}
                onChange={other.onValueChange}
            />
        </FormGroup>
    </form>
);

MentorForm.propTypes = {
    account: PropTypes.shape({
        gender: PropTypes.string,
        languages: PropTypes.arrayOf(PropTypes.string),
        skills: PropTypes.arrayOf(PropTypes.string),
        communicationChannels: PropTypes.arrayOf(PropTypes.string),
        story: PropTypes.string,
        isVacationing: PropTypes.bool,
        statusMessage: PropTypes.string,
    }).isRequired,
    skillsInputValue: PropTypes.string.isRequired,
    languagesInputValue: PropTypes.string.isRequired,
    selectedSkills: PropTypes.arrayOf(PropTypes.string).isRequired,
    selectedLanguages: PropTypes.arrayOf(PropTypes.string).isRequired,
    availableSkills: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        id: PropTypes.string,
    })).isRequired,
    availableLanguages: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        id: PropTypes.string,
    })).isRequired,
    classes: PropTypes.shape({
        row: PropTypes.string,
        radioRow: PropTypes.string,
        checkboxRow: PropTypes.string,
        formControl: PropTypes.string,
        legend: PropTypes.string,
        button: PropTypes.string,
        buttonIcon: PropTypes.string,
    }).isRequired,
    onValueChange: PropTypes.func.isRequired,
    onCheckboxesChange: PropTypes.func.isRequired,
    onSkillsChange: PropTypes.func.isRequired,
    onSkillsInputChange: PropTypes.func.isRequired,
    onSkillsRemove: PropTypes.func.isRequired,
    onLanguagesChange: PropTypes.func.isRequired,
    onLanguagesInputChange: PropTypes.func.isRequired,
    onLanguagesRemove: PropTypes.func.isRequired,
    onChangeIsVacationing: PropTypes.func.isRequired,
    isNew: PropTypes.bool.isRequired,
};

export default withStyles(styles)(MentorForm);
