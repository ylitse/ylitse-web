import React from 'react';
import { render, screen } from '@testing-library/react';

import Loading from './Loading';

jest.useFakeTimers();

describe('Loading', () => {
    test('renders properly', () => {
        render(<Loading />);

        jest.runAllTimers();
        const loading = screen.getByRole('progressbar');
        expect(loading).toBeInTheDocument();
        jest.clearAllTimers();
    });
});
