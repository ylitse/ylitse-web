import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import ChipSelect from './ChipSelect';

describe('ChipSelect', () => {
    const onRemoveItem = jest.fn();

    const availableItems = [
        { name: 'F6BAB4966X', id: 'f6bab4966x' },
        { name: 'ZHTLBN7GZ8', id: 'zhtlbn7gz8' },
        { name: '7K5J4I4J7D', id: '7k5j4i4j7d' },
        { name: 'P7E124IALI', id: 'p7e124iali' },
        { name: 'F4AWNF4COZ', id: 'f4awnf4coz' },
        { name: 'NMT10X4E6H', id: 'nmt10x4e6h' },
        { name: '0CW2WL81FX', id: '0cw2wl81fx' },
        { name: 'GLP2GZ2UVF', id: 'glp2gz2uvf' },
    ];

    const props = {
        availableItems,
        onRemoveItem,
        selectedItem: [],
        itemName: 'skill',
    };

    test('Input receive correct props', () => {
        render(<ChipSelect {...props} inputValue="a test value" />);
        const textBox = screen.getByRole('textbox');
        expect(textBox).toHaveValue('a test value');
    });

    test('Input change value correctly', () => {
        render(<ChipSelect {...props} inputValue="other value" />);
        const textBox = screen.getByRole('textbox');
        expect(textBox).toHaveValue('other value');
    });

    test('render chips correctly', () => {
        const selectedItem = ['F6BAB4966X'];
        render(<ChipSelect {...props} selectedItem={selectedItem} />);
        const chip = screen.getByText('F6BAB4966X');
        expect(chip).toBeInTheDocument();
        const textBox = screen.getByRole('textbox');
        expect(textBox).toHaveValue('F6BAB4966X');
    });

    test('get the correct suggestion', () => {
        render(<ChipSelect {...props} inputValue="f4" isOpen />);
        const option = screen.getByRole('option');
        expect(option).toHaveTextContent('F4AWNF4COZ');
    });

    test('open list on click', () => {
        render(<ChipSelect {...props} />);
        const optionsAtStart = screen.queryAllByRole('option');
        expect(optionsAtStart.length).toEqual(0);

        const input = screen.getByRole('textbox');
        userEvent.click(input);
        const optionsAfterClick = screen.getAllByRole('option');
        expect(optionsAfterClick.length).toEqual(8);
    });
});
