import React, { useState, useEffect } from 'react';
import { Typography } from '@material-ui/core';
import { requestData } from '../utils';

const MessageList = ({
    messages,
    showIdentities,
    showMessagesUser,
    account,
}) => {
    // eslint-disable-next-line no-nested-ternary
    const user1 = showIdentities ? account.user.displayName ? account.user.displayName : account.account.loginName : 'mentor';
    // eslint-disable-next-line no-nested-ternary
    const user2 = showIdentities ? showMessagesUser.user.display_name ? showMessagesUser.user.display_name : showMessagesUser.account.login_name : 'mentee';
    return (
        <div>
            {messages
        && messages[0]
        && messages.map(message => (
            <Typography variant="body1" key={message.id}>
                {message.sender_id === account.user.id ? user1 : user2}:{' '}
                {message.content}
            </Typography>
        ))}
        </div>
    );
};

const MentorConversation = ({ account, showIdentities, showMessagesUser }) => {
    const [messages, setMessages] = useState([]);
    const [hasError, setHasError] = useState(false);
    const [error, setError] = useState('');

    useEffect(() => {
        const fetchMessages = async () => {
            try {
                const response = await requestData(
                    `/api/users/${account.user.id}/messages_for_admin?&contact_user_ids=${showMessagesUser.user.id}&desc=true`,
                    'GET',
                );
                setMessages(response.resources);
            } catch (e) {
                console.error(e);
                setHasError(true);
                setError(e);
            }
        };
        fetchMessages();
    }, []);

    if (hasError) {
        if (error.toString().includes('Quota exceeded')) {
            return <div style={{ display: 'flex', flexDirection: 'column', height: '80vh', color: 'red' }}>Error: Quota exceeded</div>;
        }
        return <div style={{ display: 'flex', flexDirection: 'column', height: '80vh', color: 'red' }}>Error: Something went wrong</div>;
    }

    return (
        <div style={{ display: 'flex', flexDirection: 'column', height: '80vh' }}>
            <Typography variant="h6" gutterBottom>
                Conversation
            </Typography>
            <MessageList
                messages={messages}
                showIdentities={showIdentities}
                showMessagesUser={showMessagesUser}
                account={account}
            />
        </div>
    );
};

export default MentorConversation;
