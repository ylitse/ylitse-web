import React from 'react';
import deepOrange from '@material-ui/core/colors/deepOrange';
import { DataGrid, GridToolbar } from '@material-ui/data-grid';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import PasswordIcon from '@material-ui/icons/VpnKey';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';

const styles = theme => ({
    firstCell: {
        paddingLeft: 0,
    },
    lastCell: {
        '&:last-child': {
            paddingRight: [0, '!important'],
            textAlign: ['right', '!important'],
            whiteSpace: 'nowrap',
        },
    },
    role: {
        textTransform: 'capitalize',
    },
    iconButton: {
        '&:hover': {
            color: theme.palette.secondary.main,
        },
        marginLeft: 5,
        marginRight: -8,
    },
    danger: {
        '&:hover': {
            color: deepOrange.main,
        },
    },
    icon: {
        fontSize: 22,
    },

});

const AccountListDataGrid = ({
    accounts, classes, stats, onEdit, onDelete, onPassChange, openConversation,
}) => {
    const columns = [
        {
            field: 'username',
            headerName: 'Username',
            minWidth: 100,
            flex: 1,
        },
        {
            field: 'role',
            headerName: 'Role',
            width: 110,
        },
        {
            field: 'email',
            headerName: 'Email',
            minWidth: 100,
            flex: 1,
        },
        {
            field: 'sent',
            headerName: 'Sent',
            type: 'number',
            width: 100,
        },
        {
            field: 'received',
            headerName: 'Received',
            type: 'number',
            width: 100,
        },
        {
            field: 'banned',
            headerName: 'Banned',
            type: 'number',
            width: 100,
        },
        {
            field: 'status',
            headerName: 'Status',
            width: 100,
        },
        {
            field: 'created',
            headerName: 'Created',
            type: 'dateTime',
            minWidth: 100,
            flex: 1,
            valueFormatter: (params) => {
                const dateTimeOptions = { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric', timeZoneName: 'short' };
                const accountCreated = new Intl.DateTimeFormat('fi-FI', dateTimeOptions).format(params.value);
                return accountCreated;
            },
        },
        {
            field: 'Actions',
            filterable: false,
            sortable: false,
            width: 200,
            renderCell: (cellValues) => {
                const account = { ...cellValues.row };
                return (
                    <>
                        <IconButton
                            name="edit"
                            className={classes.iconButton}
                            onClick={onEdit(account)}
                            aria-label="edit"
                        >
                            <EditIcon className={classes.icon} />
                        </IconButton>
                        <IconButton
                            name="pass"
                            className={classes.iconButton}
                            onClick={onPassChange(account)}
                            aria-label="password"
                        >
                            <PasswordIcon className={classes.icon} />
                        </IconButton>
                        <IconButton
                            name="delete"
                            className={`${classes.iconButton} ${classes.danger}`}
                            onClick={onDelete(account)}
                            aria-label="delete"
                        >
                            <DeleteIcon className={classes.icon} />
                        </IconButton>
                        {account.role === 'mentor'
                        && (
                            <IconButton
                                name="conversations"
                                className={`${classes.iconButton} ${classes.danger}`}
                                onClick={openConversation(account)}
                                aria-label="conversations"
                            >
                                <QuestionAnswerIcon className={classes.icon} />
                            </IconButton>
                        )
                        }
                    </>
                );
            },
        },
    ];

    const rows = [];
    accounts.forEach((account) => {
        const accountStats = stats && stats[account.id] ? stats[account.id] : { sent: 0, received: 0, banned: 0 };
        const accountStatus = account.role === 'mentor' && account.isVacationing ? 'Vacationing' : '';
        // backend gives datetimes without timezone info
        // force utc with +Z if it is not there already
        const dateTimeWithZone = account.account.created.endsWith('Z') || account.account.created.endsWith('+00:00') ? account.account.created : `${account.account.created}Z`;
        const accountCreated = new Date(dateTimeWithZone);
        rows.push({ ...account, sent: accountStats.sent, received: accountStats.received, banned: accountStats.banned, status: accountStatus, created: accountCreated });
    });

    return (
        <div style={{ display: 'flex', height: '100%' }}>
            <div style={{ flexGrow: 1 }}>
                <DataGrid
                    editable={false}
                    autoHeight
                    rows={rows}
                    columns={columns}
                    components={{
                        Toolbar: GridToolbar,
                    }}
                />
            </div>
        </div>
    );
};

export default withStyles(styles)(AccountListDataGrid);
