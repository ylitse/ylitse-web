import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import ReportComment from './ReportComment';

const styles = () => ({});

const ReportStatus = ({ report, patchReport }) => {
    const isHandled = report.status === 'handled';

    return (
        <Box>
            <Typography variant="h6" gutterBottom>
                Status
            </Typography>
            <Box display="flex" sx={{ gap: '4px' }}>
                <Typography color={isHandled ? 'secondary' : 'primary'}>
                    {report.status}
                </Typography>
                {report.comment && (
                    <ReportComment report={report} patchReport={patchReport} />
                )}
            </Box>
        </Box>
    );
};

export default withStyles(styles)(ReportStatus);
