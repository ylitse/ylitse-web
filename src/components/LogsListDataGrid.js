import React from 'react';
import deepOrange from '@material-ui/core/colors/deepOrange';
import { DataGrid, GridToolbar } from '@material-ui/data-grid';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    firstCell: {
        paddingLeft: 0,
    },
    lastCell: {
        '&:last-child': {
            paddingRight: [0, '!important'],
            textAlign: ['right', '!important'],
            whiteSpace: 'nowrap',
        },
    },
    role: {
        textTransform: 'capitalize',
    },
    iconButton: {
        '&:hover': {
            color: theme.palette.secondary.main,
        },
        marginLeft: 5,
        marginRight: -8,
    },
    danger: {
        '&:hover': {
            color: deepOrange.main,
        },
    },
    icon: {
        fontSize: 22,
    },
});

const LogsListDataGrid = ({ logs, classes }) => {
    const columns = [
        {
            field: 'reviewerUserName',
            headerName: 'Viewer',
            minWidth: 100,
            flex: 1,
        },
        {
            field: 'created',
            headerName: 'Created',
            type: 'dateTime',
            minWidth: 100,
            flex: 1,
            valueFormatter: (params) => {
                const dateTimeOptions = { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric', timeZoneName: 'short' };
                const reportCreated = new Intl.DateTimeFormat('fi-FI', dateTimeOptions).format(params.value);
                return reportCreated;
            },
        },
        {
            field: 'chatUserName1',
            headerName: 'Chat participant',
            minWidth: 100,
            flex: 1,
        },
        {
            field: 'chatUserName2',
            headerName: 'Chat participant',
            minWidth: 100,
            flex: 1,
        },
    ];

    const rows = logs.map((log) => {
        const dateTimeWithZone = log.created.endsWith('Z') || log.created.endsWith('+00:00') ? log.created : `${log.created}Z`;
        const logCreated = new Date(dateTimeWithZone);
        return { ...log, created: logCreated };
    });

    return (
        <div style={{ display: 'flex', height: '100%' }}>
            <div style={{ flexGrow: 1 }}>
                <DataGrid
                    editable={false}
                    autoHeight
                    rows={rows}
                    columns={columns}
                    components={{
                        Toolbar: GridToolbar,
                    }}
                    classes={{
                        root: classes.root,
                        cell: classes.cell,
                    }}
                />
            </div>
        </div>
    );
};

export default withStyles(styles)(LogsListDataGrid);
