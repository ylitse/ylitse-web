import React from 'react';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
    label: {
        textTransform: 'capitalize',
    },
});

const RadioGroupControl = ({
    name, label, value, options, disabled, required, className, classes,
    onChange,
}) => (
    <FormControl
        component="fieldset"
        disabled={disabled}
        required={required}
        className={className}
    >
        <FormLabel component="legend">{label}</FormLabel>
        <RadioGroup
            name={name}
            value={value}
            row
            onChange={onChange}
        >
            {options.map(option => (
                <FormControlLabel
                    key={option}
                    label={option}
                    value={option}
                    control={<Radio />}
                    className={classes.label}
                />
            ))}
        </RadioGroup>
    </FormControl>
);

RadioGroupControl.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    options: PropTypes.arrayOf(PropTypes.string).isRequired,
    disabled: PropTypes.bool,
    required: PropTypes.bool,
    className: PropTypes.string,
    classes: PropTypes.shape({ label: PropTypes.string }).isRequired,
    onChange: PropTypes.func.isRequired,
};

RadioGroupControl.defaultProps = {
    disabled: false,
    required: false,
    className: '',
};

export default withStyles(styles)(RadioGroupControl);
