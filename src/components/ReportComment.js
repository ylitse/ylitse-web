import React from 'react';
import { Box, IconButton, TextField, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import EditIcon from '@material-ui/icons/Edit';

const styles = () => ({});

const ReportComment = ({ report, patchReport }) => {
    const [localComment, setLocalComment] = React.useState(report.comment ?? '');
    const [isEditingComment, setIsEditingComment] = React.useState(false);

    const toggleEditing = () => {
        setIsEditingComment(!isEditingComment);
    };

    const handleKeyDown = (event) => {
        switch (event.key) {
            case 'Enter': {
                patchReport({
                    reportId: report.id,
                    status: report.status,
                    comment: localComment,
                });
                setIsEditingComment(false);
                break;
            }
            case 'Escape': {
                setLocalComment(report.comment ?? '');
                setIsEditingComment(false);
                break;
            }
            default:
                break;
        }
    };

    React.useEffect(() => {
        setLocalComment(report.comment);
    }, [report.comment]);

    return (
        <Box display="flex" alignItems="center">
            {isEditingComment ? (
                <TextField
                    name="comment"
                    color="secondary"
                    value={localComment}
                    multiline
                    maxRows={8}
                    onChange={e => setLocalComment(e.target.value)}
                    onKeyDown={handleKeyDown}
                />
      ) : (
          <Typography>
              {localComment}
              <IconButton name="edit" size="small" onClick={toggleEditing}>
                  <EditIcon fontSize="small" />
              </IconButton>
          </Typography>
      )}
        </Box>
    );
};

export default withStyles(styles)(ReportComment);
