import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import deepOrange from '@material-ui/core/colors/deepOrange';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';

const styles = theme => ({
    text: {
        noWrap: true,
        fontSize: 16,
    },
    card: {
        background: theme.palette.background.default,
        margin: [[5, 7, 5, 3]],
        paddingLeft: 8,
    },
    cardContent: {
        paddingTop: 6,
        paddingBottom: [6, '!important'],
    },
    iconButton: {
        '&:hover': {
            color: theme.palette.secondary.main,
        },
        marginLeft: 8,
        marginRight: -10,
    },
    danger: {
        '&:hover': {
            color: deepOrange.main,
        },
    },
    icon: {
        fontSize: 22,
    },
});

const SkillListItem = ({ label, classes, onDelete }) => (
    <Card
        className={classes.card}
    >
        <CardContent className={classes.cardContent}>
            <Typography
                className={classes.text}
            >
                {label}
                <IconButton
                    className={`${classes.iconButton} ${classes.danger}`}
                    onClick={onDelete}
                >
                    <DeleteIcon className={classes.icon} />
                </IconButton>
            </Typography>
        </CardContent>
    </Card>
);

SkillListItem.propTypes = {
    label: PropTypes.string.isRequired,
    classes: PropTypes.shape({
        text: PropTypes.string,
        card: PropTypes.string,
        cardContent: PropTypes.string,
        iconButton: PropTypes.string,
        icon: PropTypes.string,
    }).isRequired,
    onDelete: PropTypes.func.isRequired,
};

export default withStyles(styles)(SkillListItem);
