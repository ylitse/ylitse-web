import React from 'react';
import { TextField } from '@material-ui/core';

const Localizable = ({ name, values, onLabelsChange, isDisabled }) => {
    const handleLocalizationTextChange = (text, locale) => {
        const updatedTexts = { ...values, [locale]: text };
        onLabelsChange(updatedTexts);
    };

    return (
        <>
            {Object.keys(values).map(locale => (
                <TextField
                    key={`${locale}_${name}_localization`}
                    name={`${locale}_${name}_localization`}
                    id={`${locale}_${name}_localization`}
                    label={`${locale}: question text`}
                    value={values[locale] ?? ''}
                    onChange={event => handleLocalizationTextChange(event.target.value, locale)
                    }
                    disabled={isDisabled}
                    multiline
                    minRows={1}
                    maxRows={8}
                />
            ))}
        </>
    );
};

export default Localizable;
