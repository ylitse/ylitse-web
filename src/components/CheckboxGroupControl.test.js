import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import CheckboxGroupControl from './CheckboxGroupControl';

describe('CheckboxGroupControl', () => {
    const mockFunc = jest.fn();
    const props = {
        options: { fi: 'finnish' },
        checkedKeys: ['fi'],
        className: '',
        label: 'Language',
        onChange: mockFunc,
    };

    test('checkbox can be clicked', () => {
        render(<CheckboxGroupControl {...props} />);
        const checkbox = screen.getByRole('checkbox');
        userEvent.click(checkbox);
        expect(mockFunc).toHaveBeenCalled();
    });

    test('checkbox shows checked', () => {
        render(<CheckboxGroupControl {...props} />);
        const checkbox = screen.getByRole('checkbox');
        expect(checkbox.checked).toBe(true);
    });

    test('checkbox can be checked and unchecked', () => {
        const { rerender } = render(<CheckboxGroupControl {...props} />);
        const checkbox = screen.getByRole('checkbox');
        expect(checkbox.checked).toBe(true);

        const updatedProps = { ...props, checkedKeys: [] };

        rerender(<CheckboxGroupControl {...updatedProps} />);
        const updatedCheckbox = screen.getByRole('checkbox');
        expect(updatedCheckbox.checked).toBe(false);
    });
});

