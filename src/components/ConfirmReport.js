import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Box, DialogContentText, TextField } from '@material-ui/core';

const content = {
    received: {
        title: 'Restore',
        Element: () => (
            <DialogContentText>Do you want to restore the report?</DialogContentText>
        ),
    },
    handled: {
        title: 'Resolve',
        Element: ({ comment, setComment }) => (
            <Box display="flex" flexDirection="column">
                <DialogContentText>Resolve the report</DialogContentText>
                <TextField
                    name="comment"
                    label="Comment"
                    color="secondary"
                    value={comment}
                    helperText="Resolution of report"
                    multiline
                    maxRows={8}
                    onChange={e => setComment(e.target.value)}
                />
            </Box>
        ),
    },
};

const ConfirmReport = ({
    nextStatus,
    onClose,
    patchReport,
    reportId,
    reportComment,
}) => {
    const { title, Element } = content[nextStatus];
    const [comment, setComment] = React.useState(reportComment);

    const handlePatchReport = () => {
        patchReport({ reportId, status: nextStatus, comment });
        onClose();
    };

    return (
        <Dialog onClose={onClose} open>
            <DialogTitle>{title} report</DialogTitle>
            <DialogContent>{Element({ comment, setComment })}</DialogContent>
            <DialogActions>
                <Button onClick={onClose}>Cancel</Button>
                <Button onClick={handlePatchReport}>{title}</Button>
            </DialogActions>
        </Dialog>
    );
};

ConfirmReport.propTypes = {
    nextStatus: PropTypes.string,
    patchReport: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired,
    reportId: PropTypes.string.isRequired,
    reportComment: PropTypes.string,
};

ConfirmReport.defaultProps = {
    nextStatus: 'received',
    reportComment: '',
};

export default ConfirmReport;
