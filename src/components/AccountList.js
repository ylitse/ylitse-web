import React from 'react';
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableRow from '@material-ui/core/TableRow';
import TableFooter from '@material-ui/core/TableFooter';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import AccountListDataGrid from './AccountListDataGrid';

const styles = () => ({
    table: {
        overflowX: 'auto',
    },
    firstCell: {
        paddingLeft: 0,
    },
    lastCell: {
        '&:last-child': {
            paddingRight: [0, '!important'],
            textAlign: ['right', '!important'],
        },
    },
    stats: {
        paddingTop: 24,
    },
});

const AccountList = ({
    accounts,
    stats,
    totals,
    onEdit,
    onDelete,
    onPassChange,
    openConversation,
    classes,
}) => (
    <main>
        {accounts.length > 0 && (
            <>
                <AccountListDataGrid
                    classes={classes}
                    accounts={accounts}
                    stats={stats}
                    onEdit={onEdit}
                    onDelete={onDelete}
                    onPassChange={onPassChange}
                    openConversation={openConversation}
                />

                <Table className={classes.table}>
                    <TableFooter>
                        <TableRow>
                            <td>
                                <Typography variant="subtitle1" className={classes.stats}>
                                    Total messages, admins: {totals.admins.sent}/
                                    {totals.admins.received}
                                </Typography>
                            </td>
                        </TableRow>
                        <TableRow>
                            <td>
                                <Typography variant="subtitle1" className={classes.stats}>
                                    Total messages, mentors: {totals.mentors.sent}/
                                    {totals.mentors.received}
                                </Typography>
                            </td>
                        </TableRow>
                        <TableRow>
                            <td>
                                <Typography variant="subtitle1" className={classes.stats}>
                                    Total messages, mentees: {totals.mentees.sent}/
                                    {totals.mentees.received}
                                </Typography>
                            </td>
                        </TableRow>
                        <TableRow>
                            <td>
                                <Typography variant="subtitle1" className={classes.stats}>
                                    Number of registered mentees: {totals.accounts.mentees}
                                </Typography>
                            </td>
                        </TableRow>
                        <TableRow>
                            <td>
                                <Typography variant="subtitle1" className={classes.stats}>
                                    Number of registered mentors: {totals.accounts.mentors}
                                </Typography>
                            </td>
                        </TableRow>
                        <TableRow>
                            <td>
                                <Typography variant="subtitle1" className={classes.stats}>
                                    Number of vacation status: {totals.accounts.isVacationing}
                                </Typography>
                            </td>
                        </TableRow>
                        <TableRow>
                            <td>
                                <Typography variant="subtitle1" className={classes.stats}>
                                    Total of banned mentors: {totals.mentors.banned}
                                </Typography>
                            </td>
                        </TableRow>
                        <TableRow>
                            <td>
                                <Typography variant="subtitle1" className={classes.stats}>
                                    Total of banned mentees: {totals.mentees.banned}
                                </Typography>
                            </td>
                        </TableRow>
                    </TableFooter>
                </Table>
            </>
        )}
        {accounts.length === 0 && (
            <Typography variant="h2">No user accounts</Typography>
        )}
    </main>
);

AccountList.propTypes = {
    accounts: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string,
            name: PropTypes.string,
        }),
    ).isRequired,
    totals: PropTypes.shape({
        admins: PropTypes.object,
        mentors: PropTypes.object,
        mentees: PropTypes.object,
        accounts: PropTypes.shape({
            mentees: PropTypes.number,
            mentors: PropTypes.number,
        }),
    }),
    stats: PropTypes.objectOf(
        PropTypes.shape({
            sent: PropTypes.number.isRequired,
            received: PropTypes.number.isRequired,
            banned: PropTypes.number.isRequired,
        }).isRequired,
    ),
    onEdit: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onPassChange: PropTypes.func.isRequired,
    classes: PropTypes.shape({
        table: PropTypes.string,
        firstCell: PropTypes.string,
        lastCell: PropTypes.string,
    }).isRequired,
};

AccountList.defaultProps = {
    totals: {
        admins: { sent: 0, received: 0, banned: 0 },
        mentors: { sent: 0, received: 0, banned: 0 },
        mentees: { sent: 0, received: 0, banned: 0 },
        accounts: { mentees: 0, mentors: 0 },
    },
    stats: {},
};

export default withStyles(styles)(AccountList);
