import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { FormGroup, Typography } from '@material-ui/core';
import RadioGroupControl from './RadioGroupControl';
import { NumberValue } from './NumberValue';
import { selectSelectedQuestionSchedule } from '../reducers';
import { QuestionsActionTypes } from '../actions';

const defaultRepetition = {
    predefined: {
        type: 'predefined',
        times: 0,
        days_since_previous_answer: 100,
        messages_since_previous_answer: 100,
        min_days_since_previous_answer: 14,
    },
    until_value: {
        type: 'until_value',
        value: 1,
        comparison: 'exact',
        days_since_previous_answer: 7,
    },
};

const scheduleRepetitionsFields = {
    predefined: {
        times: { label: 'How many times' },
        days_since_previous_answer: { label: 'Days since previous answer' },
        messages_since_previous_answer: { label: 'Messages since previous answer' },
        min_days_since_previous_answer: { label: 'Min days since previous answer' },
    },
    until_value: {
        value: { label: 'ask until question answer-value is' },
        days_since_previous_answer: { label: 'Days since previous answer' },
    },
};

const scheduleTypeMap = {
    until_value: 'until',
    predefined: 'predefined',
    until: 'until_value',
};
const QuestionSchedule = ({ classes }) => {
    const dispatch = useDispatch();
    const { schedule, isExisting } = useSelector(selectSelectedQuestionSchedule);
    const scheduleType = schedule?.repetitions?.type ?? 'predefined';

    const setSchedule = (updatedSchedule) => {
        dispatch({
            type: QuestionsActionTypes.SET_QUESTION_RULES,
            key: 'schedule',
            value: updatedSchedule,
        });
    };

    const handleScheduleChange = (rule, key, value) => {
        const updatedScheduleRules = {
            ...schedule,
            [rule]: { ...schedule[rule], [key]: value },
        };

        setSchedule(updatedScheduleRules);
    };

    const handleScheduleTypeChange = (value) => {
        const updatedScheduleRules = {
            ...schedule,
            repetitions: defaultRepetition[scheduleTypeMap[value]],
        };

        setSchedule(updatedScheduleRules);
    };

    const handleReminChange = (value, key) => {
        const updatedScheduleRules = {
            ...schedule,
            [key]: value,
        };
        setSchedule(updatedScheduleRules);
    };

    return (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
            <FormGroup className={classes.formControl}>
                <Typography variant="h5" gutterBottom>
                    First time
                </Typography>
                <NumberValue
                    name="days_since_registration"
                    label="How many days since registration to ask question"
                    value={schedule?.first?.days_since_registration ?? ''}
                    onChange={value => handleScheduleChange('first', 'days_since_registration', value)
                    }
                    isDisabled={isExisting}
                />
                <NumberValue
                    name="sent_messages_threshold"
                    label="How many messages to be sent before ask question"
                    value={schedule?.first?.sent_messages_threshold ?? ''}
                    onChange={value => handleScheduleChange('first', 'sent_messages_threshold', value)
                    }
                    isDisabled={isExisting}
                />
                <NumberValue
                    name="max_old_account_in_days"
                    label="Maximum old (days) account to receive the question"
                    value={schedule?.first?.max_old_account_in_days ?? ''}
                    onChange={value => handleScheduleChange('first', 'max_old_account_in_days', value)
                    }
                    isDisabled={isExisting}
                />
            </FormGroup>
            <FormGroup className={classes.formControl}>
                <Typography variant="h5" gutterBottom>
                    Repetitions
                </Typography>
                <RadioGroupControl
                    name="scheduleType"
                    label="Schedule-type"
                    value={scheduleTypeMap[schedule?.repetitions?.type] ?? 'until'}
                    options={['until', 'predefined']}
                    onChange={event => handleScheduleTypeChange(event.target.value)}
                    disabled={isExisting}
                />
                {Object.keys(scheduleRepetitionsFields[scheduleType]).map(field => (
                    <NumberValue
                        key={field}
                        name={field}
                        label={scheduleRepetitionsFields[scheduleType][field].label}
                        value={schedule?.repetitions?.[field] ?? ''}
                        onChange={value => handleScheduleChange('repetitions', field, value)
                        }
                        isDisabled={isExisting}
                    />
                ))}
            </FormGroup>
            <FormGroup className={classes.formControl}>
                <Typography variant="h5" gutterBottom>
                    Reminder
                </Typography>
                <NumberValue
                    name="remind_times_when_skipped"
                    label="How many times to remind when skipped"
                    value={schedule?.remind_times_when_skipped ?? ''}
                    onChange={value => handleReminChange(value, 'remind_times_when_skipped')
                    }
                    isDisabled={isExisting}
                />
                <NumberValue
                    name="remind_interval_days"
                    label="The amount of time that has to pass before user is reminded"
                    value={schedule?.remind_interval_days ?? ''}
                    onChange={value => handleReminChange(value, 'remind_interval_days')}
                    isDisabled={isExisting}
                />
            </FormGroup>
        </div>
    );
};

export default QuestionSchedule;
