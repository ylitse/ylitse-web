import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    root: theme.mixins.gutters({
        marginTop: theme.spacing(4),
        marginLeft: '2%',
        marginRight: '2%',
        paddingTop: 24,
        paddingBottom: 24,
        overflowX: 'auto',
    }),
});

const Page = ({ header, children, classes }) => (
    <Paper className={classes.root}>
        {header && (
            <Typography variant="h5" component="h3">
                {header}
            </Typography>
        )}
        {children}
    </Paper>
);

Page.propTypes = {
    header: PropTypes.string,
    children: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.element,
        PropTypes.string,
    ]).isRequired,
    classes: PropTypes.shape({ root: PropTypes.string }).isRequired,
};

Page.defaultProps = {
    header: '',
};

export default withStyles(styles)(Page);
