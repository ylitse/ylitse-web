import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import PasswordField from './PasswordField';

describe('PasswordField', () => {
    test('icon button toggles the visibility', () => {
        render(<PasswordField />);
        const inputField = screen.getByLabelText(/password/i, { name: /password/i });
        expect(inputField.getAttribute('type')).toBe('password');
        const toggleInputFieldType = screen.getByRole('button');
        userEvent.click(toggleInputFieldType);
        expect(inputField.getAttribute('type')).toBe('text');
    });

    test('helper text is displayed', () => {
        render(<PasswordField helperText="Not a good password" />);
        const helperText = screen.getByText(/Not a good/i);
        expect(helperText).toBeInTheDocument();
    });
});
