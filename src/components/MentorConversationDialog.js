import React, { useState } from 'react';
import { Button } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { withStyles } from '@material-ui/core/styles';
import MentorConversation from './MentorConversations';
import MentorContacts from './MentorContacts';

const styles = () => ({
    dialogcontent: {
        overflow: 'visible',
        minWidth: '80vw',
        width: '80vw',
    },
    dialog: {
        paper: {
            minWidth: '80vw',
            width: '80vw',
        },
    },
});

const MentorConversationDialog = ({ classes, onClose, account }) => {
    const [showIdentities, setShowIdentities] = useState(false);
    const [showMessages, setShowMessages] = useState(false);
    const [showMessagesUserId, setShowMessagesUserId] = useState(undefined);

    const toggleShowIdentites = () => {
        setShowIdentities(!showIdentities);
    };

    const handleOnClose = () => {
        onClose();
    };

    const handleSetShowMessagesUserId = (userId) => {
        setShowMessagesUserId(userId);
        setShowMessages(true);
    };

    return (
        <>
            <Dialog
                open
                onClose={handleOnClose}
                maxWidth="lg"
                fullWidth
                className={classes.dialog}
            >
                <DialogTitle>Mentor conversations</DialogTitle>
                <DialogContent className={classes.dialogContent}>
                    {!showMessages && (
                        <MentorContacts
                            showIdentities={showIdentities}
                            account={account}
                            showMessages={handleSetShowMessagesUserId}
                        />
                    )}
                    {showMessages && (
                        <MentorConversation
                            showIdentities={showIdentities}
                            account={account}
                            showMessagesUser={showMessagesUserId}
                        />
                    )}
                </DialogContent>
                <DialogActions>
                    {showMessages && (
                        <Button onClick={() => setShowMessages(false)}>Show contacts</Button>
                    )}
                    <Button onClick={handleOnClose}>Close</Button>
                    <Button onClick={toggleShowIdentites}>
                        {showIdentities ? 'Hide identities' : 'Show identities'}
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default withStyles(styles)(MentorConversationDialog);
