import React from 'react';
import { render, screen } from '@testing-library/react';

import AboutDialog from './AboutDialog';

describe('AboutDialog', () => {
    const props = {
        uiVersion: 'N/A', apiVersion: 'N/A', onOkClick: () => {}, open: true,
    };

    it('renders About', () => {
        render(<AboutDialog {...props} />);

        const aboutText = screen.getByText(/About/i);
        expect(aboutText).toBeVisible();
    });

    it('renders button', () => {
        render(<AboutDialog {...props} />);

        const okButton = screen.getByRole('button');
        expect(okButton).toBeInTheDocument();
    });

    it('renders prop UI-version', () => {
        render(<AboutDialog {...props} />);

        const uiText = screen.getByText(/UI version/i);
        expect(uiText).toHaveTextContent(`UI version: ${props.uiVersion}`);
    });

    it('renders prop API-version', () => {
        render(<AboutDialog {...props} />);

        const uiText = screen.getByText(/API version/i);
        expect(uiText).toHaveTextContent(`API version: ${props.apiVersion}`);
    });
});

