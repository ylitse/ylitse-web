import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import ReportStatus from './ReportStatus';

const styles = () => ({});

const hiddenIdentity = '*'.repeat(6);

const ReportInfo = ({ showIdentities, report, patchReport }) => (
    <Box
        display="flex"
        flexDirection="column"
        height="80vh"
        sx={{ gap: '16px' }}
    >
        <Box>
            <Typography variant="h6" gutterBottom>
                Reported user
            </Typography>
            <Typography>
                {showIdentities ? report.reportedUserName : hiddenIdentity}
            </Typography>
        </Box>
        <ReportStatus report={report} patchReport={patchReport} />
        <Box>
            <Typography variant="h6" gutterBottom>
                Reason
            </Typography>
            <Typography>{report.reportReason}</Typography>
        </Box>
        <Box>
            <Typography variant="h6" gutterBottom>
                Reporter
            </Typography>
            <Typography>
                Reporter: {showIdentities ? report.reporterUserName : hiddenIdentity}
            </Typography>
            <Typography>
                Reporter contact field: {showIdentities ? report.contactField : ' '}
            </Typography>
        </Box>
    </Box>
);

export default withStyles(styles)(ReportInfo);
