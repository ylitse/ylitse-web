import React from 'react';
import TextField from '@material-ui/core/TextField';

export const NumberValue = ({ name, label, value, onChange, isDisabled }) => (
    <TextField
        name={name}
        label={label}
        id={name}
        value={value}
        onChange={event => onChange(Number(event.target.value))}
        type="number"
        disabled={isDisabled}
    />
);

export default NumberValue;
