import React, { Component } from 'react';
import PropTypes from 'prop-types';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputLabel from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core/styles';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

const styles = () => ({
    iconButton: {
        marginLeft: 20,
        marginRight: -12,
    },
});

class PasswordField extends Component {
    static propTypes = {
        classes: PropTypes.shape({ iconButton: PropTypes.string }).isRequired,
        label: PropTypes.string,
        helperText: PropTypes.string,
        required: PropTypes.bool,
        error: PropTypes.bool,
        className: PropTypes.string,
        id: PropTypes.string,
    }

    static defaultProps = {
        label: 'Password',
        helperText: '',
        required: false,
        error: false,
        className: '',
        id: 'password',
    }

    constructor(props) {
        super(props);

        this.state = { showPassword: false };
    }

    toggleVisibility = () => {
        const { showPassword } = this.state;

        this.setState({ showPassword: !showPassword });
    };

    render() {
        const {
            label, helperText, required, error, className, classes, ...props
        } = this.props;
        const { showPassword } = this.state;

        return (
            <FormControl
                required={required}
                error={Boolean(error)}
                className={className}
            >
                <InputLabel htmlFor={props.id}>{label}</InputLabel>
                <Input
                    {...props}
                    type={showPassword ? 'text' : 'password'}
                    autoComplete="new-password"
                    endAdornment={(
                        <InputAdornment position="end">
                            <IconButton
                                className={classes.iconButton}
                                onClick={this.toggleVisibility}
                            >
                                {showPassword ? (
                                    <VisibilityOffIcon />
                                ) : (
                                    <VisibilityIcon />
                                )}
                            </IconButton>
                        </InputAdornment>
                    )}
                />
                {helperText && <FormHelperText>{helperText}</FormHelperText>}
            </FormControl>
        );
    }
}

export default withStyles(styles)(PasswordField);
