import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import SkillListItem from './SkillListItem';

describe('SkillListItem', () => {
    const props = { label: 'Test', onDelete: jest.fn() };

    test('label is rendered', () => {
        render(<SkillListItem {...props} />);
        const labelElement = screen.getByText('Test');
        expect(labelElement).toBeInTheDocument();
    });

    test('delete callback works', () => {
        render(<SkillListItem {...props} />);
        const button = screen.getByRole('button');
        userEvent.click(button);
        expect(props.onDelete).toBeCalled();
    });
});
