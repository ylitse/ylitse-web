import React, { useState, useEffect } from 'react';
import { Typography } from '@material-ui/core';
import { requestData } from '../utils';

const MessageList = ({ messages, report, showIdentities }) => {
    const reported = showIdentities ? report.reportedUserName : 'Reported mentor';
    const reporter = showIdentities ? report.reporterUserName : 'Reporter';
    return (
        <div>
            {messages
        && messages[0]
        && messages.map(message => (
            <Typography variant="body1" key={message.id}>
                {message.sender_id === report.reportedUserId ? reported : reporter}:{' '}
                {message.content}
            </Typography>
        ))}
        </div>
    );
};

const ReportConversation = ({ showIdentities, report }) => {
    const [messages, setMessages] = useState([]);
    const [hasError, setHasError] = useState(false);
    const [error, setError] = useState('');

    useEffect(() => {
        const fetchMessages = async () => {
            try {
                const response = await requestData(
                    `/api/users/${report.reportedUserId}/messages_for_admin?&contact_user_ids=${report.reporterUserId}&desc=true`,
                    'GET',
                );
                setMessages(response.resources);
            } catch (e) {
                console.error(e);
                setHasError(true);
                setError(e);
            }
        };
        fetchMessages();
    }, []);

    if (hasError) {
        if (error.toString().includes('Quota exceeded')) {
            return <div style={{ display: 'flex', flexDirection: 'column', height: '80vh', color: 'red' }}>Error: Quota exceeded</div>;
        }
        return <div style={{ display: 'flex', flexDirection: 'column', height: '80vh', color: 'red' }}>Error: Something went wrong</div>;
    }

    return (
        <div style={{ display: 'flex', flexDirection: 'column', height: '80vh' }}>
            <Typography variant="h6" gutterBottom>
                Reported conversation
            </Typography>
            <MessageList
                messages={messages}
                report={report}
                showIdentities={showIdentities}
            />
        </div>
    );
};

export default ReportConversation;
