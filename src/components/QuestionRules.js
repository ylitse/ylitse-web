import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormGroup, Typography } from '@material-ui/core';
import RadioGroupControl from './RadioGroupControl';
import QuestionValue from './QuestionValue';
import Localizable from './Localizable';
import { selectSelectedQuestionRules } from '../reducers';
import { QuestionsActionTypes } from '../actions';
import ChipSelect from './ChipSelect';

const defaultRules = {
    'yes-no': {
        yes: { value: 1, labels: { en: '', fi: '' } },
        no: { value: 0, labels: { en: '', fi: '' } },
        type: 'yes-no',
    },
    range: {
        min: { value: 0, labels: { en: '', fi: '' } },
        max: { value: 100, labels: { en: '', fi: '' } },
        type: 'range',
        step: 1,
    },
};

const ruleFields = {
    'yes-no': {
        yes: { label: 'yes value', value: 'yes' },
        no: { label: 'no value', value: 'no' },
    },
    range: {
        max: { label: 'maximum value', value: 'max' },
        min: { label: 'minimum value', value: 'min' },
    },
};

const QuestionRules = ({ classes }) => {
    const dispatch = useDispatch();

    const { answerRules, recipients, titles, isExisting } = useSelector(
        selectSelectedQuestionRules,
    );

    const [recipientsInputValue, setRecipientsInputValue] = React.useState('');

    const setRules = (key, updatedRules) => {
        dispatch({
            type: QuestionsActionTypes.SET_QUESTION_RULES,
            key,
            value: updatedRules,
        });
    };

    const handleRuleChange = (rule, key, value) => {
        const updatedAnswerRules = {
            ...answerRules,
            [rule]: { ...answerRules[rule], [key]: value },
        };

        setRules('answer', updatedAnswerRules);
    };

    const handleTitlesChange = (localizedTexts) => {
        setRules('titles', localizedTexts);
    };

    const removeRecipient = (recipient) => {
        const nextRecipients = recipients.filter(r => r !== recipient);
        setRules('recipients', nextRecipients);
    };

    const addRecipient = (recipient) => {
        const nextRecipients = recipients.concat(recipient);
        setRules('recipients', nextRecipients);
    };

    const handleRecipientsChange = (nextRecipient) => {
        if (recipients.includes(nextRecipient)) {
            removeRecipient(nextRecipient);
        } else {
            addRecipient(nextRecipient);
        }
    };

    const handleRecipientsInputValueChange = (inputVal) => {
        const t = inputVal.split(',');

        if (JSON.stringify(t) !== JSON.stringify(recipients)) {
            setRecipientsInputValue(inputVal);
        }
    };

    const ruleType = answerRules?.type ?? 'range';

    return (
        <div style={{ display: 'flex', flexDirection: 'column' }}>
            <FormGroup className={classes.formControl}>
                <Typography variant="h5" gutterBottom>
                    Recipients
                </Typography>
                <ChipSelect
                    itemName="recipients"
                    inputValue={recipientsInputValue}
                    selectedItem={recipients}
                    availableItems={[
                        { id: 0, name: 'mentee' },
                        { id: 1, name: 'mentor' },
                    ]}
                    onChange={handleRecipientsChange}
                    onRemoveItem={removeRecipient}
                    onInputValueChange={handleRecipientsInputValueChange}
                    disabled={isExisting}
                />
                <Typography variant="h5" gutterBottom>
                    Question
                </Typography>
                <Localizable
                    name="titles"
                    values={titles}
                    onLabelsChange={handleTitlesChange}
                    isDisabled={isExisting}
                    classes={classes}
                />
            </FormGroup>
            <Typography variant="h5" gutterBottom>
                Type
            </Typography>
            <RadioGroupControl
                name="questionType"
                label=""
                value={answerRules?.type ?? 'yes-no'}
                options={['yes-no', 'range']}
                onChange={event => setRules('answer', defaultRules[event.target.value])
                }
                disabled={isExisting}
            />
            {Object.keys(ruleFields[ruleType]).map(field => (
                <QuestionValue
                    key={field}
                    name={field}
                    label={ruleFields[ruleType][field].label}
                    values={answerRules?.[field]}
                    onChange={event => handleRuleChange(field, 'value', Number(event.target.value))
                    }
                    onLabelsChange={updatedLabels => handleRuleChange(field, 'labels', updatedLabels)
                    }
                    isDisabled={isExisting}
                    classes={classes}
                />
            ))}
        </div>
    );
};

export default QuestionRules;
