import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import AboutDialog from './AboutDialog';

const styles = () => ({
    flex: {
        flex: 1,
    },
    iconButton: {
        marginLeft: 20,
        marginRight: -24,
    },
});

class TopBar extends Component {
  static propTypes = {
      username: PropTypes.string.isRequired,
      toolbarLinks: PropTypes.arrayOf(
          PropTypes.shape({
              label: PropTypes.string,
              to: PropTypes.string,
          }),
      ).isRequired,
      version: PropTypes.shape({
          ui: PropTypes.string,
          api: PropTypes.string,
      }).isRequired,
      onLogoutClick: PropTypes.func.isRequired,
      classes: PropTypes.shape({
          flex: PropTypes.string,
          infoButton: PropTypes.string,
      }).isRequired,
  }

  constructor(props) {
      super(props);

      this.state = {
          aboutOpen: false,
          menuAnchor: null,
      };
  }

  openAbout = () => {
      this.setState({ aboutOpen: true, menuAnchor: null });
  }

  closeAbout = () => {
      this.setState({ aboutOpen: false });
  }

  openMenu = (event) => {
      this.setState({ menuAnchor: event.currentTarget });
  };

  closeMenu = () => {
      this.setState({ menuAnchor: null });
  };

  render() {
      const {
          toolbarLinks, username, version, classes, onLogoutClick,
      } = this.props;
      const { aboutOpen, menuAnchor } = this.state;

      return (
          <AppBar position="static">
              <Toolbar display="flex">
                  <Typography
                      variant="h6"
                      color="inherit"
                      className={classes.flex}
                  >
                      Ylitse Admin
                  </Typography>
                  {toolbarLinks.map(link => (
                      <Button
                          key={link.label}
                          component={Link}
                          color="inherit"
                          to={link.to}
                      >
                          {link.label}
                      </Button>
                  ))}
                  <IconButton
                      name="profile"
                      aria-label="profile"
                      component={Link}
                      to="/profile"
                      color="inherit"
                      className={classes.iconButton}
                  >
                      <AccountCircleIcon />
                  </IconButton>
                  <IconButton
                      name="menu"
                      aria-label="menu"
                      aria-owns={menuAnchor ? 'menu-appbar' : null}
                      aria-haspopup="true"
                      color="inherit"
                      className={classes.iconButton}
                      onClick={this.openMenu}
                  >
                      <MoreVertIcon />
                  </IconButton>
              </Toolbar>
              <Menu
                  open={Boolean(menuAnchor)}
                  id="menu-appbar"
                  anchorEl={menuAnchor}
                  onClose={this.closeMenu}
              >
                  <MenuItem aria-label="about" name="about" onClick={this.openAbout}>
                      About...
                  </MenuItem>
                  <MenuItem aria-label="logout" name="logout" onClick={onLogoutClick}>
                      {`Sign out ${username}`}
                  </MenuItem>
              </Menu>
              <AboutDialog
                  open={aboutOpen}
                  uiVersion={version.ui}
                  apiVersion={version.api}
                  onOkClick={this.closeAbout}
                  onClose={this.closeAbout}
              />
          </AppBar>
      );
  }
}

export default withStyles(styles)(TopBar);
