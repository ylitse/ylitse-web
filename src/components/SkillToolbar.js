import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Toolbar from '@material-ui/core/Toolbar';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
    toolbar: {
        paddingLeft: 0,
    },
    input: {
        marginTop: theme.spacing(1),
        marginRight: theme.spacing(1),
        marginBottom: theme.spacing(3),
    },
});

const SkillToolbar = ({
    disabled, classes, addValue, onAddChange, onAddKeyDown, addError,
}) => (
    <Toolbar className={classes.toolbar}>
        <TextField
            name="newSkillName"
            label="Add a skill"
            value={addValue}
            disabled={disabled}
            className={classes.input}
            error={Boolean(addError)}
            helperText={addError}
            onChange={onAddChange}
            onKeyDown={onAddKeyDown}
        />
    </Toolbar>
);

SkillToolbar.propTypes = {
    disabled: PropTypes.bool,
    addValue: PropTypes.string.isRequired,
    addError: PropTypes.string,
    onAddChange: PropTypes.func.isRequired,
    onAddKeyDown: PropTypes.func.isRequired,
    classes: PropTypes.shape({
        toolbar: PropTypes.string,
        input: PropTypes.string,
    }).isRequired,
};

SkillToolbar.defaultProps = {
    disabled: false,
    addError: '',
};

export default withStyles(styles)(SkillToolbar);
