import PropTypes from 'prop-types';

const localizable = PropTypes.shape({
    fi: PropTypes.string.isRequired,
    en: PropTypes.string.isRequired,
});

const value = PropTypes.shape({
    value: PropTypes.number.isRequired,
    labels: localizable.isRequired,
});

const yesNoRules = PropTypes.shape({
    no: value.isRequired,
    yes: value.isRequired,
    type: PropTypes.string.isRequired, // yes-no
});

const rangeRules = PropTypes.shape({
    max: value.isRequired,
    min: value.isRequired,
    step: PropTypes.number,
    type: PropTypes.string.isRequired, // range
});

const predefinedRepetition = PropTypes.shape({
    type: PropTypes.string.isRequired, // predefined
    times: PropTypes.number.isRequired,
    days_since_previous_answer: PropTypes.number.isRequired,
    messages_since_previous_answer: PropTypes.number.isRequired,
    min_days_since_previous_answer: PropTypes.number.isRequired,
});

const untilRepetition = PropTypes.shape({
    type: PropTypes.string.isRequired, // until_value
    value: PropTypes.number.isRequired,
    comparison: PropTypes.string.isRequired, // exact
    days_since_previous_answer: PropTypes.number.isRequired,
});

const schedule = PropTypes.shape({
    first: PropTypes.shape({
        days_since_registration: PropTypes.number.isRequired,
        sent_message_threshold: PropTypes.number,
        max_old_account_in_days: PropTypes.number,
    }).isRequired,
    repetitions: PropTypes.oneOfType([predefinedRepetition, untilRepetition])
        .isRequired,
    remind_times_when_skipped: PropTypes.number.isRequired,
    remind_interval_days: PropTypes.number.isRequired,
});

const rules = PropTypes.shape({
    answer: PropTypes.oneOfType([yesNoRules, rangeRules]).isRequired,
    recipients: PropTypes.arrayOf(PropTypes.string).isRequired,
    titles: localizable.isRequired,
    schedule: schedule.isRequired,
});

export const question = PropTypes.shape({
    id: PropTypes.string.isRequired,
    active: PropTypes.bool.isRequired,
    updated: PropTypes.string.isRequired,
    created: PropTypes.string.isRequired,
    rules: rules.isRequired,
});

export const defaultQuestion = {
    rules: {
        recipients: ['mentee'],
        answer: {
            yes: { value: 1, labels: { en: '', fi: '' } },
            no: { value: 0, labels: { en: '', fi: '' } },
            type: 'yes-no',
        },
        titles: { en: '', fi: '' },
        schedule: {
            first: {
                days_since_registration: 10,
                sent_messages_threshold: 10,
                max_old_account_in_days: 60,
            },
            repetitions: {
                type: 'predefined',
                times: 2,
                days_since_previous_answer: 30,
                messages_since_previous_answer: 50,
                min_days_since_previous_answer: 14,
            },
            remind_times_when_skipped: 3,
            remind_interval_days: 1,
        },
    },
};

export default question;
