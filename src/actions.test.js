import thunk from 'redux-thunk';
import configureStore from 'redux-mock-store';
import { updateKeys, camelify, combineToAccount } from './utils';

import {
    FeedbackActionTypes, DialogActionTypes, VersionActionTypes,
    SkillsActionTypes, AccountsActionTypes, StatsActionTypes, enqueueFeedback,
    dequeueFeedback, fetchVersion, fetchSkills, createSkill, deleteSkill,
    fetchAccounts, createAccount, updateAccount, deleteAccount, addAccount,
    editAccount, fetchStats, closeDialog,
} from './actions';
import { DialogTypes } from './constants';

const mockStore = configureStore([thunk]);

function mockFetch(status, json) {
    const res = new window.Response(JSON.stringify(json), {
        status,
        headers: { 'Content-Type': 'application/json' },
    });

    window.fetch = jest.fn().mockImplementation(() => Promise.resolve(res));
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function mockManyFetch(mapping) {
    window.fetch = jest.fn().mockImplementation(endpoint => Promise.resolve(
        new window.Response(JSON.stringify(mapping[endpoint].json), {
            status: mapping[endpoint].status,
            headers: { 'Content-Type': 'application/json' },
        }),
    ));
}

describe('actions', () => {
    test('can create feedback actions', () => {
        const message = 'Test message';

        expect(enqueueFeedback(message)).toEqual({
            type: FeedbackActionTypes.ENQUEUE,
            message,
        });
        expect(dequeueFeedback()).toEqual({
            type: FeedbackActionTypes.DEQUEUE,
        });
    });

    test('can create dialog actions', () => {
        const account = { username: 'test' };

        expect(addAccount()).toEqual({
            type: DialogActionTypes.OPEN,
            dialogType: DialogTypes.ADD_ACCOUNT,
            dialogProps: {},
        });
        expect(editAccount(account)).toEqual({
            type: DialogActionTypes.OPEN,
            dialogType: DialogTypes.EDIT_ACCOUNT,
            dialogProps: { account },
        });
        expect(closeDialog()).toEqual({ type: DialogActionTypes.CLOSE });
    });

    test('can fetch version', async () => {
        const store = mockStore({ version: { api: '' } });

        mockFetch(200, { api: '0.1' });
        await store.dispatch(fetchVersion());
        expect(store.getActions()).toEqual([{
            type: VersionActionTypes.REQUEST,
        }, {
            type: VersionActionTypes.SUCCESS,
            version: { api: '0.1' },
        }]);
    });

    test('version fetch error is handled', async () => {
        const store = mockStore({ version: { api: '' } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(fetchVersion());
        expect(store.getActions()).toEqual([{
            type: VersionActionTypes.REQUEST,
        }, {
            type: VersionActionTypes.FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-fetch version', () => {
        window.fetch = jest.fn();

        let store = mockStore({ version: { api: '0.1' } });
        store.dispatch(fetchVersion());
        expect(window.fetch).not.toBeCalled();

        store = mockStore({ version: { isFetching: true } });
        store.dispatch(fetchVersion());
        expect(window.fetch).not.toBeCalled();
    });

    test('can fetch skills', async () => {
        const store = mockStore({ skills: {} });

        mockFetch(200, { resources: [{ name: 'A' }, { name: 'B' }] });
        await store.dispatch(fetchSkills());
        expect(store.getActions()).toEqual([{
            type: SkillsActionTypes.FETCH_REQUEST,
        }, {
            type: SkillsActionTypes.FETCH_SUCCESS,
            skills: [{ name: 'A' }, { name: 'B' }],
        }]);
    });

    test('skill fetch error is handled', async () => {
        const store = mockStore({ skills: { isFetching: false } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(fetchSkills());
        expect(store.getActions()).toEqual([{
            type: SkillsActionTypes.FETCH_REQUEST,
        }, {
            type: SkillsActionTypes.FETCH_FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-fetch skills', () => {
        window.fetch = jest.fn();

        const store = mockStore({ skills: { isFetching: true } });
        store.dispatch(fetchSkills());
        expect(window.fetch).not.toBeCalled();
    });

    test('can create a skill', async () => {
        const store = mockStore({ skills: {} });

        mockFetch(201, { name: 'A' });
        await store.dispatch(createSkill('A'));
        expect(store.getActions()).toEqual([{
            type: SkillsActionTypes.CREATE_REQUEST,
        }, {
            type: SkillsActionTypes.CREATE_SUCCESS,
            skill: { name: 'A' },
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'A skill created',
        }]);
    });

    test('skill create error is handled', async () => {
        const store = mockStore({ skills: { isCreating: false } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(createSkill('A'));
        expect(store.getActions()).toEqual([{
            type: SkillsActionTypes.CREATE_REQUEST,
        }, {
            type: SkillsActionTypes.CREATE_FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-create a skill', () => {
        window.fetch = jest.fn();

        const store = mockStore({ skills: { isCreating: true } });
        store.dispatch(createSkill('A'));
        expect(window.fetch).not.toBeCalled();
    });

    test('can delete a skill', async () => {
        const store = mockStore({ skills: {} });

        mockFetch(200, {});
        await store.dispatch(deleteSkill({ id: '1', name: 'A' }));
        expect(store.getActions()).toEqual([{
            type: SkillsActionTypes.DELETE_REQUEST,
        }, {
            type: SkillsActionTypes.DELETE_SUCCESS,
            id: '1',
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'A skill deleted',
        }]);
    });

    test('skill delete error is handled', async () => {
        const store = mockStore({ skills: { isDeleting: false } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(deleteSkill({ id: '1', name: 'A' }));
        expect(store.getActions()).toEqual([{
            type: SkillsActionTypes.DELETE_REQUEST,
        }, {
            type: SkillsActionTypes.DELETE_FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-delete a skill', () => {
        window.fetch = jest.fn();

        const store = mockStore({ skills: { isDeleting: true } });
        store.dispatch(deleteSkill({ id: '1', name: 'A' }));
        expect(window.fetch).not.toBeCalled();
    });

    test('can fetch accounts', async () => {
        const store = mockStore({ accounts: {} });

        const { account, user, mentor } = {
            account: {
                role: 'mentor',
                loginName: 'janteri',
                email: 'janteri@mail.fi',
                phone: '1234567890',

                id: 'wtm0sF2m92L_PljrG71GM2RU4CDcOUxrlovVpqCnXf0',
                created: '2018-08-03T11:34:58.168290',
                updated: '2018-08-03T11:34:58.182284',
                active: true,
            },
            user: {
                displayName: 'maikkeli',
                role: 'mentor',
                accountId: 'wtm0sF2m92L_PljrG71GM2RU4CDcOUxrlovVpqCnXf0',
                id: 'msOx_d8XmUncJ6sC2szkehTNvdCDc5hV-dAnWqGjS0c',
                created: '2018-08-03T11:34:58.182681',
                updated: '2018-08-03T11:34:58.183154',
                active: true,
            },
            mentor: {
                birthYear: 1990,
                communicationChannels: ['chat'],
                displayName: 'maikkeli',
                gender: 'male',
                languages: ['fi', 'sv'],
                region: 'Helsinki',
                skills: ['vim', 'js'],
                story: 'Cool story.',
                accountId: 'wtm0sF2m92L_PljrG71GM2RU4CDcOUxrlovVpqCnXf0',
                userId: 'msOx_d8XmUncJ6sC2szkehTNvdCDc5hV-dAnWqGjS0c',
                id: 'cRyIdqEc7LMl4ceMZItdLOKwBNF1uFdjZ9r_At2nmg8',
                created: '2018-08-03T11:34:58.183535',
                updated: '2018-08-03T11:34:58.184336',
                active: true,
            },
        };

        const combined = {
            id: account.id,

            role: 'mentor',
            username: 'janteri',
            nickname: 'maikkeli',
            email: 'janteri@mail.fi',
            phone: '1234567890',
            gender: 'male',
            birthYear: 1990,
            area: 'Helsinki',
            languages: ['fi', 'sv'],
            skills: ['vim', 'js'],
            communicationChannels: ['chat'],
            story: 'Cool story.',

            account,
            user,
            mentor,
        };

        const accounts = { resources: [account] };
        const users = { resources: [user] };
        const mentors = { resources: [mentor] };

        mockManyFetch({
            '/api/accounts': { status: 200, json: accounts },
            '/api/users': { status: 200, json: users },
            '/api/mentors': { status: 200, json: mentors },
        });

        await store.dispatch(fetchAccounts());
        await sleep(10);
        expect(store.getActions()).toEqual([
            { type: AccountsActionTypes.FETCH_REQUEST },
            { type: AccountsActionTypes.FETCH_REQUEST },
            { type: AccountsActionTypes.FETCH_REQUEST },
            {
                type: AccountsActionTypes.FETCH_SUCCESS,
                accounts: [combined],
            }]);
    });

    test('account fetch error is handled', async () => {
        const store = mockStore({ accounts: { isFetching: false } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(fetchAccounts());
        expect(store.getActions()).toEqual([{
            type: AccountsActionTypes.FETCH_REQUEST,
        }, {
            type: AccountsActionTypes.FETCH_FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-fetch accounts', () => {
        window.fetch = jest.fn();

        const store = mockStore({ accounts: { isFetching: true } });
        store.dispatch(fetchAccounts());
        expect(window.fetch).not.toBeCalled();
    });

    test('can create a account', async () => {
        const store = mockStore({ accounts: {} });

        const accountsResp = {
            account: {
                role: 'mentor',
                login_name: 'janteri',
                email: 'janteri@mail.fi',
                phone: '1234567890',

                id: 'wtm0sF2m92L_PljrG71GM2RU4CDcOUxrlovVpqCnXf0',
                created: '2018-08-03T11:34:58.168290',
                updated: '2018-08-03T11:34:58.182284',
                active: true,
            },
            user: {
                display_name: 'janteri',
                role: 'mentor',
                account_id: 'wtm0sF2m92L_PljrG71GM2RU4CDcOUxrlovVpqCnXf0',

                id: 'msOx_d8XmUncJ6sC2szkehTNvdCDc5hV-dAnWqGjS0c',
                created: '2018-08-03T11:34:58.182681',
                updated: '2018-08-03T11:34:58.183154',
                active: true,
            },
            mentor: {
                birth_year: 1234,
                communication_channels: ['asdfasdf'],
                display_name: 'asdfasdf',

                gender: 'adsfasdf',
                languages: ['asdf'],
                region: 'adsf',

                skills: ['vim', 'js'],
                story: '',

                account_id: 'wtm0sF2m92L_PljrG71GM2RU4CDcOUxrlovVpqCnXf0',
                user_id: 'msOx_d8XmUncJ6sC2szkehTNvdCDc5hV-dAnWqGjS0c',
                id: 'cRyIdqEc7LMl4ceMZItdLOKwBNF1uFdjZ9r_At2nmg8',

                created: '2018-08-03T11:34:58.183535',
                updated: '2018-08-03T11:34:58.184336',
                active: true,
            },
        };

        const userFromAPI = {
            displayName: 'maikkeli',
            role: 'mentor',
            accountId: 'wtm0sF2m92L_PljrG71GM2RU4CDcOUxrlovVpqCnXf0',
            id: 'msOx_d8XmUncJ6sC2szkehTNvdCDc5hV-dAnWqGjS0c',
            created: '2018-08-03T11:34:58.182681',
            updated: '2018-08-03T11:34:58.183154',
            active: true,
        };

        const mentorFromAPI = {
            birthYear: 1990,
            communicationChannels: ['chat'],
            display_name: 'maikkeli',
            gender: 'male',
            languages: ['fi', 'sv'],
            region: 'Helsinki',
            skills: ['vim', 'js'],
            story: 'Cool story.',
            accountId: 'wtm0sF2m92L_PljrG71GM2RU4CDcOUxrlovVpqCnXf0',
            userId: 'msOx_d8XmUncJ6sC2szkehTNvdCDc5hV-dAnWqGjS0c',
            id: 'cRyIdqEc7LMl4ceMZItdLOKwBNF1uFdjZ9r_At2nmg8',
            created: '2018-08-03T11:34:58.183535',
            updated: '2018-08-03T11:34:58.184336',
            active: true,
        };

        mockManyFetch({
            '/api/accounts': { status: 201, json: accountsResp },
            '/api/users/msOx_d8XmUncJ6sC2szkehTNvdCDc5hV-dAnWqGjS0c': {
                status: 201, json: userFromAPI,
            },
            '/api/mentors/cRyIdqEc7LMl4ceMZItdLOKwBNF1uFdjZ9r_At2nmg8': {
                status: 201, json: mentorFromAPI,
            },
        });

        const formState = {
            role: 'mentor',
            username: 'janteri',
            password: 'asdf',
            nickname: 'maikkeli',
            email: 'janteri@mail.fi',
            phone: '12345678090',
            gender: 'male',
            birthYear: 1990,
            area: 'Helsinki',
            languages: ['fi', 'sv'],
            skills: ['vim', 'js'],
            communicationChannels: ['chat'],
            story: 'Cool story.',
        };

        await store.dispatch(createAccount(formState));
        expect(store.getActions()).toEqual([
            { type: AccountsActionTypes.CREATE_REQUEST },
            { type: AccountsActionTypes.CREATE_REQUEST },
            { type: AccountsActionTypes.CREATE_REQUEST },
            {
                type: AccountsActionTypes.CREATE_SUCCESS,
                account: {
                    id: accountsResp.account.id,

                    area: 'Helsinki',
                    birthYear: 1990,
                    communicationChannels: ['chat'],

                    email: 'janteri@mail.fi',
                    gender: 'male',
                    nickname: 'maikkeli',

                    languages: ['fi', 'sv'],
                    phone: '1234567890',
                    role: 'mentor',

                    skills: ['vim', 'js'],
                    story: 'Cool story.',
                    username: 'janteri',

                    account: updateKeys(accountsResp.account, camelify),
                    user: updateKeys(userFromAPI, camelify),
                    mentor: updateKeys(mentorFromAPI, camelify),
                },
            },
            {
                type: FeedbackActionTypes.ENQUEUE,
                message: 'janteri\'s account created',
            },
        ]);
    });

    test('account create error is handled', async () => {
        const store = mockStore({ accounts: { isCreating: false } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(createAccount({ username: 'A' }));
        expect(store.getActions()).toEqual([{
            type: AccountsActionTypes.CREATE_REQUEST,
        }, {
            type: AccountsActionTypes.CREATE_FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-create a account', () => {
        window.fetch = jest.fn();

        const store = mockStore({ accounts: { isCreating: true } });
        store.dispatch(createAccount({ username: 'A' }));
        expect(window.fetch).not.toBeCalled();
    });

    test('can update a account', async () => {
        const { oldAccount, oldUser, oldMentor } = {
            oldAccount: {
                role: 'mentor',
                loginName: 'janteri',
                email: 'janteri@mail.fi',
                phone: '1234567890',

                id: 'wtm0sF2m92L_PljrG71GM2RU4CDcOUxrlovVpqCnXf0',
                created: '2018-08-03T11:34:58.168290',
                updated: '2018-08-03T11:34:58.182284',
                active: true,
            },
            oldUser: {
                displayName: 'maikkeli',
                role: 'mentor',
                accountId: 'wtm0sF2m92L_PljrG71GM2RU4CDcOUxrlovVpqCnXf0',
                id: 'msOx_d8XmUncJ6sC2szkehTNvdCDc5hV-dAnWqGjS0c',
                created: '2018-08-03T11:34:58.182681',
                updated: '2018-08-03T11:34:58.183154',
                active: true,
            },
            oldMentor: {
                birthYear: 1990,
                communicationChannels: ['chat'],
                displayName: 'maikkeli',
                gender: 'male',
                languages: ['fi', 'sv'],
                region: 'Helsinki',
                skills: ['vim', 'js'],
                story: 'Cool story.',
                accountId: 'wtm0sF2m92L_PljrG71GM2RU4CDcOUxrlovVpqCnXf0',
                userId: 'msOx_d8XmUncJ6sC2szkehTNvdCDc5hV-dAnWqGjS0c',
                id: 'cRyIdqEc7LMl4ceMZItdLOKwBNF1uFdjZ9r_At2nmg8',
                created: '2018-08-03T11:34:58.183535',
                updated: '2018-08-03T11:34:58.184336',
                active: true,
            },
        };

        const { newAccount, newUser, newMentor } = {
            newAccount: {
                ...oldAccount,
                phone: '0987654321',
            },
            newUser: {
                ...oldUser,
            },
            newMentor: {
                ...oldMentor,
                languages: ['fi', 'sv', 'en'],
                story: 'Very new story',
            },
        };

        const store = mockStore({
            accounts: { items: [
                combineToAccount(oldAccount, oldUser, oldMentor),
            ] },
        });

        mockManyFetch({
            '/api/accounts/wtm0sF2m92L_PljrG71GM2RU4CDcOUxrlovVpqCnXf0': {
                status: 200, json: newAccount,
            },
            '/api/users/msOx_d8XmUncJ6sC2szkehTNvdCDc5hV-dAnWqGjS0c': {
                status: 200, json: newUser,
            },
            '/api/mentors/cRyIdqEc7LMl4ceMZItdLOKwBNF1uFdjZ9r_At2nmg8': {
                status: 200, json: newMentor,
            },
        });

        await store.dispatch(
            updateAccount(combineToAccount(newAccount, newUser, newMentor)),
        );

        expect(store.getActions()).toEqual([
            { type: AccountsActionTypes.UPDATE_REQUEST },
            { type: AccountsActionTypes.UPDATE_REQUEST },
            { type: AccountsActionTypes.UPDATE_REQUEST },
            {
                type: AccountsActionTypes.UPDATE_SUCCESS,
                account: combineToAccount(newAccount, newUser, newMentor),
            },
            {
                type: FeedbackActionTypes.ENQUEUE,
                message: 'janteri\'s account updated',
            },
        ]);
    });

    test('account update error is handled', async () => {
        const store = mockStore({ accounts: { isCreating: false } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(updateAccount({ id: '1', username: 'A' }));
        expect(store.getActions()).toEqual([{
            type: AccountsActionTypes.UPDATE_REQUEST,
        }, {
            type: AccountsActionTypes.UPDATE_FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-update a account', () => {
        window.fetch = jest.fn();

        const store = mockStore({ accounts: { isUpdating: true } });
        store.dispatch(updateAccount({ id: '1', username: 'A' }));
        expect(window.fetch).not.toBeCalled();
    });

    test('can delete a account', async () => {
        const store = mockStore({ accounts: {} });

        mockFetch(200, {});
        await store.dispatch(deleteAccount({ id: '1', username: 'A' }));
        expect(store.getActions()).toEqual([{
            type: AccountsActionTypes.DELETE_REQUEST,
        }, {
            type: AccountsActionTypes.DELETE_SUCCESS,
            id: '1',
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'A\'s account deleted',
        }]);
    });

    test('account delete error is handled', async () => {
        const store = mockStore({ accounts: { isCreating: false } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(deleteAccount({ id: '1', username: 'A' }));
        expect(store.getActions()).toEqual([{
            type: AccountsActionTypes.DELETE_REQUEST,
        }, {
            type: AccountsActionTypes.DELETE_FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });

    test('doesn\'t re-delete a account', () => {
        window.fetch = jest.fn();

        const store = mockStore({ accounts: { isDeleting: true } });
        store.dispatch(deleteAccount({ id: '1', username: 'A' }));
        expect(window.fetch).not.toBeCalled();
    });

    test('can fetch stats', async () => {
        const store = mockStore({ stats: {}, totals: {} });

        mockFetch(200, {
            stats: {
                0: { sent: 1, received: 1, banned: 0 },
                1: { sent: 1, received: 1, banned: 0 },
            },
            total_message_count: 2,
        });
        await store.dispatch(fetchStats());
        expect(store.getActions()).toEqual([{
            type: StatsActionTypes.REQUEST,
        }, {
            type: StatsActionTypes.SUCCESS,
            stats: {
                stats: {
                    0: { sent: 1, received: 1, banned: 0 },
                    1: { sent: 1, received: 1, banned: 0 },
                },
                totalMessageCount: 2,
            },
        }]);
    });

    test('stats fetch error is handled', async () => {
        const store = mockStore({ stats: { isFetching: false } });

        mockFetch(400, { message: 'Error' });
        await store.dispatch(fetchStats());
        expect(store.getActions()).toEqual([{
            type: StatsActionTypes.REQUEST,
        }, {
            type: StatsActionTypes.FAILURE,
        }, {
            type: FeedbackActionTypes.ENQUEUE,
            message: 'Error',
        }]);
    });
});
