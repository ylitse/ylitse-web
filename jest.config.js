const config = {
    verbose: false,
    collectCoverage: true,
    collectCoverageFrom: ['src/**/*.js'],
    coverageReporters: ['text', 'html'],
    setupFilesAfterEnv: ['./setupTests.js'],
    testPathIgnorePatterns: [
        '/node_modules/',
        '/cypress/',
    ],
    moduleNameMapper: {
        '\\.(css|less|scss|sass)$': 'identity-obj-proxy',
    },
};

module.exports = config;
