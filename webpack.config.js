const webpack = require('webpack');

const redirect = (res, loc) => {
    res.statusCode = 302;
    res.statusMessage = 'Found';
    res.headers.location = loc;
};

const checkAuth = (res, req) => {
    if (req.url !== '/login' && [401].includes(res.statusCode)) {
        // not logged in, redirect to login page
        redirect(res, '/login');
    }
};

module.exports = (env, argv) => {
    const commonConf = {
        output: {
            filename: 'bundle.js',
            sourceMapFilename: 'bundle.map',
        },
        module: {
            rules: [{
                test: /\.js$/,
                use: 'babel-loader',
                exclude: [/node_modules/],
            }, {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            }, {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                use: 'file-loader?name=fonts/[name].[ext]',
            }, {
                test: /\.(jpg|png|gif|svg)$/,
                use: 'file-loader?name=images/[name].[ext]',
                include: [/images/],
            }],
        },
        plugins: [new webpack.DefinePlugin({
            'process.env': {
              REV: JSON.stringify(process.env.REV),
              BASE_PATH: JSON.stringify(process.env.YLITSE_BASE_PATH),
            },
        })],
    };

    const devConf = {
        devServer: {
            port: 3000,
            publicPath: '/',
            contentBase: './src',
            hot: true,
            stats: 'minimal',
            historyApiFallback: true,
            proxy: {
                '/api/**': {
                    target: process.env.DEV_API,
                    pathRewrite: { '^/api': '' },
                    cookieDomainRewrite: JSON.stringify(process.env.DEV_API),
                    changeOrigin: true,
                    logLevel: 'debug',
                    onProxyRes: checkAuth,
                },
            },
        },
        devtool: 'source-map',
        plugins: [new webpack.HotModuleReplacementPlugin()],
        optimization: {
            noEmitOnErrors: true,
        },
    };

    if (argv.mode === 'development') {
        return {
            ...commonConf,
            ...devConf,
            plugins: [
                ...commonConf.plugins,
                ...devConf.plugins,
            ],
        };
    }

    return commonConf;
};
