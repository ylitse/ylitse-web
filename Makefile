PATH:=${PATH}:node_modules/.bin
DEV_API=http://localhost:8080
REV=${shell git rev-parse --short HEAD}
SHELL:=env PATH=${PATH} /bin/sh

all: run

version:
	@echo ${shell grep '"version":' package.json | cut -d\" -f4}

install:
	npm ci

list-installed:
	npm list --depth 0

lint:
	eslint "{src,cypress}/**/*.js"

unittest:
	jest
	@echo "Full report in browser: file://${PWD}/coverage/index.html"

unittest-update:
	jest --updateSnapshot

unittest-watch:
	jest --watch

unittest-debug:
	jest --bail --runInBand

e2e:
	cypress run

test: unittest lint

run-dev:
	DEV_API=${DEV_API} REV=${REV} webpack-dev-server --mode development

run: clean test run-dev

dist: clean test
	REV=${REV} webpack --mode production
	cp src/index.html dist/
	cp -r src/images dist/
	cp -r src/login dist/

clean:
	find . -type f -name '*~' -exec rm -f {} \;
	rm -rf dist coverage src/bundle.*
	jest --clearCache

.EXPORT_ALL_VARIABLES:

.PHONY: all version install list-installed lint unittest unittest-update unittest-watch unittest-debug e2e test run-dev run dist clean
