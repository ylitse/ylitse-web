/* eslint-disable no-restricted-syntax */
/* global cy, context */
// / <reference types="cypress" />

let API_USER;
let API_PASS;

const loginAdmin = () => {
    cy.get('input[name="username"]').type(API_USER);

    cy.get('input[name="password"]').type(API_PASS);

    cy.get('input[type="submit"]').click();
};

context('Admin', () => {
    beforeEach(() => {
        cy.task('getApiUser').then((val) => {
            API_USER = val;
        });
        cy.task('getApiPass').then((val) => {
            API_PASS = val;
        });

        cy.task('deleteAccounts');
        cy.task('createInitialSkills');
        cy.visit('http://localhost:3000');
    });

    it('can login', () => {
        cy.get('input[name="username"]').type(API_USER);

        cy.get('input[name="password"]').type(API_PASS);

        cy.get('input[type="submit"]').click();

        cy.get('h3').should('have.text', 'User accounts');
    });

    it('can create mentor', () => {
        loginAdmin();

        cy.fixture('accounts.json').then((accounts) => {
            const mentor = accounts.mentors[0];

            cy.get('button').contains('Add new').click();

            cy.get('div[role="dialog"]').within(() => {
                cy.get('input[value="mentor"]').click();
                cy.get('input[name="username"]').type(mentor.loginName);
                cy.get('input[name="password"]').type(mentor.password);
                cy.get('input[name="nickname"]').type(mentor.displayName);
                cy.get('input[name="email"]').type(mentor.email);

                cy.get('button').contains('Mentor profile').click();

                cy.get('input[name="phone"]').type(mentor.phone);
                cy.get(`input[value="${mentor.gender}"]`).click();
                cy.get('input[name="birthYear"]').type(mentor.birthYear);
                cy.get('input[name="area"]').type(mentor.region);
                for (const lang of mentor.languages) {
                    cy.get('input[placeholder="Add a language"]').type(lang);
                    cy.contains(lang).click();
                }
                for (const skill of mentor.skills) {
                    cy.get('input[placeholder="Add a skill"]').type(skill);
                    cy.contains(skill).click();
                }
                for (const channel of mentor.communication_channels) {
                    cy.get(`input[value="${channel}"]`).click();
                }

                cy.get('textarea[name="story"]').type(mentor.story);

                cy.get('button').contains('Create').click();
            });

            cy.get('h6')
                .contains('Number of registered mentors')
                .should('have.text', 'Number of registered mentors: 1');

            cy.get('button[name="edit"]').last().click();

            cy.get('input[name="username"]').should('have.value', mentor.loginName);
            cy.get('input[name="nickname"]').should('have.value', mentor.displayName);
            cy.get('input[name="email"]').should('have.value', mentor.email);

            cy.get('button').contains('Mentor profile').click();

            cy.get('input[name="phone"]').should('have.value', mentor.phone);
            cy.get(`input[value="${mentor.gender}"]`).should('be.checked');
            cy.get('input[name="birthYear"]').should('have.value', mentor.birthYear);
            cy.get('input[name="area"]').should('have.value', mentor.region);
            for (const lang of mentor.languages) {
                cy.contains(lang);
            }
            for (const skill of mentor.skills) {
                cy.contains(skill);
            }
            for (const channel of mentor.communication_channels) {
                cy.get(`input[value="${channel}"]`).should('be.checked');
            }

            cy.get('textarea[name="story"]').contains(mentor.story);
        });
    });

    it('create mentor disabled if missing data', () => {
        loginAdmin();

        cy.fixture('accounts.json').then((accounts) => {
            const mentor = accounts.mentors[0];

            cy.get('button').contains('Add new').click();

            cy.get('div[role="dialog"]').within(() => {
                cy.get('input[value="mentor"]').click();
                cy.get('input[name="username"]').type(mentor.loginName);
                cy.get('input[name="password"]').type(mentor.password);
                cy.get('input[name="nickname"]').type(mentor.displayName);
                cy.get('input[name="email"]').type(mentor.email);

                cy.get('button').contains('Mentor profile').click();

                cy.get('input[name="phone"]').type(mentor.phone);
                cy.get(`input[value="${mentor.gender}"]`).click();
                cy.get('input[name="area"]').type(mentor.region);
                for (const lang of mentor.languages) {
                    cy.get('input[placeholder="Add a language"]').type(lang);
                    cy.contains(lang).click();
                }
                for (const skill of mentor.skills) {
                    cy.get('input[placeholder="Add a skill"]').type(skill);
                    cy.contains(skill).click();
                }
                for (const channel of mentor.communication_channels) {
                    cy.get(`input[value="${channel}"]`).click();
                }

                cy.get('textarea[name="story"]').type(mentor.story);

                cy.get('button[tabIndex=-1]').should('be.disabled');
            });
        });
    });

    it("can't open mentor-tab if mentor not selected", () => {
        loginAdmin();

        cy.fixture('accounts.json').then((accounts) => {
            const mentee = accounts.mentees[0];

            cy.get('button').contains('Add new').click();

            cy.get('div[role="dialog"]').within(() => {
                cy.get('input[value="mentee"]').click();
                cy.get('input[name="username"]').type(mentee.loginName);
                cy.get('input[name="password"]').type(mentee.password);
                cy.get('input[name="nickname"]').type(mentee.displayName);
                cy.get('input[name="email"]').type(mentee.email);

                cy.get('button[role="tab"][tabIndex=-1]').should('be.disabled');
            });
        });
    });

    it('can create mentee', () => {
        loginAdmin();

        cy.fixture('accounts.json').then((accounts) => {
            const mentee = accounts.mentees[0];

            cy.get('button').contains('Add new').click();

            cy.get('div[role="dialog"]').within(() => {
                cy.get('input[value="mentee"]').click();
                cy.get('input[name="username"]').type(mentee.loginName);
                cy.get('input[name="password"]').type(mentee.password);
                cy.get('input[name="nickname"]').type(mentee.displayName);
                cy.get('input[name="email"]').type(mentee.email);

                cy.get('button').contains('Create').click();
            });

            cy.get('h6')
                .contains('Number of registered mentees')
                .should('have.text', 'Number of registered mentees: 1');

            cy.get('button[name="edit"]').last().click();

            cy.get('input[name="username"]').should('have.value', mentee.loginName);
            cy.get('input[name="nickname"]').should('have.value', mentee.displayName);
            cy.get('input[name="email"]').should('have.value', mentee.email);
        });
    });

    it('can create admin', () => {
        loginAdmin();

        cy.fixture('accounts.json').then((accounts) => {
            const admin = accounts.admins[0];

            cy.get('button').contains('Add new').click();

            cy.get('div[role="dialog"]').within(() => {
                cy.get('input[value="admin"]').click();
                cy.get('input[name="username"]').type(admin.loginName);
                cy.get('input[name="password"]').type(admin.password);
                cy.get('input[name="nickname"]').type(admin.displayName);
                cy.get('input[name="email"]').type(admin.email);

                cy.get('button').contains('Create').click();
            });

            // Setting nickname for admin happens in another request,
            // without waiting, this test is flaky
            cy.wait(2000);

            cy.get('button[name="edit"]').last().click();

            cy.get('input[name="username"]').should('have.value', admin.loginName);
            cy.get('input[name="nickname"]').should('have.value', admin.displayName);
            cy.get('input[name="email"]').should('have.value', admin.email);
        });
    });

    it('can update vacation status for mentor', () => {
        loginAdmin();

        cy.fixture('accounts.json').then((accounts) => {
            const mentor = accounts.mentors[0];

            cy.get('button').contains('Add new').click();

            cy.get('div[role="dialog"]').within(() => {
                cy.get('input[value="mentor"]').click();
                cy.get('input[name="username"]').type(mentor.loginName);
                cy.get('input[name="password"]').type(mentor.password);
                cy.get('input[name="nickname"]').type(mentor.displayName);
                cy.get('input[name="email"]').type(mentor.email);

                cy.get('button').contains('Mentor profile').click();

                cy.get('input[name="phone"]').type(mentor.phone);
                cy.get(`input[value="${mentor.gender}"]`).click();
                cy.get('input[name="birthYear"]').type(mentor.birthYear);
                cy.get('input[name="area"]').type(mentor.region);
                for (const lang of mentor.languages) {
                    cy.get('input[placeholder="Add a language"]').type(lang);
                    cy.contains(lang).click();
                }
                for (const skill of mentor.skills) {
                    cy.get('input[placeholder="Add a skill"]').type(skill);
                    cy.contains(skill).click();
                }
                for (const channel of mentor.communication_channels) {
                    cy.get(`input[value="${channel}"]`).click();
                }

                cy.get('textarea[name="story"]').type(mentor.story);

                cy.get('button').contains('Create').click();
            });
            cy.wait(5000);
            cy.get('button[name="edit"]').last().click();
            cy.get('div[role="dialog"]').within(() => {
                cy.get('button').contains('Mentor profile').click();
                cy.get('input[name="vacation"]').click();
                cy.get('textarea[name="statusMessage"]').type(mentor.statusMessage);
                cy.get('button').contains('Save').click();
            });
        });
        cy.contains('Vacationing');
        cy.get('h6')
            .contains('Number of vacation status')
            .should('have.text', 'Number of vacation status: 1');
    });
});
