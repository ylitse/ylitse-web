/* eslint-disable no-restricted-syntax */
/* global cy, context */
// / <reference types="cypress" />

/* eslint-disable no-unused-vars */
let API_USER;
let API_PASS;
/* eslint-enable no-unused-vars */

const loginAdmin = () => {
    cy.get('input[name="username"]').type(API_USER);

    cy.get('input[name="password"]').type(API_PASS);

    cy.get('input[type="submit"]').click();
};

context('Feedback', () => {
    beforeEach(() => {
        cy.task('getApiUser').then((val) => {
            API_USER = val;
        });
        cy.task('getApiPass').then((val) => {
            API_PASS = val;
        });

        cy.task('deleteQuestions');

        cy.visit('http://localhost:3000');
    });

    it('can open and close the question-form', () => {
        loginAdmin();

        cy.get('[href="/questions"]').click();
        cy.get('button').contains('create new').click();

        cy.get('div[role="dialog"]').within(() => {
            cy.get('.MuiDialogTitle-root > .MuiTypography-root').should('exist');
            cy.get('button').contains('Cancel').click();
        });

        cy.get('.MuiDialogTitle-root > .MuiTypography-root').should('not.exist');
    });

    it('can create a question', () => {
        loginAdmin();

        cy.fixture('questions.json').then((fixture) => {
            const question = fixture.questions[1];

            cy.get('[href="/questions"]').click();
            cy.get('button').contains('create new').click();

            cy.get('div[role="dialog"]').within(() => {
                // rules
                cy.get('#en_titles_localization').type(question.rules.titles.en);
                cy.get('#fi_titles_localization').type(question.rules.titles.fi);
                cy.get('#yes')
                    .type('{selectAll}')
                    .type(question.rules.answer.yes.value);
                cy.get('#en_yes_localization').type(
                    question.rules.answer.yes.labels.en,
                );
                cy.get('#fi_yes_localization').type(
                    question.rules.answer.yes.labels.fi,
                );
                cy.get('#no').type('{selectAll}').type(question.rules.answer.no.value);
                cy.get('#en_no_localization').type(question.rules.answer.no.labels.en);
                cy.get('#fi_no_localization').type(question.rules.answer.no.labels.fi);

                // schedule
                cy.get('[tabindex="-1"] > .MuiTab-wrapper').click();

                // first
                cy.get('#days_since_registration')
                    .type('{selectAll}')
                    .type(question.rules.schedule.first.days_since_registration);
                cy.get('#sent_messages_threshold')
                    .type('{selectAll}')
                    .type(question.rules.schedule.first.sent_messages_threshold);

                // repetitions
                cy.get('#times').type('{selectAll}').type(2);
                cy.get('#days_since_previous_answer').type('{selectAll}').type(30);
                cy.get('#messages_since_previous_answer').type('{selectAll}').type(100);
                cy.get('#min_days_since_previous_answer').type('{selectAll}').type(5);
                cy.get('#max_old_account_in_days').type('{selectAll}').type(30);

                // reminder
                cy.get('#remind_times_when_skipped')
                    .type('{selectAll}')
                    .type(question.rules.schedule.remind_times_when_skipped);

                // save it
                cy.get('button').contains('Save').click();
            });

            // assertion
            cy.get('.MuiList-root').children().should('have.length', 1);
        });
    });

    it('can delete a question', () => {
        loginAdmin();

        // create a question to delete
        cy.fixture('questions.json').then((fixture) => {
            const question = fixture.questions[0];
            cy.task('createQuestion', question);

            // open the item
            cy.get('[href="/questions"]').click();
            cy.get('.MuiList-root').children().first().click();

            // the delete button
            cy.get('div[role="dialog"]').within(() => {
                cy.get('button').contains('Delete').click();
            });

            // the confirmation
            cy.get('div[role="dialog"]').within(() => {
                cy.get('button').contains('Confirm delete').click();
            });
        });

        cy.get('.MuiList-root').children().should('have.length', 0);
    });
});
