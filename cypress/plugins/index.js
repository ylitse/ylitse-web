/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */
// / <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const fetch = require('node-fetch');
const fixturesSkills = require('../fixtures/skills.json');

const API_URL = process.env.YLITSE_API_URL || 'http://localhost:8080';
const API_USER = process.env.YLITSE_API_USER || 'admin';
const API_PASS = process.env.YLITSE_API_PASS || '';

/**
 * Get access_token
 */
const APIAccessToken = async (loginName, password) => {
    const loginResponse = await fetch(`${API_URL}/login`, {
        method: 'POST',
        body: JSON.stringify({ login_name: loginName, password }),
    });
    const tokens = await loginResponse.json();
    return tokens.tokens.access_token;
};

/**
 * Get access_token for admin
 */
const APIAdminAccessToken = async () => APIAccessToken(API_USER, API_PASS);

/**
 * Get users from API
 */
const APIUsers = async (accessToken) => {
    const headers = {
        Authorization: `Bearer ${accessToken}`,
    };
    const usersResponse = await fetch(`${API_URL}/users`, {
        method: 'GET',
        headers,
    });
    const users = await usersResponse.json();
    return users.resources;
};

/**
 * Get myuser from API
 */
const APIMyUser = async (accessToken) => {
    const headers = {
        Authorization: `Bearer ${accessToken}`,
    };
    const myUsersResponse = await fetch(`${API_URL}/myuser`, {
        method: 'GET',
        headers,
    });
    const users = await myUsersResponse.json();
    return users;
};

/**
 * Makes HTTP API calls to delete all users except those with role 'admin'
 */
const APIDeleteAccounts = async () => {
    const accessToken = await APIAdminAccessToken();
    const users = await APIUsers(accessToken);

    const adminMyUser = await APIMyUser(accessToken);

    const headers = {
        Authorization: `Bearer ${accessToken}`,
    };
    for (const user of users) {
    // Delete all accounts except the first admin user
        if (adminMyUser.account.id !== user.account_id) {
            await fetch(`${API_URL}/accounts/${user.account_id}`, {
                method: 'DELETE',
                headers,
            });
        }
    }
};

/**
 * Get all questions from API
 */
async function APIQuestions(accessToken) {
    const headers = {
        Authorization: `Bearer ${accessToken}`,
    };

    const questionsResponse = await fetch(`${API_URL}/feedback/questions`, {
        method: 'GET',
        headers,
    });
    const questions = await questionsResponse.json();

    return questions.resources;
}

/**
 * Makes HTTP API calls to delete all questions
 */
async function APIDeleteQuestions() {
    const accessToken = await APIAdminAccessToken();
    const questions = await APIQuestions(accessToken);

    const headers = {
        Authorization: `Bearer ${accessToken}`,
    };
    for (const question of questions) {
        await fetch(`${API_URL}/feedback/questions/${question.id}`, {
            method: 'DELETE',
            headers,
        });
    }
}

/**
 * Create a question
 */
async function APICreateQuestion(question) {
    const accessToken = await APIAdminAccessToken();

    const headers = {
        Authorization: `Bearer ${accessToken}`,
    };
    await fetch(`${API_URL}/feedback/questions`, {
        method: 'POST',
        headers,
        body: JSON.stringify(question),
    });
}

/**
 * Create skills delete all users except those with role 'admin'
 */
const APICreateInitialSkills = async () => {
    const accessToken = await APIAdminAccessToken();

    const headers = {
        Authorization: `Bearer ${accessToken}`,
    };
    for (const skill of fixturesSkills.skills) {
        await fetch(`${API_URL}/skills`, {
            method: 'POST',
            headers,
            body: JSON.stringify(skill),
        });
    }
};

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (on) => {
    // `on` is used to hook into various events Cypress emits
    // `config` is the resolved Cypress config
    on('task', {
        async deleteAccounts() {
            await APIDeleteAccounts();
            return null;
        },
        async createInitialSkills() {
            await APICreateInitialSkills();
            return null;
        },
        async getApiUrl() {
            return API_URL;
        },
        async getApiUser() {
            return API_USER;
        },
        async getApiPass() {
            return API_PASS;
        },
        async createQuestion(question) {
            await APICreateQuestion(question);
            return null;
        },
        async deleteQuestions() {
            await APIDeleteQuestions();
            return null;
        },
    });
};
